/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.loginserver.network.serverpackets;

import com.l2jfrozen.database.GameServerInstanceManager;
import com.l2jfrozen.loginserver.L2LoginClient;
import com.l2jfrozen.thread.GameServerInfo;
import com.l2jfrozen.thread.ServerStatus;
import javolution.util.FastList;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

/**
 * ServerList Format: cc [cddcchhcdc] c: server list size (number of servers) c: ? [ (repeat for each servers) c: server id (ignored by client?) d: server ip d: server port c: age limit (used by client?) c: pvp or not (used by client?) h: current number of players h: max number of players c: 0 if server is down d: 2nd bit: clock 3rd bit: wont dsiplay server name 4th bit: test server (used by
 * client?) c: 0 if you dont want to display brackets in front of sever name ] Server will be considered as Good when the number of online players is less than half the maximum. as Normal between half and 4/5 and Full when there's more than 4/5 of the maximum number of players
 */
public final class ServerList extends L2LoginServerPacket {
    private List<ServerData> _servers;
    private int _lastServer;

    class ServerData {
        private String ip;
        private int port;
        private boolean pvp;
        private int currentPlayers;
        private int maxPlayers;
        private boolean testServer;
        private boolean brackets;
        private boolean clock;
        private int status;
        private int serverId;

        ServerData(String pIp, int pPort, boolean pPvp, boolean pTestServer, int pCurrentPlayers, int pMaxPlayers, boolean pBrackets, boolean pClock, int pStatus, int pServer_id) {
            ip = pIp;
            port = pPort;
            pvp = pPvp;
            testServer = pTestServer;
            currentPlayers = pCurrentPlayers;
            maxPlayers = pMaxPlayers;
            brackets = pBrackets;
            clock = pClock;
            status = pStatus;
            serverId = pServer_id;
        }
    }

    public ServerList(L2LoginClient client) {
        _servers = new FastList<>();
        _lastServer = client.getLastServer();

        for (GameServerInfo gsi : GameServerInstanceManager.getInstance().getServers()) {
            if (gsi.getStatus() == ServerStatus.STATUS_GM_ONLY && client.getAccessLevel() >= 100) {
                // Server is GM-Only but you've got GM Status
                addServer(client.usesInternalIP() ? gsi.getInternalHost() : gsi.getExternalHost(), gsi.getPort(), gsi.isPvp(), gsi.isTestServer(), gsi.getCurrentPlayerCount(), gsi.getMaxPlayers(), gsi.isShowingBrackets(), gsi.isShowingClock(), gsi.getStatus().ordinal(), gsi.getId());
            } else if (gsi.getStatus() != ServerStatus.STATUS_GM_ONLY) {
                // Server is not GM-Only
                addServer(client.usesInternalIP() ? gsi.getInternalHost() : gsi.getExternalHost(), gsi.getPort(), gsi.isPvp(), gsi.isTestServer(), gsi.getCurrentPlayerCount(), gsi.getMaxPlayers(), gsi.isShowingBrackets(), gsi.isShowingClock(), gsi.getStatus().ordinal(), gsi.getId());
            } else {
                // Server's GM-Only and you've got no GM-Status
                addServer(client.usesInternalIP() ? gsi.getInternalHost() : gsi.getExternalHost(), gsi.getPort(), gsi.isPvp(), gsi.isTestServer(), gsi.getCurrentPlayerCount(), gsi.getMaxPlayers(), gsi.isShowingBrackets(), gsi.isShowingClock(), ServerStatus.STATUS_DOWN.ordinal(), gsi.getId());
            }
        }
    }

    public void addServer(String ip, int port, boolean pvp, boolean testServer, int currentPlayer, int maxPlayer, boolean brackets, boolean clock, int status, int server_id) {
        _servers.add(new ServerData(ip, port, pvp, testServer, currentPlayer, maxPlayer, brackets, clock, status, server_id));
    }

    @Override
    public void write() {
        writeC(0x04);
        writeC(_servers.size());
        writeC(_lastServer);

        for (ServerData server : _servers) {
            writeC(server.serverId); // server id

            try {
                InetAddress inetAddress = InetAddress.getByName(server.ip);

                byte[] raw = inetAddress.getAddress();

                writeC(raw[0] & 0xff);
                writeC(raw[1] & 0xff);
                writeC(raw[2] & 0xff);
                writeC(raw[3] & 0xff);
                raw = null;
            } catch (UnknownHostException e) {
                e.printStackTrace();
                writeC(127);
                writeC(0);
                writeC(0);
                writeC(1);
            }

            writeD(server.port);
            writeC(0x00); // age limit
            writeC(server.pvp ? 0x01 : 0x00);
            writeH(server.currentPlayers);
            writeH(server.maxPlayers);
            writeC(server.status == ServerStatus.STATUS_DOWN.ordinal() ? 0x00 : 0x01);

            int bits = 0;

            if (server.testServer) {
                bits |= 0x04;
            }

            if (server.clock) {
                bits |= 0x02;
            }

            writeD(bits);
            writeC(server.brackets ? 0x01 : 0x00);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getType() {
        return "ServerList";
    }
}
