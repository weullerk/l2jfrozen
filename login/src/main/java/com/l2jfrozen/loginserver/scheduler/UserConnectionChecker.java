package com.l2jfrozen.loginserver.scheduler;

import com.l2jfrozen.configuration.LoginServerConfig;
import com.l2jfrozen.loginserver.LoginController;
import com.l2jfrozen.loginserver.network.serverpackets.LoginFail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * author vadim.didenko
 * 16.04.14.
 */
@Service
public class UserConnectionChecker {
    protected static final Logger LOGGER = LoggerFactory.getLogger(UserConnectionChecker.class.getName());

    @Scheduled(cron = "${user.expired.connection.time}")
    public void checkUserConnectionExpired() {
        long timeNow = System.currentTimeMillis();
        LoginController.getClients().forEach(client -> {
            try {
                if (client != null && timeNow - client.getConnectionStartTime() > LoginServerConfig.SESSION_TTL) {
                    client.close(LoginFail.LoginFailReason.REASON_TEMP_PASS_EXPIRED);
                } else {
                    LoginController.removeLoginClient(client);
                }
            } catch (Exception e) {
                LOGGER.error("", e);

            }
        });
    }
}
