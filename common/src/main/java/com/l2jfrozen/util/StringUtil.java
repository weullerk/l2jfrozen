/*
 * $Header$
 * 
 * $Author: fordfrog $ $Date$ $Revision$ $Log$
 * 
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.Properties;

import javolution.text.TextBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.util.random.Rnd;

public final class StringUtil
{
	private static final Logger LOGGER = LoggerFactory.getLogger(StringUtil.class);

	private StringUtil()
	{
	}

	/**
	 * Concatenates strings.
	 * 
	 * @param strings
	 *            strings to be concatenated
	 * @return concatenated string
	 * @see StringUtil
	 */
	public static String concat(final String... strings)
	{
		final TextBuilder sbString = TextBuilder.newInstance();

		for (final String string : strings)
		{
			sbString.append(string);
		}

		String result = sbString.toString();
		TextBuilder.recycle(sbString);
		return result;
	}

	/**
	 * Creates new string builder with size initializated to <code>sizeHint</code>, unless total length of strings is greater than <code>sizeHint</code>.
	 * 
	 * @param sizeHint
	 *            hint for string builder size allocation
	 * @param strings
	 *            strings to be appended
	 * @return created string builder
	 * @see StringUtil
	 */
	public static StringBuilder startAppend(final int sizeHint, final String... strings)
	{
		final int length = getLength(strings);
		final StringBuilder sbString = new StringBuilder(sizeHint > length ? sizeHint : length);

		for (final String string : strings)
		{
			sbString.append(string);
		}

		return sbString;
	}

	/**
	 * Appends strings to existing string builder.
	 * 
	 * @param sbString
	 *            string builder
	 * @param strings
	 *            strings to be appended
	 * @see StringUtil
	 */
	public static void append(final StringBuilder sbString, final String... strings)
	{
		sbString.ensureCapacity(sbString.length() + getLength(strings));

		for (final String string : strings)
		{
			sbString.append(string);
		}
	}

	/**
	 * Counts total length of all the strings.
	 * 
	 * @param strings
	 *            array of strings
	 * @return total length of all the strings
	 */
	private static int getLength(final String[] strings)
	{
		int length = 0;

		for (final String string : strings)
		{
			if (string == null)
				length += 4;
			else
				length += string.length();
		}

		return length;
	}

	public static String getTraceString(StackTraceElement[] trace)
	{
		final TextBuilder sbString = TextBuilder.newInstance();
		for (final StackTraceElement element : trace)
		{
			sbString.append(element.toString()).append('\n');
		}

		String result = sbString.toString();
		TextBuilder.recycle(sbString);
		return result;
	}

	public static String generateHex()
	{
		byte[] array = new byte[16];
		Rnd.nextBytes(array);

		return new BigInteger(array).toString(16);
	}

	public static byte[] generateHex(int size)
	{
		byte[] array = new byte[size];
		Rnd.nextBytes(array);
		return array;
	}

	public static void saveHexid(int serverId, String hexId)
	{
		String filePath = "./hexid.properties";
		saveHexid(serverId, hexId, filePath);
	}

	public static void saveHexid(int serverId, String hexId, String filePath)
	{
		OutputStream out = null;
		try
		{
			Properties properties = new Properties();
			File file = new File(filePath);

			out = new FileOutputStream(file);
			properties.setProperty("register.server.id", String.valueOf(serverId));
			properties.setProperty("register.server.hex.id", hexId);
			properties.store(out, "the hexID to auth into login");

		}
		catch (Exception e)
		{
			LOGGER.error("Failed to save hex id to " + filePath + " File.", e);
		}
		finally
		{

			if (out != null)
				try
				{
					out.close();
				}
				catch (Exception e)
				{
					LOGGER.error("", e);
				}

		}
	}
}
