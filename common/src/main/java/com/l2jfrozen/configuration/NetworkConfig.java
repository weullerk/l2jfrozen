package com.l2jfrozen.configuration;

import java.util.Properties;
import java.util.StringTokenizer;

import javax.annotation.PostConstruct;

import javolution.util.FastList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

@Component
@ManagedResource(objectName = "com.l2jfrozen:application=Common, name=NetworkConfig")
public class NetworkConfig
{
	public static boolean PACKET_HANDLER_DEBUG;
	/**
	 * MMO settings
	 */
	public int MMO_SELECTOR_SLEEP_TIME = 20; // default 20
	public int MMO_MAX_SEND_PER_PASS = 12; // default 12
	public int MMO_MAX_READ_PER_PASS = 12; // default 12
	public int MMO_HELPER_BUFFER_COUNT = 20; // default 20
	public static boolean ENABLE_MMOCORE_EXCEPTIONS = false;
	public static boolean ENABLE_MMOCORE_DEBUG = false;
	public static boolean ENABLE_MMOCORE_DEVELOP = false;
	/**
	 * Client Packets Queue settings
	 */
	public static boolean ENABLE_CLIENT_FLOOD_PROTECTION = true;
	public static int CLIENT_PACKET_QUEUE_SIZE; // default MMO_MAX_READ_PER_PASS + 2
	public static int CLIENT_PACKET_QUEUE_MAX_BURST_SIZE; // default MMO_MAX_READ_PER_PASS + 1
	public static int CLIENT_PACKET_QUEUE_MAX_PACKETS_PER_SECOND; // default 80
	public static int CLIENT_PACKET_QUEUE_MEASURE_INTERVAL; // default 5
	public static int CLIENT_PACKET_QUEUE_MAX_AVERAGE_PACKETS_PER_SECOND; // default 40
	public static int CLIENT_PACKET_QUEUE_MAX_FLOODS_PER_MIN; // default 2
	public static int CLIENT_PACKET_QUEUE_MAX_OVERFLOWS_PER_MIN; // default 1
	public static int CLIENT_PACKET_QUEUE_MAX_UNDERFLOWS_PER_MIN; // default 1
	public static int CLIENT_PACKET_QUEUE_MAX_UNKNOWN_PER_MIN; // default 5
	// Packets flooding Config
	public static boolean DISABLE_FULL_PACKETS_FLOOD_PROTECTOR;
	public static int FLOOD_PACKET_PROTECTION_INTERVAL;
	public static boolean LOG_PACKET_FLOODING;
	public static int PACKET_FLOODING_PUNISHMENT_LIMIT;
	public static String PACKET_FLOODING_PUNISHMENT_TYPE;
	public static String PROTECTED_OPCODES;
	public static FastList<Integer> GS_LIST_PROTECTED_OPCODES = new FastList<>();
	public static FastList<Integer> GS_LIST_PROTECTED_OPCODES2 = new FastList<>();
	public static FastList<Integer> LS_LIST_PROTECTED_OPCODES = new FastList<>();
	public static String ALLOWED_OFFLINE_OPCODES;
	public static FastList<Integer> LIST_ALLOWED_OFFLINE_OPCODES = new FastList<>();
	public static FastList<Integer> LIST_ALLOWED_OFFLINE_OPCODES2 = new FastList<>();
	public static boolean DUMP_CLOSE_CONNECTIONS;

	@Autowired
	@Qualifier("commonProperties")
	private Properties properties;

	private NetworkConfig()
	{

	}

	@PostConstruct
	@ManagedOperation(description = "Reload config")
	public void load()
	{
		ENABLE_MMOCORE_EXCEPTIONS = Boolean.getBoolean(properties.getProperty("EnableMMOCoreExceptions"));
		ENABLE_MMOCORE_DEBUG = Boolean.getBoolean(properties.getProperty("EnableMMOCoreDebug"));
		ENABLE_MMOCORE_DEVELOP = Boolean.getBoolean(properties.getProperty("EnableMMOCoreDevelop"));
		PACKET_HANDLER_DEBUG = Boolean.getBoolean(properties.getProperty("PacketHandlerDebug"));

		// flooding protection
		DISABLE_FULL_PACKETS_FLOOD_PROTECTOR = Boolean.getBoolean(properties.getProperty("DisableOpCodesFloodProtector"));
		FLOOD_PACKET_PROTECTION_INTERVAL = Integer.decode(properties.getProperty("FloodPacketProtectionInterval"));
		LOG_PACKET_FLOODING = Boolean.getBoolean(properties.getProperty("LogPacketFlooding"));
		PACKET_FLOODING_PUNISHMENT_LIMIT = Integer.decode(properties.getProperty("PacketFloodingPunishmentLimit"));
		PACKET_FLOODING_PUNISHMENT_TYPE = properties.getProperty("PacketFloodingPunishmentType");

		// CLIENT-QUEUE-SETTINGS
		ENABLE_CLIENT_FLOOD_PROTECTION = Boolean.getBoolean(properties.getProperty("EnableClientFloodProtection"));
		CLIENT_PACKET_QUEUE_SIZE = Integer.decode(properties.getProperty("ClientPacketQueueSize"));
		CLIENT_PACKET_QUEUE_MAX_BURST_SIZE = Integer.decode(properties.getProperty("ClientPacketQueueMaxBurstSize"));
		CLIENT_PACKET_QUEUE_MAX_PACKETS_PER_SECOND = Integer.decode(properties.getProperty("ClientPacketQueueMaxPacketsPerSecond")); // default 80
		CLIENT_PACKET_QUEUE_MEASURE_INTERVAL = Integer.decode(properties.getProperty("ClientPacketQueueMeasureInterval")); // default 5
		CLIENT_PACKET_QUEUE_MAX_AVERAGE_PACKETS_PER_SECOND = Integer.decode(properties.getProperty("ClientPacketQueueMaxAveragePacketsPerSecond")); // default 40
		CLIENT_PACKET_QUEUE_MAX_FLOODS_PER_MIN = Integer.decode(properties.getProperty("ClientPacketQueueMaxFloodPerMin")); // default 6
		CLIENT_PACKET_QUEUE_MAX_OVERFLOWS_PER_MIN = Integer.decode(properties.getProperty("ClientPacketQueueOverflowsPerMin")); // default 3
		CLIENT_PACKET_QUEUE_MAX_UNDERFLOWS_PER_MIN = Integer.decode(properties.getProperty("ClientPacketQueueUnderflowsPerMin")); // default 3
		CLIENT_PACKET_QUEUE_MAX_UNKNOWN_PER_MIN = Integer.decode(properties.getProperty("ClientPacketQueueUnknownPerMin")); // default 5

		PROTECTED_OPCODES = properties.getProperty("ListOfProtectedOpCodes");

		LS_LIST_PROTECTED_OPCODES = new FastList<>();
		GS_LIST_PROTECTED_OPCODES = new FastList<>();
		GS_LIST_PROTECTED_OPCODES2 = new FastList<>();

		if (PROTECTED_OPCODES != null && !PROTECTED_OPCODES.equals(""))
		{

			StringTokenizer st = new StringTokenizer(PROTECTED_OPCODES, ";");

			while (st.hasMoreTokens())
			{

				String token = st.nextToken();

				String[] token_splitted = null;

				if (token != null && !token.equals(""))
				{
					token_splitted = token.split(",");
				}
				else
				{
					continue;
				}

				if (token_splitted == null || token_splitted.length < 2)
				{
					continue;
				}

				String server = token_splitted[0];

				if (server.equalsIgnoreCase("g"))
				{

					String opcode1 = token_splitted[1].substring(2);
					String opcode2 = "";

					if (token_splitted.length == 3 && opcode1.equals("0xd0"))
					{
						opcode2 = token_splitted[2].substring(2);
					}

					if (opcode1 != null && !opcode1.equals(""))
					{
						GS_LIST_PROTECTED_OPCODES.add(Integer.parseInt(opcode1, 16));
					}

					if (opcode2 != null && !opcode2.equals(""))
					{
						GS_LIST_PROTECTED_OPCODES2.add(Integer.parseInt(opcode2, 16));
					}

				}
				else if (server.equalsIgnoreCase("l"))
				{ // login opcode

					LS_LIST_PROTECTED_OPCODES.add(Integer.parseInt(token_splitted[1].substring(2), 16));

				}

			}

		}

		ALLOWED_OFFLINE_OPCODES = properties.getProperty("ListOfAllowedOfflineOpCodes");

		LIST_ALLOWED_OFFLINE_OPCODES = new FastList<>();
		LIST_ALLOWED_OFFLINE_OPCODES2 = new FastList<>();

		if (ALLOWED_OFFLINE_OPCODES != null && !ALLOWED_OFFLINE_OPCODES.equals(""))
		{

			StringTokenizer st = new StringTokenizer(ALLOWED_OFFLINE_OPCODES, ";");

			while (st.hasMoreTokens())
			{

				String token = st.nextToken();

				String[] token_splitted = null;

				if (token != null && !token.equals(""))
				{
					token_splitted = token.split(",");
				}
				else
				{
					continue;
				}

				if (token_splitted == null || token_splitted.length == 0 || token_splitted[0].length() <= 3)
				{
					continue;
				}

				String opcode1 = token_splitted[0].substring(2);

				if (opcode1 != null && !opcode1.equals(""))
				{
					LIST_ALLOWED_OFFLINE_OPCODES.add(Integer.parseInt(opcode1, 16));
					if (token_splitted.length > 1 && opcode1.equals("d0"))
					{
						for (int i = 1; i < token_splitted.length; i++)
						{
							if (token_splitted[i].length() <= 3)
							{
								break;
							}

							String opcode2 = token_splitted[i].substring(2);
							if (opcode2 != null && !opcode2.equals(""))
							{
								LIST_ALLOWED_OFFLINE_OPCODES2.add(Integer.parseInt(opcode2, 16));
							}
						}
					}
				}
			}

		}
		DUMP_CLOSE_CONNECTIONS = Boolean.getBoolean(properties.getProperty("DumpCloseConnectionLogs"));
	}
}