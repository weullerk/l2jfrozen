package com.l2jfrozen.configuration;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ThreadConfig
{
	// Threads
	public static int THREAD_P_EFFECTS;
	public static int THREAD_P_GENERAL;
	public static int GENERAL_PACKET_THREAD_CORE_SIZE;
	public static int IO_PACKET_THREAD_CORE_SIZE;
	public static int GENERAL_THREAD_CORE_SIZE;
	public static int AI_MAX_THREAD;
	@Autowired
	private ConfigManager configManager;
	private static ThreadConfig networkConfig;

	private ThreadConfig()
	{

	}

	public static ThreadConfig getInstance()
	{
		if (networkConfig == null)
			networkConfig = new ThreadConfig();
		return networkConfig;
	}

	@PostConstruct
	public void load()
	{
		THREAD_P_EFFECTS = configManager.getInteger("ThreadPoolSizeEffects");
		THREAD_P_GENERAL = configManager.getInteger("ThreadPoolSizeGeneral");
		GENERAL_PACKET_THREAD_CORE_SIZE = configManager.getInteger("GeneralPacketThreadCoreSize");
		IO_PACKET_THREAD_CORE_SIZE = configManager.getInteger("UrgentPacketThreadCoreSize");
		AI_MAX_THREAD = configManager.getInteger("AiMaxThread");
		GENERAL_THREAD_CORE_SIZE = configManager.getInteger("GeneralThreadCoreSize");
	}
}