package com.l2jfrozen.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Server config manger Contains all properties
 * <p/>
 * User: vdidenko Date: 11/17/13 Time: 6:09 PM
 */
@Service
public class ConfigManager
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigManager.class);
	// config properties list
	private static Map<Object, Object> configList = new HashMap<>();
	private List<ConfigManagerObserver> observers = new ArrayList<>();
	@Autowired
	@Qualifier("commonProperties")
	private Properties properties;

	public ConfigManager()
	{

	}

	private void loadProperties()
	{
		configList = new HashMap<>();
		parseConfigs();
		LOGGER.info("Found {} properties", configList.size());
		this.onConfigLoad();
	}

	private void parseConfigs()
	{

		try
		{
			for (Map.Entry<Object, Object> entry : properties.entrySet())
			{
				Object key = entry.getKey();
				if (configList.containsKey(key))
				{
					LOGGER.warn("-- Property: '{}' duplicate.", key);
					continue;
				}
				configList.put(entry.getKey(), entry.getValue());
			}

		}
		catch (Exception e)
		{
			LOGGER.warn("Parse fault", e);
		}
	}

	/**
	 * @param property
	 *            - Property name
	 * @return - property integer value
	 */
	public int getInteger(String property)
	{
		Object prop = getProperty(property);
		if (prop == null)
		{
			return 0;
		}
		return Integer.decode(prop.toString());
	}

	/**
	 * @param property
	 *            - Property name
	 * @return - property integer value
	 */
	public long getLong(String property)
	{
		Object prop = getProperty(property);
		if (prop == null)
		{
			return 0;
		}
		return Long.parseLong(prop.toString());
	}

	/**
	 * @param property
	 *            - Property name
	 * @return - property string value
	 */
	public String getString(String property)
	{
		Object prop = getProperty(property);
		if (prop == null)
		{
			return "";
		}
		return prop.toString();
	}

	/**
	 * @param property
	 *            - Property name
	 * @return - property byte value
	 */
	public Byte getByte(String property)
	{
		Object prop = getProperty(property);
		if (prop == null)
		{
			return 0;
		}
		return Byte.parseByte(prop.toString());
	}

	/**
	 * @param property
	 *            - Property name
	 * @return - property boolean value
	 */
	public Boolean getBoolean(String property)
	{
		Object prop = getProperty(property);
		return prop != null && Boolean.parseBoolean(prop.toString());
	}

	/**
	 * @param property
	 *            - Property name
	 * @return - property float value
	 */
	public float getFloat(String property)
	{
		Object prop = getProperty(property);
		if (prop == null)
		{
			return 0;
		}
		return Float.valueOf(prop.toString());
	}

	/**
	 * @param property
	 *            - Property name
	 * @return - property float value
	 */
	public double getDouble(String property)
	{
		Object prop = getProperty(property);
		if (prop == null)
		{
			return 0;
		}
		return Double.parseDouble(prop.toString());
	}

	/**
	 * Return array values for property key
	 * 
	 * @param property
	 *            - property name
	 * @param splitChar
	 *            - property delimiter
	 * @param propertyType
	 *            property type {@link PropertyType}
	 * @return list of values
	 */
	public ArrayList getArray(String property, String splitChar, PropertyType propertyType)
	{

		ArrayList list = new ArrayList();

		String prop = getString(property);
		String[] values = prop.split(splitChar);
		Object val = null;
		try
		{
			for (String s : values)
			{
				switch (propertyType)
				{
					case INTEGER:
						val = Integer.decode(s);
						break;
					case STRING:
						val = s;
						break;
					case BOOLEAN:
						val = Boolean.getBoolean(s);
						break;
					default:
						break;
				}
				list.add(val);
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Parse properties \'{}\' error", prop);
			LOGGER.error("Propetie error: " + property);
		}

		return list;
	}

	/**
	 * Return hash list of values by property name
	 * 
	 * @param property
	 *            -property name
	 * @return hash list of values
	 */
	public HashMap<Integer, Integer> getHashMap(String property)
	{
		HashMap<Integer, Integer> props = new HashMap<>();
		String prop = getString(property);
		if (prop.length() == 0)
		{
			LOGGER.warn("Empty property: {}", property);
			return props;
		}
		String[] v = prop.split(";");
		if (v.length == 0)
		{
			LOGGER.warn("Empty property: {}", property);
			return props;
		}
		for (String item : v)
		{
			Integer index = Integer.decode(item.split(",")[0]);
			Integer value = Integer.decode(item.split(",")[1]);
			props.put(index, value);
		}
		return props;
	}

	/**
	 * Found property by name
	 * 
	 * @param property
	 *            - property name
	 * @return - property value
	 */
	private Object getProperty(String property)
	{
		Object prop;
		prop = properties.getProperty(property);
		if (prop == null)
		{
			LOGGER.error("Property {} not found", property);
			return null;
		}
		return prop;
	}

	/**
	 * Added config observer
	 * 
	 * @param observer
	 *            - observer
	 */
	public void addObserver(ConfigManagerObserver observer)
	{
		observers.add(observer);
	}

	/**
	 * Remove observer
	 * 
	 * @param observer
	 */
	public void removeObserver(ConfigManagerObserver observer)
	{
		observers.remove(observer);
	}

	/**
	 * Called of config load done
	 */
	private void onConfigLoad()
	{
		for (ConfigManagerObserver observer : observers)
		{
			observer.configurationLoad();
		}
	}

	/**
	 * This method reload config
	 */
	public void nativeReloadConfig()
	{
		loadProperties();
	}

	/**
	 * Used to drive the data to array type
	 */
	public enum PropertyType
	{
		/**
		 * Integer type
		 */
		INTEGER,
		/**
		 * String type
		 */
		STRING,
		/**
		 * Boolean type
		 */
		BOOLEAN,
		/**
		 * Long type
		 */
		LONG
	}

}
