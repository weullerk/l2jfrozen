/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.datatables.sql;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.database.manager.CommonManager;
import com.l2jfrozen.database.model.game.AccessLevelType;
import com.l2jfrozen.database.model.game.character.CharacterAccessLevel;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author FBIagent<br>
 */
public class AccessManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccessManager.class.getName());
    /**
     * The one and only instance of this class, retriveable by getInstance()<br>
     */
    private static AccessManager _instance = null;

    /**
     * FastMap of access levels defined in database<br>
     */
    private final Map<AccessLevelType, CharacterAccessLevel> _accessLevels = new FastMap<>();

    /**
     * Loads the access levels from database<br>
     */
    private AccessManager() {

        try {
            for (CharacterAccessLevel characterAccessLevel : CommonManager.getInstance().getEntityList(CharacterAccessLevel.class, "accessType")) {

                characterAccessLevel.setNameColor("0x" + characterAccessLevel.getNameColor());
                characterAccessLevel.setTitleColor("0x" + characterAccessLevel.getTitleColor());
                _accessLevels.put(characterAccessLevel.getAccessType(), characterAccessLevel);
            }


        } catch (Exception e) {
            LOGGER.error("AccessLevels: Error loading from database ", e);
        }
        LOGGER.debug("AccessLevels: Master Access Level is " + GameServerConfig.MASTERACCESS_LEVEL);
        LOGGER.debug("AccessLevels: User Access Level is " + GameServerConfig.USERACCESS_LEVEL);
    }

    /**
     * Returns the one and only instance of this class<br>
     * <br>
     *
     * @return AccessLevels: the one and only instance of this class<br>
     */
    public static AccessManager getInstance() {
        return _instance == null ? (_instance = new AccessManager()) : _instance;
    }

    /**
     * Returns the access level by characterAccessLevel<br>
     * <br>
     *
     * @param accessLevelNum as int<br>
     *                       <br>
     * @return CharacterAccessLevel: CharacterAccessLevel instance by char access level<br>
     */
    public CharacterAccessLevel getAccessLevel(int accessLevelNum) {
        CharacterAccessLevel characterAccessLevel = null;
        characterAccessLevel = _accessLevels.get(AccessLevelType.getType(accessLevelNum));
        return characterAccessLevel;
    }

    /**
     * Returns the access level by characterAccessLevel<br>
     * <br>
     *
     * @param levelType as int<br>
     *                  <br>
     * @return CharacterAccessLevel: CharacterAccessLevel instance by char access level<br>
     */
    public CharacterAccessLevel getAccessLevel(AccessLevelType levelType) {
        CharacterAccessLevel characterAccessLevel;
        characterAccessLevel = _accessLevels.get(levelType);
        return characterAccessLevel;
    }

    public void addBanAccessLevel(int accessLevel) {
    }

    public static void reload() {
        _instance = null;
        getInstance();
    }

}
