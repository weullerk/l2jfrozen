package com.l2jfrozen.gameserver.datatables.xml;

import com.l2jfrozen.configuration.GameServerConfig;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.sql.Connection;


/**
 * @author Shyla
 */
public class SiegeData {
    private static final Logger LOGGER = LoggerFactory.getLogger(SiegeData.class.getName());

    // =========================================================
    private static SiegeData _instance;

    private FastMap<String, FastMap<Integer, SiegeControlTower>> controlTowers = new FastMap<>();
    private FastMap<String, FastMap<Integer, SiegeArtefact>> artefacts = new FastMap<>();

    public static final SiegeData getInstance() {
        if (_instance == null) {
            _instance = new SiegeData();
        }

        return _instance;
    }

    // =========================================================
    // Data Field

    // =========================================================
    // Constructor
    public SiegeData() {
        LOGGER.info("Loading siege info...");
        load();
    }

    //reload
    public void reload() {
        synchronized (_instance) {
            _instance = null;
            _instance = new SiegeData();
        }
    }

    // =========================================================
    // Method - Private

    private final void load() {
        Connection con = null;
        int siegesCount = 0;
        boolean done = false;

        // Load the zone xml
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            factory.setIgnoringComments(true);

            File file = new File(GameServerConfig.DATAPACK_ROOT + "/data/clans/sieges.xml");

            if (!file.exists()) {
                LOGGER.info("The sieges.xml file is missing.");

            } else {

                Document doc = factory.newDocumentBuilder().parse(file);
                factory = null;
                file = null;

                for (Node root = doc.getFirstChild(); root != null; root = root.getNextSibling()) {
                    if ("list".equalsIgnoreCase(root.getNodeName())) {
                        for (Node siege = root.getFirstChild(); siege != null; siege = siege.getNextSibling()) {
                            if ("siege".equalsIgnoreCase(siege.getNodeName())) {
                                NamedNodeMap attrs = siege.getAttributes();

                                String siegeName = "";
                                if (attrs.getNamedItem("name") != null) {
                                    siegeName = attrs.getNamedItem("name").getNodeValue();
                                }

                                LOGGER.info("SiegeData: loading data for siege " + siegeName);

                                //clear loaded data for siege
                                if (controlTowers.get(siegeName) == null) {
                                    controlTowers.put(siegeName, new FastMap<Integer, SiegeControlTower>());
                                }
                                controlTowers.get(siegeName).clear();

                                if (artefacts.get(siegeName) == null) {
                                    artefacts.put(siegeName, new FastMap<Integer, SiegeArtefact>());
                                }
                                artefacts.get(siegeName).clear();

                                //init data for siege
                                for (Node data = siege.getFirstChild(); data != null; data = data.getNextSibling()) {
                                    String dataName = data.getNodeName();

                                    if (dataName.equalsIgnoreCase("controlTowers")) {

                                        for (Node controlTowerNode = data.getFirstChild(); controlTowerNode != null; controlTowerNode = controlTowerNode.getNextSibling()) {
                                            String controlTowerNodeName = controlTowerNode.getNodeName();
                                            if (controlTowerNodeName.equalsIgnoreCase("controlTower")) {

                                                NamedNodeMap controlTowerNodeAttrs = controlTowerNode.getAttributes();

                                                int towerId = -1;
                                                if (controlTowerNodeAttrs.getNamedItem("id") != null) {
                                                    towerId = Integer.parseInt(controlTowerNodeAttrs.getNamedItem("id").getNodeValue());
                                                }

                                                LOGGER.debug("SiegeData: initializing siege " + siegeName + " tower " + towerId);

                                                SiegeControlTower controlTower = new SiegeControlTower();

                                                for (Node controlTowerNodeProperties = controlTowerNode.getFirstChild(); controlTowerNodeProperties != null; controlTowerNodeProperties = controlTowerNodeProperties.getNextSibling()) {
                                                    String controlTowerNodePropertiesName = controlTowerNodeProperties.getNodeName();
                                                    if (controlTowerNodePropertiesName.equalsIgnoreCase("position")) {

                                                        NamedNodeMap controlTowerNodePropertiesAttrs = controlTowerNodeProperties.getAttributes();
                                                        int x = -1;
                                                        int y = -1;
                                                        int z = -1;

                                                        if (controlTowerNodePropertiesAttrs.getNamedItem("x") != null) {
                                                            x = Integer.parseInt(controlTowerNodePropertiesAttrs.getNamedItem("x").getNodeValue());
                                                        }
                                                        if (controlTowerNodePropertiesAttrs.getNamedItem("y") != null) {
                                                            y = Integer.parseInt(controlTowerNodePropertiesAttrs.getNamedItem("y").getNodeValue());
                                                        }
                                                        if (controlTowerNodePropertiesAttrs.getNamedItem("z") != null) {
                                                            z = Integer.parseInt(controlTowerNodePropertiesAttrs.getNamedItem("z").getNodeValue());
                                                        }

                                                        if (x == -1
                                                                || y == -1
                                                                || z == -1) {

                                                            LOGGER.debug("SiegeData: error loading " + siegeName + " controlTower " + towerId + " position");

                                                        } else {
                                                            controlTower.setX(x);
                                                            controlTower.setY(y);
                                                            controlTower.setZ(z);

                                                            LOGGER.debug("SiegeData: siege " + siegeName + " controlTower " + towerId + " position is [" + x + "," + y + "," + z + "]");

                                                        }

                                                    } else if (controlTowerNodePropertiesName.equalsIgnoreCase("npc")) {

                                                        NamedNodeMap controlTowerNodePropertiesAttrs = controlTowerNodeProperties.getAttributes();
                                                        int npc_id = -1;
                                                        if (controlTowerNodePropertiesAttrs.getNamedItem("id") != null) {
                                                            npc_id = Integer.parseInt(controlTowerNodePropertiesAttrs.getNamedItem("id").getNodeValue());
                                                        }

                                                        if (npc_id == -1) {

                                                            LOGGER.debug("SiegeData: error loading " + siegeName + " controlTower " + towerId + " npc");

                                                        } else {
                                                            controlTower.setNpc_id(npc_id);
                                                            LOGGER.debug("SiegeData: siege " + siegeName + " controlTower " + towerId + " npc is [" + npc_id + "]");

                                                        }

                                                    } else if (controlTowerNodePropertiesName.equalsIgnoreCase("hp")) {
                                                        NamedNodeMap controlTowerNodePropertiesAttrs = controlTowerNodeProperties.getAttributes();
                                                        int hp = -1;
                                                        if (controlTowerNodePropertiesAttrs.getNamedItem("value") != null) {
                                                            hp = Integer.parseInt(controlTowerNodePropertiesAttrs.getNamedItem("value").getNodeValue());
                                                        }

                                                        if (hp == -1) {

                                                            LOGGER.debug("SiegeData: error loading " + siegeName + " controlTower " + towerId + " max hp");

                                                        } else {
                                                            controlTower.setHp(hp);
                                                            LOGGER.debug("SiegeData: siege " + siegeName + " controlTower " + towerId + " max hp is [" + hp + "]");

                                                        }
                                                    }
                                                }
                                                controlTowers.get(siegeName).put(towerId, controlTower);
                                            }

                                        }

                                    } else if (dataName.equalsIgnoreCase("artefacts")) {


                                        for (Node artefactNode = data.getFirstChild(); artefactNode != null; artefactNode = artefactNode.getNextSibling()) {

                                            String artefactNodeName = artefactNode.getNodeName();
                                            if (artefactNodeName.equalsIgnoreCase("artefact")) {

                                                NamedNodeMap artefactNodeAttrs = artefactNode.getAttributes();

                                                int artefactId = -1;
                                                if (artefactNodeAttrs.getNamedItem("id") != null) {
                                                    artefactId = Integer.parseInt(artefactNodeAttrs.getNamedItem("id").getNodeValue());
                                                }

                                                LOGGER.debug("SiegeData: initializing siege " + siegeName + " artefact " + artefactId);

                                                SiegeArtefact artefact = new SiegeArtefact();

                                                for (Node artefactNodeProperties = artefactNode.getFirstChild(); artefactNodeProperties != null; artefactNodeProperties = artefactNodeProperties.getNextSibling()) {

                                                    String artefactNodePropertiesName = artefactNodeProperties.getNodeName();
                                                    if (artefactNodePropertiesName.equalsIgnoreCase("position")) {
                                                        NamedNodeMap artefactNodePropertieyAttrs = artefactNodeProperties.getAttributes();
                                                        int x = -1;
                                                        int y = -1;
                                                        int z = -1;

                                                        if (artefactNodePropertieyAttrs.getNamedItem("x") != null) {
                                                            x = Integer.parseInt(artefactNodePropertieyAttrs.getNamedItem("x").getNodeValue());
                                                        }
                                                        if (artefactNodePropertieyAttrs.getNamedItem("y") != null) {
                                                            y = Integer.parseInt(artefactNodePropertieyAttrs.getNamedItem("y").getNodeValue());
                                                        }
                                                        if (artefactNodePropertieyAttrs.getNamedItem("z") != null) {
                                                            z = Integer.parseInt(artefactNodePropertieyAttrs.getNamedItem("z").getNodeValue());
                                                        }

                                                        if (x == -1
                                                                || y == -1
                                                                || z == -1) {

                                                            LOGGER.debug("SiegeData: error loading " + siegeName + " artefact " + artefactId + " position");

                                                        } else {
                                                            artefact.setX(x);
                                                            artefact.setY(y);
                                                            artefact.setZ(z);

                                                            LOGGER.debug("SiegeData: siege " + siegeName + " artefact " + artefactId + " position is [" + x + "," + y + "," + z + "]");

                                                        }

                                                    } else if (artefactNodePropertiesName.equalsIgnoreCase("npc")) {
                                                        NamedNodeMap artefactNodePropertieyAttrs = artefactNodeProperties.getAttributes();
                                                        int npc_id = -1;
                                                        if (artefactNodePropertieyAttrs.getNamedItem("id") != null) {
                                                            npc_id = Integer.parseInt(artefactNodePropertieyAttrs.getNamedItem("id").getNodeValue());
                                                        }

                                                        if (npc_id == -1) {

                                                            LOGGER.debug("SiegeData: error loading " + siegeName + " artefact " + artefactId + " npc");

                                                        } else {
                                                            artefact.setNpc_id(npc_id);
                                                            LOGGER.debug("SiegeData: siege " + siegeName + " artefact " + artefactId + " npc is [" + npc_id + "]");

                                                        }


                                                    } else if (artefactNodePropertiesName.equalsIgnoreCase("heading")) {
                                                        NamedNodeMap artefactNodePropertieyAttrs = artefactNodeProperties.getAttributes();
                                                        int heading = -1;
                                                        if (artefactNodePropertieyAttrs.getNamedItem("value") != null) {
                                                            heading = Integer.parseInt(artefactNodePropertieyAttrs.getNamedItem("value").getNodeValue());
                                                        }

                                                        if (heading == -1) {

                                                            LOGGER.debug("SiegeData: error loading " + siegeName + " artefact " + artefactId + " heading");

                                                        } else {
                                                            artefact.setHeading(heading);
                                                            LOGGER.debug("SiegeData: siege " + siegeName + " artefact " + artefactId + " heading is [" + heading + "]");

                                                        }
                                                    }

                                                }
                                                artefacts.get(siegeName).put(artefactId, artefact);

                                            }

                                        }

                                    }
                                }

                                siegesCount++;
                            }
                        }
                    }
                }

                done = true;
                doc = null;
            }

        } catch (Exception e) {
            LOGGER.info("Error while loading sieges.", e);
        }

        LOGGER.info("Siege data loaded for " + siegesCount + " sieges..");

    }

    public class SiegeControlTower {

        private int x;
        private int y;
        private int z;
        private int npc_id;
        private int hp;

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getZ() {
            return z;
        }

        public void setZ(int z) {
            this.z = z;
        }

        public int getNpc_id() {
            return npc_id;
        }

        public void setNpc_id(int npc_id) {
            this.npc_id = npc_id;
        }

        public int getHp() {
            return hp;
        }

        public void setHp(int hp) {
            this.hp = hp;
        }


        @Override
        public String toString() {
            return "SiegeControlTower{" +
                    "x=" + x +
                    ", y=" + y +
                    ", z=" + z +
                    ", npc_id=" + npc_id +
                    ", hp=" + hp +
                    '}';
        }
    }

    public class SiegeArtefact {

        private int x;
        private int y;
        private int z;
        private int heading;
        private int npc_id;

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getZ() {
            return z;
        }

        public void setZ(int z) {
            this.z = z;
        }

        public int getHeading() {
            return heading;
        }

        public void setHeading(int heading) {
            this.heading = heading;
        }

        public int getNpc_id() {
            return npc_id;
        }

        public void setNpc_id(int npc_id) {
            this.npc_id = npc_id;
        }

        @Override
        public String toString() {
            return "SiegeArtefact{" +
                    "x=" + x +
                    ", y=" + y +
                    ", z=" + z +
                    ", heading=" + heading +
                    ", npc_id=" + npc_id +
                    '}';
        }
    }

    public FastMap<Integer, SiegeControlTower> getSiegeControlTowers(String siegeName) {
        return controlTowers.get(siegeName);
    }

    public FastMap<Integer, SiegeArtefact> getSiegeArtefacts(String siegeName) {
        return artefacts.get(siegeName);
    }

    public static void main(String[] args) {

        SiegeData.getInstance();

    }

}
