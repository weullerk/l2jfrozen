/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.geo.geoeditorcon;

import com.l2jfrozen.configuration.GameServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class GeoEditorListener extends Thread {
    protected static final Logger LOGGER = LoggerFactory.getLogger(GeoEditorListener.class.getName());

    private static final int PORT = GameServerConfig.GEOEDITOR_PORT;

    private static final class SingletonHolder {
        protected static final GeoEditorListener INSTANCE = new GeoEditorListener();
    }

    public static GeoEditorListener getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private ServerSocket _serverSocket;
    private GeoEditorThread _geoEditor;

    protected GeoEditorListener() {
        try {
            _serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            LOGGER.error("", e);

            LOGGER.info("Error creating geoeditor listener! ", e);
            System.exit(1);
        }
        start();
        LOGGER.info("GeoEditorListener Initialized.");
    }

    public GeoEditorThread getThread() {
        return _geoEditor;
    }

    public String getStatus() {
        if (_geoEditor != null && _geoEditor.isWorking()) {
            return "Geoeditor connected.";
        }
        return "Geoeditor not connected.";
    }

    @Override
    public void run() {
        Socket connection = null;
        try {
            while (true) {
                connection = _serverSocket.accept();
                if (_geoEditor != null && _geoEditor.isWorking()) {
                    LOGGER.info("Geoeditor already connected!");
                    connection.close();
                    continue;
                }
                LOGGER.info("Received geoeditor connection from: " + connection.getInetAddress().getHostAddress());
                _geoEditor = new GeoEditorThread(connection);
                _geoEditor.start();
            }
        } catch (Exception e) {
            LOGGER.error("", e);

            LOGGER.info("GeoEditorListener: ", e);
            try {
                if (connection != null) connection.close();
            } catch (Exception e2) {
                LOGGER.error("", e2);

            }
        } finally {
            try {
                _serverSocket.close();
            } catch (IOException io) {
                LOGGER.error("", io);

                LOGGER.info("", io);
            }
            LOGGER.info("GeoEditorListener Closed!");
        }
    }
}