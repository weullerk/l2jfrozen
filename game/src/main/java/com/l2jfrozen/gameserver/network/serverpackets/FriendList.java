/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.network.serverpackets;

import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterFriendEntity;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Support for "Chat with Friends" dialog. Format: ch (hdSdh) h: Total Friend Count h: Unknown d: Player Object ID S: Friend Name d: Online/Offline h: Unknown
 *
 * @author Tempy
 */
public class FriendList extends L2GameServerPacket {
    private static final String _S__FA_FRIENDLIST = "[S] FA FriendList";
    private static Logger LOGGER = LoggerFactory.getLogger(FriendList.class.getName());
    private L2PcInstance _activeChar;

    public FriendList(L2PcInstance character) {
        _activeChar = character;
    }

    @Override
    protected final void writeImpl() {
        if (_activeChar == null)
            return;

        try {

            List<CharacterFriendEntity> friends = CharacterManager.getInstance().getFriends(_activeChar.getCharacter(), CharacterManager.FriendType.UNBLOCK);

            writeC(0xfa);
            writeD(friends.size());

            for (CharacterFriendEntity friend : friends) {
                int friendId = friend.getFriend().getId().intValue();
                String friendName = friend.getFriendName();

                if (friendId == _activeChar.getObjectId()) {
                    continue;
                }

                L2PcInstance friendItem = L2World.getInstance().getPlayer(friendName);

                writeD(friendId);
                writeS(friendName);

                if (friendItem == null) {
                    writeD(0); // offline
                    writeD(0x00);
                } else {
                    writeD(1); // online
                    writeD(friendId);
                }

            }

        } catch (Exception e) {
            LOGGER.error("", e);

            LOGGER.warn("Error found in " + _activeChar.getName() + "'s FriendList: " + e);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.l2jfrozen.gameserver.serverpackets.ServerBasePacket#getType()
     */
    @Override
    public String getType() {
        return _S__FA_FRIENDLIST;
    }
}