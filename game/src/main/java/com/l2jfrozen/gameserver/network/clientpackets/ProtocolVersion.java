/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.network.clientpackets;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.network.serverpackets.KeyPacket;
import com.l2jfrozen.gameserver.network.serverpackets.L2GameServerPacket;
import com.l2jfrozen.gameserver.network.serverpackets.SendStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ProtocolVersion extends L2GameClientPacket {
    static Logger LOGGER = LoggerFactory.getLogger(ProtocolVersion.class.getName());
    private int _version;

    @Override
    protected void readImpl() {
        _version = readH();
    }

    @Override
    protected void runImpl() {
        // this packet is never encrypted
        if (_version == 65534 || _version == -2) // Ping
        {
            if (GameServerConfig.DEBUG)
                LOGGER.info("DEBUG " + getType() + ": Ping received");

            getClient().close((L2GameServerPacket) null);
        } else if (_version == 65533 || _version == -3) // RWHO
        {
            if (GameServerConfig.RWHO_LOG)
                LOGGER.info(getClient().toString() + " RWHO received");

            getClient().close(new SendStatus());
        } else if (_version < GameServerConfig.MIN_PROTOCOL_REVISION || _version > GameServerConfig.MAX_PROTOCOL_REVISION) {
            LOGGER.info("Client: " + getClient().toString() + " -> Protocol Revision: " + _version + " is invalid. Minimum is " + GameServerConfig.MIN_PROTOCOL_REVISION + " and Maximum is " + GameServerConfig.MAX_PROTOCOL_REVISION + " are supported. Closing connection.");
            LOGGER.warn("Wrong Protocol Version " + _version);
            getClient().close((L2GameServerPacket) null);
        } else {
            if (GameServerConfig.DEBUG)
                LOGGER.debug("DEBUG " + getType() + ": Client Protocol Revision is ok: " + _version);

            KeyPacket pk = new KeyPacket(getClient().enableCrypt());
            getClient().sendPacket(pk);
        }
    }

    @Override
    public String getType() {
        return "[C] 00 ProtocolVersion";
    }
}