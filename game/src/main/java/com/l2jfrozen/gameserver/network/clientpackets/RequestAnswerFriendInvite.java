/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.network.clientpackets;

import com.l2jfrozen.database.model.game.character.CharacterFriendEntity;
import com.l2jfrozen.gameserver.PlayerManager;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.FriendList;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * sample 5F 01 00 00 00 format cdd
 */
public final class RequestAnswerFriendInvite extends L2GameClientPacket {
    private static Logger LOGGER = LoggerFactory.getLogger(RequestAnswerFriendInvite.class.getName());

    private int _response;

    @Override
    protected void readImpl() {
        _response = readD();
    }

    @Override
    protected void runImpl() {
        L2PcInstance player = getClient().getActiveChar();
        if (player == null) {
            return;
        }
        L2PcInstance requestor = player.getActiveRequester();
        if (requestor == null)
            return;

        if (_response == 1) {

            try {
                boolean success = PlayerManager.getInstance().addFriend(requestor, player);
                if (success) {
                    SystemMessage msg = new SystemMessage(SystemMessageId.YOU_HAVE_SUCCEEDED_INVITING_FRIEND);
                    requestor.sendPacket(msg);

                    // Player added to your friendlist
                    msg = new SystemMessage(SystemMessageId.S1_ADDED_TO_FRIENDS);
                    msg.addString(player.getName());
                    requestor.sendPacket(msg);
                    requestor.getFriendList().add(player.getName());

                    // has joined as friend.
                    msg = new SystemMessage(SystemMessageId.S1_JOINED_AS_FRIEND);
                    msg.addString(requestor.getName());
                    player.sendPacket(msg);
                    player.getFriendList().add(requestor.getName());

                    // friend list rework ;)
                    notifyFriends(player);
                    notifyFriends(requestor);
                    player.sendPacket(new FriendList(player));
                    requestor.sendPacket(new FriendList(requestor));
                }
            } catch (Exception e) {
                LOGGER.error("", e);

                LOGGER.warn("could not add friend objectid: " + e);
            }
        } else {
            SystemMessage msg = new SystemMessage(SystemMessageId.FAILED_TO_INVITE_A_FRIEND);
            requestor.sendPacket(msg);
        }

        player.setActiveRequester(null);
        requestor.onTransactionResponse();
    }

    private void notifyFriends(L2PcInstance cha) {
        try {
            for (CharacterFriendEntity friend : cha.getCharacter().getFriends().values()) {
                if (friend.getNotBlocked() == 1) {
                    L2PcInstance friendInstance;
                    friendInstance = L2World.getInstance().getPlayer(friend.getFriendName());

                    if (friendInstance != null) {
                        friendInstance.sendPacket(new FriendList(friendInstance));
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.warn("could not restore friend data:" + e);
        }
    }

    @Override
    public String getType() {
        return "[C] 5F RequestAnswerFriendInvite";
    }
}