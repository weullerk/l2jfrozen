/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.network.clientpackets;

import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterFriendEntity;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class RequestBlock extends L2GameClientPacket {
    private static Logger LOGGER = LoggerFactory.getLogger(RequestBlock.class.getName());

    private enum RequestBlockType {
        BLOCK,
        UNBLOCK,
        BLOCKLIST,
        ALLBLOCK,
        ALLUNBLOCK;

        public static RequestBlockType get(int id) {
            for (RequestBlockType type : RequestBlockType.values()) {
                if (type.ordinal() == id) {
                    return type;
                }
            }
            return null;
        }
    }

    private String _name;
    private RequestBlockType type;

    // private L2PcInstance _target;

    @Override
    protected void readImpl() {
        type = RequestBlockType.get(readD()); // 0x00 - block, 0x01 - unblock, 0x03 - allblock, 0x04 - allunblock

        if (type == RequestBlockType.BLOCK || type == RequestBlockType.UNBLOCK) {
            _name = readS();
        }
    }

    @Override
    protected void runImpl() {
        L2PcInstance activeChar = getClient().getActiveChar();

        if (activeChar == null)
            return;

        switch (type) {
            case BLOCK:
            case UNBLOCK:

                L2PcInstance _target = L2World.getInstance().getPlayer(_name);

                if (_target == null) {
                    // Incorrect player name.
                    activeChar.sendPacket(new SystemMessage(SystemMessageId.FAILED_TO_REGISTER_TO_IGNORE_LIST));
                    return;
                }

                if (_target.isGM()) {
                    // Cannot block a GM character.
                    activeChar.sendPacket(new SystemMessage(SystemMessageId.YOU_MAY_NOT_IMPOSE_A_BLOCK_AN_A_GM));
                    return;
                }

                if (type == RequestBlockType.BLOCK) {

                    if (activeChar.getBlockList().isInBlockList(_name)) {
                        // Player is already in your blocklist
                        activeChar.sendPacket(new SystemMessage(SystemMessageId.FAILED_TO_REGISTER_TO_IGNORE_LIST));
                        return;
                    }

                    activeChar.getBlockList().addToBlockList(_name);

                    CharacterFriendEntity friend = new CharacterFriendEntity();
                    friend.setNotBlocked(type.ordinal());
                    friend.setFriend(_target.getCharacter());
                    friend.setFriendName(_target.getCharacter().getCharName());
                    CharacterManager.getInstance().addFriend(activeChar.getCharacter(), friend);

                } else {
                    activeChar.getBlockList().removeFromBlockList(_name);
                    CharacterManager.getInstance().deleteFriend(activeChar.getCharacter(), _target.getCharacter());
                }

                break;
            case BLOCKLIST:

                activeChar.sendBlockList();

                break;
            case ALLBLOCK:

                activeChar.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.MESSAGE_REFUSAL_MODE));// Update by rocknow
                activeChar.getBlockList().setBlockAll(true);

                break;
            case ALLUNBLOCK:
                activeChar.sendPacket(SystemMessage.getSystemMessage(SystemMessageId.MESSAGE_REFUSAL_MODE));// Update by rocknow
                activeChar.getBlockList().setBlockAll(false);

                break;
            default:
                LOGGER.info("Unknown 0x0a block type: " + type);
        }
    }

    @Override
    public String getType() {
        return "[C] A0 RequestBlock";
    }
}