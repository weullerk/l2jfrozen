/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.network;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.configuration.NetworkConfig;
import com.l2jfrozen.crypt.nProtect;
import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.gameserver.PlayerManager;
import com.l2jfrozen.gameserver.communitybbs.Manager.RegionBBSManager;
import com.l2jfrozen.gameserver.datatables.OfflineTradeTable;
import com.l2jfrozen.gameserver.datatables.SkillTable;
import com.l2jfrozen.gameserver.datatables.sql.ClanTable;
import com.l2jfrozen.gameserver.managers.AwayManager;
import com.l2jfrozen.gameserver.model.CharSelectInfoPackage;
import com.l2jfrozen.gameserver.model.L2Clan;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.entity.event.*;
import com.l2jfrozen.gameserver.model.entity.olympiad.Olympiad;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.L2GameServerPacket;
import com.l2jfrozen.gameserver.network.serverpackets.LeaveWorld;
import com.l2jfrozen.gameserver.network.serverpackets.ServerClose;
import com.l2jfrozen.gameserver.thread.LoginServerThread;
import com.l2jfrozen.gameserver.thread.LoginServerThread.SessionKey;
import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import com.l2jfrozen.gameserver.util.EventData;
import com.l2jfrozen.gameserver.util.FloodProtectors;
import com.l2jfrozen.netcore.MMOClient;
import com.l2jfrozen.netcore.MMOConnection;
import com.l2jfrozen.netcore.ReceivablePacket;
import com.l2jfrozen.util.Log;
import javolution.util.FastList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author L2JFrozen dev
 */
public final class L2GameClient extends MMOClient<MMOConnection<L2GameClient>> implements Runnable {
    protected static final Logger LOGGER = LoggerFactory.getLogger(L2GameClient.class.getName());

    public GameClientState state;

    // Info
    public String accountName;
    public SessionKey sessionId;
    public L2PcInstance activeChar;

    // Crypt
    public GameCrypt crypt;

    // Flood protection
    public long packetsNextSendTick = 0;

    protected ScheduledFuture<?> _cleanupTask = null;

    protected boolean _closenow = true;

    protected boolean _forcedToClose = false;

    // floodprotectors
    private final FloodProtectors _floodProtectors = new FloodProtectors(this);
    private ReentrantLock _activeCharLock = new ReentrantLock();

    private boolean _isAuthedGG;
    private long _connectionStartTime;
    private List<Integer> _charSlotMapping = new FastList<Integer>();

    // Task
    private ScheduledFuture<?> _guardCheckTask = null;

    private ClientStats _stats;

    //unknownPacket protection
    private int unknownPacketCount = 0;
    private boolean _isDetached = false;

    private final ArrayBlockingQueue<ReceivablePacket<L2GameClient>> _packetQueue;
    private ReentrantLock _queueLock = new ReentrantLock();

    private long _last_received_packet_action_time = 0;

    public L2GameClient(MMOConnection<L2GameClient> con) {
        super(con);
        state = GameClientState.CONNECTED;
        _connectionStartTime = System.currentTimeMillis();
        crypt = new GameCrypt();
        _stats = new ClientStats();
        _packetQueue = new ArrayBlockingQueue<ReceivablePacket<L2GameClient>>(NetworkConfig.CLIENT_PACKET_QUEUE_SIZE);

        _guardCheckTask = nProtect.getInstance().startTask(this);
        ThreadPoolManager.getInstance().scheduleGeneral(new Runnable() {
            @Override
            public void run() {
                if (_closenow)
                    close(new LeaveWorld());
            }
        }, 4000);
    }

    public void close(L2GameServerPacket gsp) {
        if (getConnection() != null)
            getConnection().close(gsp);
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String pAccountName) {
        accountName = pAccountName;
    }

    public L2PcInstance getActiveChar() {
        return activeChar;
    }

    public SessionKey getSessionId() {
        return sessionId;
    }

    public void setSessionId(SessionKey sk) {
        sessionId = sk;
    }

    public GameClientState getState() {
        return state;
    }

    /**
     * @return the _forcedToClose
     */
    public boolean is_forcedToClose() {
        return _forcedToClose;
    }

    @Override
    public void run() {
        if (!_queueLock.tryLock())
            return;

        try {
            int count = 0;
            while (true) {
                final ReceivablePacket<L2GameClient> packet = _packetQueue.poll();
                if (packet == null) // queue is empty
                    return;

                if (_isDetached) // clear queue immediately after detach
                {
                    _packetQueue.clear();
                    return;
                }

                try {
                    packet.run();
                } catch (Exception e) {
                    LOGGER.error("", e);

                    LOGGER.error("Exception during execution " + packet.getClass().getSimpleName() + ", client: " + toString() + "," + e.getMessage());
                }

                count++;
                if (getStats().countBurst(count))
                    return;
            }
        } finally {
            _queueLock.unlock();
        }
    }

    public boolean checkUnknownPackets() {
        L2PcInstance player = getActiveChar();

        if (player != null && _floodProtectors != null && _floodProtectors.getUnknownPackets() != null && !_floodProtectors.getUnknownPackets().tryPerformAction("check packet")) {
            unknownPacketCount++;

            if (unknownPacketCount >= GameServerConfig.MAX_UNKNOWN_PACKETS)
                return true;
            return false;
        }
        unknownPacketCount = 0;
        return false;
    }

    @Override
    public boolean decrypt(ByteBuffer buf, int size) {
        _closenow = false;
        GameCrypt.decrypt(buf.array(), buf.position(), size, crypt);
        return true;
    }

    /**
     * Returns false if client can receive packets.
     * True if detached, or flood detected, or queue overflow detected and queue still not empty.
     *
     * @return
     */
    public boolean dropPacket() {
        if (NetworkConfig.ENABLE_CLIENT_FLOOD_PROTECTION) {
            if (_isDetached) // detached clients can't receive any packets
                return true;

            // flood protection
            if (getStats().countPacket(_packetQueue.size())) {
                sendPacket(ActionFailed.STATIC_PACKET);
                return true;
            }

            return getStats().dropPacket();
        } else {
            if (_isDetached) {
                return true;
            }

            return false;
        }
    }

    public void sendPacket(L2GameServerPacket gsp) {
        if (_isDetached)
            return;

        if (getConnection() != null) {
            if (GameServerConfig.DEBUG_PACKETS) {
                LOGGER.debug("[ServerPacket] SendingGameServerPacket, Client: " + this.toString() + " Packet:" + gsp.getType(), "GameServerPacketsLog");
            }

            getConnection().sendPacket(gsp);
            gsp.runImpl();
        }
    }

    /**
     * Produces the best possible string representation of this client.
     */
    @Override
    public String toString() {
        try {
            InetAddress address = getConnection().getInetAddress();
            String ip = "N/A";

            if (address == null) {
                ip = "disconnected";
            } else {
                ip = address.getHostAddress();
            }

            switch (getState()) {
                case CONNECTED:
                    return "[IP: " + ip + "]";
                case AUTHED:
                    return "[Account: " + getAccountName() + " - IP: " + ip + "]";
                case IN_GAME:
                    address = null;
                    return "[Character: " + (getActiveChar() == null ? "disconnected" : getActiveChar().getName()) + " - Account: " + getAccountName() + " - IP: " + ip + "]";
                default:
                    address = null;
                    throw new IllegalStateException("Missing state on switch");
            }
        } catch (NullPointerException e) {
            LOGGER.error("", e);

            return "[Character read failed due to disconnect]";
        }
    }

    public byte[] enableCrypt() {
        byte[] key = BlowFishKeygen.getRandomKey();
        GameCrypt.setKey(key, crypt);
        return key;
    }

    @Override
    public boolean encrypt(final ByteBuffer buf, final int size) {
        GameCrypt.encrypt(buf.array(), buf.position(), size, crypt);
        buf.position(buf.position() + size);
        return true;
    }

    /**
     * Add packet to the queue and start worker thread if needed
     *
     * @param packet
     */
    public void execute(ReceivablePacket<L2GameClient> packet) {
        if (getStats().countFloods()) {
            LOGGER.error("Client " + toString() + " - Disconnected, too many floods:" + getStats().longFloods + " long and " + getStats().shortFloods + " short.");
            closeNow();
            return;
        }

        if (!_packetQueue.offer(packet)) {
            if (getStats().countQueueOverflow()) {
                LOGGER.error("Client " + toString() + " - Disconnected, too many queue overflows.");
                closeNow();
            } else
                sendPacket(ActionFailed.STATIC_PACKET);

            return;
        }

        if (_queueLock.isLocked()) // already processing
            return;

        //save last action time
        _last_received_packet_action_time = System.currentTimeMillis();
        //LOGGER.severe("Client " + toString() + " - updated last action state "+_last_received_packet_action_time);

        try {
            if (state == GameClientState.CONNECTED) {
                if (getStats().processedPackets > 3) {
                    LOGGER.error("Client " + toString() + " - Disconnected, too many packets in non-authed state.");
                    closeNow();
                    return;
                }

                ThreadPoolManager.getInstance().executeIOPacket(this);
            } else
                ThreadPoolManager.getInstance().executePacket(this);
        } catch (RejectedExecutionException e) {
            LOGGER.error("", e);

            // if the server is shutdown we ignore
            if (!ThreadPoolManager.getInstance().isShutdown()) {
                LOGGER.error("Failed executing: " + packet.getClass().getSimpleName() + " for Client: " + toString());
            }
        }
    }

    public ClientStats getStats() {
        return _stats;
    }

    /**
     * Close client connection with {@link ServerClose} packet
     */
    public void closeNow() {
        close(0);
    }

    /**
     * Close client connection with {@link ServerClose} packet
     *
     * @param delay
     */
    public void close(int delay) {
        close(ServerClose.STATIC_PACKET);
        synchronized (this) {
            if (_cleanupTask != null)
                cancelCleanup();
            _cleanupTask = ThreadPoolManager.getInstance().scheduleGeneral(new CleanupTask(), delay); //delayed
        }
        stopGuardTask();
        nProtect.getInstance().closeSession(this);
    }

    private boolean cancelCleanup() {
        Future<?> task = _cleanupTask;
        if (task != null) {
            _cleanupTask = null;
            return task.cancel(true);
        }
        return false;
    }

    public void stopGuardTask() {
        if (_guardCheckTask != null) {
            _guardCheckTask.cancel(true);
            _guardCheckTask = null;
        }
    }

    public ReentrantLock getActiveCharLock() {
        return _activeCharLock;
    }

    public long getConnectionStartTime() {
        return _connectionStartTime;
    }

    public FloodProtectors getFloodProtectors() {
        return _floodProtectors;
    }

    public boolean isAuthedGG() {
        return _isAuthedGG;
    }

    public boolean isConnectionAlive() {
        //if last received packet time is higher then Config.CHECK_CONNECTION_INACTIVITY_TIME --> check connection
        if (System.currentTimeMillis() - _last_received_packet_action_time > GameServerConfig.CHECK_CONNECTION_INACTIVITY_TIME) {
            _last_received_packet_action_time = System.currentTimeMillis();

            return getConnection().isConnected()
                    && !getConnection().isClosed();
        }

        return true;
    }

    public boolean isDetached() {
        return _isDetached;
    }

    public L2PcInstance loadCharFromDisk(int charslot) {
        //L2PcInstance character = L2PcInstance.load(getObjectIdForSlot(charslot));

        final int objId = getObjectIdForSlot(charslot);
        if (objId < 0)
            return null;

        L2PcInstance character = L2World.getInstance().getPlayer(objId);
        if (character != null) {
            // exploit prevention, should not happens in normal way
            if (GameServerConfig.DEBUG)
                LOGGER.error("Attempt of double login: " + character.getName() + "(" + objId + ") " + getAccountName());

            if (character.getClient() != null)
                character.getClient().closeNow();
            else {
                character.deleteMe();

                try {
                    character.store();
                } catch (Exception e2) {
                    LOGGER.error("", e2);
                }
            }
        }

        character = PlayerManager.getInstance().load(objId);
        return character;
    }

    /**
     * @param charslot
     * @return
     */
    private int getObjectIdForSlot(int charslot) {
        if (charslot < 0 || charslot >= _charSlotMapping.size()) {
            LOGGER.warn(toString() + " tried to delete Character in slot " + charslot + " but no characters exits at that slot.");
            return -1;
        }

        Integer objectId = _charSlotMapping.get(charslot);

        return objectId.intValue();
    }

    public void markRestoredChar(int charslot) {
        int objid = getObjectIdForSlot(charslot);

        if (objid < 0)
            return;
        try {
            CharacterEntity character = CharacterManager.getInstance().get(((Number) objid).longValue());
            character.setDeleteTime(0);
            CharacterManager.getInstance().update(character);
        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

    /**
     * Method to handle character deletion
     *
     * @param charslot
     * @return a byte: <li>-1: Error: No char was found for such charslot, caught exception, etc... <li>0: character is
     * not member of any clan, proceed with deletion <li>1: character is member of a clan, but not clan leader
     * <li>2: character is clan leader
     */
    public byte markToDeleteChar(int charslot) {
        int objid = getObjectIdForSlot(charslot);

        if (objid < 0)
            return -1;
        byte answer = -1;

        try {
            CharacterEntity character = CharacterManager.getInstance().get(((Number) objid).longValue());

            int clanId = character.getClanId();

            answer = 0;

            if (clanId != 0) {
                L2Clan clan = ClanTable.getInstance().getClan(clanId);

                if (clan == null) {
                    answer = 0; // jeezes!
                } else if (clan.getLeaderId() == objid) {
                    answer = 2;
                } else {
                    answer = 1;
                }

                clan = null;
            }

            // Setting delete time
            if (answer == 0) {
                if (GameServerConfig.DELETE_DAYS == 0) {
                    deleteCharByObjId(objid);
                } else {
                    character.setDeleteTime(System.currentTimeMillis() + GameServerConfig.DELETE_DAYS * 86400000L);
                    CharacterManager.getInstance().update(character);
                }
            }
        } catch (Exception e) {
            LOGGER.error("", e);
            answer = -1;
        }

        return answer;
    }

    public static void deleteCharByObjId(int objectId) {
        if (objectId < 0)
            return;
        PlayerManager.getInstance().deleteCharacter(objectId);
    }

    /**
     * Counts buffer underflow exceptions.
     */
    public void onBufferUnderflow() {
        if (getStats().countUnderflowException()) {
            LOGGER.error("Client " + toString() + " - Disconnected: Too many buffer underflow exceptions.");
            closeNow();
            return;
        }
        if (state == GameClientState.CONNECTED) // in CONNECTED state kick client immediately
        {
            LOGGER.error("Client " + toString() + " - Disconnected, too many buffer underflows in non-authed state.");
            closeNow();
        }
    }

    @Override
    public void onDisconnection() {
        // no long running tasks here, do it async
        try {
            ThreadPoolManager.getInstance().executeTask(new DisconnectTask());
        } catch (RejectedExecutionException e) {
            // server is closing
            LOGGER.error("", e);
        }
    }

    @Override
    public void onForcedDisconnection(boolean critical) {
        _forcedToClose = true;

        if (critical)
            LOGGER.info("Client " + toString() + " disconnected abnormally.");
        closeNow();
    }

    public void setActiveChar(L2PcInstance pActiveChar) {
        activeChar = pActiveChar;
        if (activeChar != null) {
            L2World.getInstance().storeObject(getActiveChar());
        }
    }

    /**
     * @param chars
     */
    public void setCharSelection(CharSelectInfoPackage[] chars) {
        _charSlotMapping.clear();

        for (CharSelectInfoPackage c : chars) {
            int objectId = c.getObjectId();

            _charSlotMapping.add(new Integer(objectId));
        }
    }

    public void setDetached(boolean b) {
        _isDetached = b;
    }

    public void setGameGuardOk(boolean val) {
        _isAuthedGG = val;
    }

    public void setState(GameClientState pState) {
        if (state != pState) {
            state = pState;
            _packetQueue.clear();
        }
    }

    /**
     * CONNECTED - client has just connected AUTHED - client has authed but doesn't has character attached to it yet
     * IN_GAME - client has selected a char and is in game
     *
     * @author KenM
     */
    public static enum GameClientState {
        CONNECTED,
        AUTHED,
        IN_GAME
    }

    protected class CleanupTask implements Runnable {
        @Override
        public void run() {
            try {
                // Update BBS
                try {
                    RegionBBSManager.getInstance().changeCommunityBoard();
                } catch (Exception e) {
                    LOGGER.error("unhandled exception", e);
                }

//				// we are going to manually save the char below thus we can force the cancel
//				if (_autoSaveInDB != null)
//					_autoSaveInDB.cancel(true);
//				

                L2PcInstance player = L2GameClient.this.getActiveChar();
                if (player != null) // this should only happen on connection loss
                {
                    // we store all data from players who are disconnected while in an event in order to restore it in the next login
                    if (player.atEvent) {
                        EventData data = new EventData(player.eventX, player.eventY, player.eventZ, player.eventKarma, player.eventPvpKills, player.eventPkKills, player.eventTitle, player.kills, player.eventSitForced);

                        L2Event.connectionLossData.put(player.getName(), data);
                        data = null;
                    } else {
                        if (player._inEventCTF) {
                            CTF.onDisconnect(player);
                        } else if (player._inEventDM) {
                            DM.onDisconnect(player);
                        } else if (player._inEventTvT) {
                            TvT.onDisconnect(player);
                        } else if (player._inEventVIP) {
                            VIP.onDisconnect(player);
                        }
                    }

                    if (player.isFlying()) {
                        player.removeSkill(SkillTable.getInstance().getInfo(4289, 1));
                    }

                    if (player.isAway()) {
                        AwayManager.getInstance().extraBack(player);
                    }

                    if (Olympiad.getInstance().isRegistered(player)) {
                        Olympiad.getInstance().unRegisterNoble(player);
                    }

                    //Decrease boxes number
                    if (player._active_boxes != -1)
                        player.decreaseBoxes();

                    // prevent closing again
                    player.setClient(null);

                    player.deleteMe();

                    try {
                        player.store(_forcedToClose);
                    } catch (Exception e2) {
                        LOGGER.error("", e2);
                    }
                }

                L2GameClient.this.setActiveChar(null);
                L2GameClient.this.setDetached(true);

            } catch (Exception e1) {
                LOGGER.error("", e1);

                LOGGER.info("Error while cleanup client.", e1);
            } finally {
                LoginServerThread.getInstance().sendLogout(L2GameClient.this.getAccountName());
            }
        }
    }

    protected class DisconnectTask implements Runnable {
        @Override
        public void run() {
            try {
                // Update BBS
                try {
                    RegionBBSManager.getInstance().changeCommunityBoard();
                } catch (Exception e) {
                    LOGGER.error("unhandled exception", e);
                }

//				// we are going to manually save the char bellow thus we can force the cancel
//				if(_autoSaveInDB != null)
//					_autoSaveInDB.cancel(true);

                L2PcInstance player = L2GameClient.this.getActiveChar();
                if (player != null) // this should only happen on connection loss
                {
                    if (GameServerConfig.ENABLE_OLYMPIAD_DISCONNECTION_DEBUG) {
                        if (player.isInOlympiadMode()
                                || player.inObserverMode()) {
                            if (player.isInOlympiadMode()) {
                                String text = "Player " + player.getName()
                                        + ", Class:" + player.getClassId()
                                        + ", Level:" + player.getLevel()
                                        + ", Mode: Olympiad, Loc: "
                                        + player.getX() + " Y:" + player.getY()
                                        + " Z:" + player.getZ()
                                        + ", Critical?: " + _forcedToClose;
                                Log.add(text, "Olympiad_crash_debug");
                            } else if (player.inObserverMode()) {
                                String text = "Player " + player.getName()
                                        + ", Class:" + player.getClassId()
                                        + ", Level:" + player.getLevel()
                                        + ", Mode: Observer, Loc: "
                                        + player.getX() + " Y:" + player.getY()
                                        + " Z:" + player.getZ()
                                        + ", Critical?: " + _forcedToClose;
                                Log.add(text, "Olympiad_crash_debug");
                            } else {
                                String text = "Player " + player.getName()
                                        + ", Class:" + player.getClassId()
                                        + ", Level:" + player.getLevel()
                                        + ", Mode: Default, Loc: "
                                        + player.getX() + " Y:" + player.getY()
                                        + " Z:" + player.getZ()
                                        + ", Critical?: " + _forcedToClose;
                                Log.add(text, "Olympiad_crash_debug");
                            }
                        }
                    }
                    // we store all data from players who are disconnected while in an event in order to restore it in the next login
                    if (player.atEvent) {
                        EventData data = new EventData(player.eventX, player.eventY, player.eventZ, player.eventKarma, player.eventPvpKills, player.eventPkKills, player.eventTitle, player.kills, player.eventSitForced);

                        L2Event.connectionLossData.put(player.getName(), data);
                        data = null;
                    } else {
                        if (player._inEventCTF) {
                            CTF.onDisconnect(player);
                        } else if (player._inEventDM) {
                            DM.onDisconnect(player);
                        } else if (player._inEventTvT) {
                            TvT.onDisconnect(player);
                        } else if (player._inEventVIP) {
                            VIP.onDisconnect(player);
                        }
                    }

                    if (player.isFlying()) {
                        player.removeSkill(SkillTable.getInstance().getInfo(4289, 1));
                    }

                    if (player.isAway()) {
                        AwayManager.getInstance().extraBack(player);
                    }

                    if (Olympiad.getInstance().isRegistered(player)) {
                        Olympiad.getInstance().unRegisterNoble(player);
                    }

                    //Decrease boxes number
                    if (player._active_boxes != -1)
                        player.decreaseBoxes();


                    if (!player.isKicked() && !Olympiad.getInstance().isRegistered(player)
                            && !player.isInOlympiadMode()
                            && !player.isInFunEvent()
                            && ((player.isInStoreMode() && GameServerConfig.OFFLINE_TRADE_ENABLE)
                            || (player.isInCraftMode() && GameServerConfig.OFFLINE_CRAFT_ENABLE))) {
                        player.setOffline(true);
                        player.leaveParty();
                        player.store();

                        if (GameServerConfig.OFFLINE_SET_NAME_COLOR) {
                            player._originalNameColorOffline = player.getAppearance().getNameColor();
                            player.getAppearance().setNameColor(GameServerConfig.OFFLINE_NAME_COLOR);
                            player.broadcastUserInfo();
                        }

                        if (player.getOfflineStartTime() == 0)
                            player.setOfflineStartTime(System.currentTimeMillis());

                        OfflineTradeTable.storeOffliner(player);

                        return;
                    }

                    // notify the world about our disconnect
                    player.deleteMe();

                    //store operation
                    try {
                        player.store();
                    } catch (Exception e2) {
                        LOGGER.error("", e2);
                    }
                }

                L2GameClient.this.setActiveChar(null);
                L2GameClient.this.setDetached(true);

                player = null;
            } catch (Exception e1) {
                LOGGER.error("", e1);

                LOGGER.info("error while disconnecting client", e1);
            } finally {
                LoginServerThread.getInstance().sendLogout(L2GameClient.this.getAccountName());
            }
        }
    }
}
