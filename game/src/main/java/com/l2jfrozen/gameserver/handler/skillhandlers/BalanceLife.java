/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.skillhandlers;

import com.l2jfrozen.gameserver.handler.ISkillHandler;
import com.l2jfrozen.gameserver.handler.SkillHandler;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.L2Skill.SkillType;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.StatusUpdate;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;
import com.l2jfrozen.util.GArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class ...
 *
 * @author earendil
 * @version $Revision: 1.1.2.2.2.4 $ $Date: 2005/04/06 16:13:48 $
 */

public class BalanceLife implements ISkillHandler<L2Character> {
    private static final SkillType[] SKILL_IDS = {SkillType.BALANCE_LIFE};
    private static Logger LOGGER = LoggerFactory.getLogger(BalanceLife.class.getName());

    @Override
    public <A extends L2Character> void useSkill(A activeChar, L2Skill skill, GArray<L2Character> targets) {

        try {
            ISkillHandler handler = SkillHandler.getInstance().getSkillHandler(SkillType.BUFF);

            if (handler != null) {
                handler.useSkill(activeChar, skill, targets);
            }
        } catch (Exception e) {
            LOGGER.error("unhandled exception {}", e);
        }

        L2PcInstance player = null;
        if (activeChar instanceof L2PcInstance) {
            player = (L2PcInstance) activeChar;
        }

        double fullHP = 0;
        double currentHPs = 0;

        for (L2Character target : targets) {

            // We should not heal if char is dead
            if (target == null || target.isDead()) {
                continue;
            }

            // Player holding a cursed weapon can't be healed and can't heal
            if (target != activeChar) {
                if (target instanceof L2PcInstance && ((L2PcInstance) target).isCursedWeaponEquiped()) {
                    continue;
                } else if (player != null && player.isCursedWeaponEquiped()) {
                    continue;
                }
            }

            player = null;

            fullHP += target.getMaxHp();
            currentHPs += target.getCurrentHp();
        }

        double percentHP = currentHPs / fullHP;

        for (L2Character target : targets) {

            if (target == null || target.isDead()) {
                continue;
            }

            double newHP = target.getMaxHp() * percentHP;
            double totalHeal = newHP - target.getCurrentHp();

            target.setCurrentHp(newHP);

            if (totalHeal > 0) {
                target.setLastHealAmount((int) totalHeal);
            }

            StatusUpdate su = new StatusUpdate(target.getObjectId());
            su.addAttribute(StatusUpdate.CUR_HP, (int) target.getCurrentHp());
            target.sendPacket(su);

            SystemMessage sm = new SystemMessage(SystemMessageId.S1_S2);
            sm.addString("HP of the party has been balanced.");
            target.sendPacket(sm);

        }
    }

    @Override
    public SkillType[] getSkillIds() {
        return SKILL_IDS;
    }
}
