/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.handler.voicedcommandhandlers;

import com.l2jfrozen.gameserver.OfflineManager;
import com.l2jfrozen.gameserver.handler.IVoicedCommandHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;

/**
 * Command .offline_shop
 *
 * @author Nefer
 */
public class OfflineShopCmd implements IVoicedCommandHandler {
    private static String[] _voicedCommands =
            {
                    "offline_shop"
            };

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean useVoicedCommand(String command, L2PcInstance player, String target) {
        return OfflineManager.getInstance().offlineUser(player);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getVoicedCommandList() {
        return _voicedCommands;
    }
}