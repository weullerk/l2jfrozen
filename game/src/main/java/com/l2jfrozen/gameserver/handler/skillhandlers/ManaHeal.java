/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.skillhandlers;

import com.l2jfrozen.gameserver.handler.ISkillHandler;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.L2Skill.SkillType;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.StatusUpdate;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;
import com.l2jfrozen.gameserver.skills.Stats;
import com.l2jfrozen.util.GArray;

/**
 * This class ...
 *
 * @version $Revision: 1.1.2.2.2.1 $ $Date: 2005/03/02 15:38:36 $
 */

public class ManaHeal implements ISkillHandler<L2Character> {

    private static final SkillType[] SKILL_IDS = {
            SkillType.MANAHEAL,
            SkillType.MANARECHARGE,
            SkillType.MANAHEAL_PERCENT};


    @Override
    public <A extends L2Character> void useSkill(A activeChar, L2Skill skill, GArray<L2Character> targets) {
        for (L2Character target : targets) {

            // We should not heal if char is dead or invulnerable (celestial?)
            if (target == null || target.isDead() || target.isInvul()) {
                continue;
            }

            double mp = skill.getPower();
            if (skill.getSkillType() == SkillType.MANAHEAL_PERCENT) {
                //double mp = skill.getPower();
                mp = target.getMaxMp() * mp / 100.0;
            } else {
                mp = (skill.getSkillType() == SkillType.MANARECHARGE) ? target.calcStat(Stats.RECHARGE_MP_RATE, mp, null, null) : mp;
            }

            target.setLastHealAmount((int) mp);
            target.setCurrentMp(mp + target.getCurrentMp());
            StatusUpdate sump = new StatusUpdate(target.getObjectId());
            sump.addAttribute(StatusUpdate.CUR_MP, (int) target.getCurrentMp());
            target.sendPacket(sump);

            if (activeChar instanceof L2PcInstance && activeChar != target) {
                SystemMessage sm = new SystemMessage(SystemMessageId.S2_MP_RESTORED_BY_S1);
                sm.addString(activeChar.getName());
                sm.addNumber((int) mp);
                target.sendPacket(sm);
            } else {
                SystemMessage sm = new SystemMessage(SystemMessageId.S1_MP_RESTORED);
                sm.addNumber((int) mp);
                target.sendPacket(sm);
            }
        }
        if (!skill.isPotion()) {
            if (activeChar.checkBss()) {
                activeChar.removeBss();
            }
            if (activeChar.checkSps()) {
                activeChar.removeSps();
            }
        }
    }

    @Override
    public SkillType[] getSkillIds() {
        return SKILL_IDS;
    }
}
