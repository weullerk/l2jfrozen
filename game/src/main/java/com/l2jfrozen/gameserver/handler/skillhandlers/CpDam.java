/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.skillhandlers;

import com.l2jfrozen.gameserver.handler.ISkillHandler;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.L2Skill.SkillType;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PlayableInstance;
import com.l2jfrozen.gameserver.skills.Formulas;
import com.l2jfrozen.util.GArray;

public class CpDam implements ISkillHandler<L2PlayableInstance> {

    private static final SkillType[] SKILL_IDS = {SkillType.CPDAM};

    @Override
    public <A extends L2Character> void useSkill(A activeChar, L2Skill skill, GArray<L2PlayableInstance> targets) {
        if (!(activeChar instanceof L2PlayableInstance)) {
            return;
        }

        if (activeChar.isAlikeDead()) {
            return;
        }

        boolean bss = activeChar.checkBss();
        boolean sps = activeChar.checkSps();
        boolean ss = activeChar.checkSs();

        for (L2PlayableInstance target : targets) {

            if (activeChar instanceof L2PcInstance && target instanceof L2PcInstance && target.isAlikeDead() && target.isFakeDeath()) {
                target.stopFakeDeath(null);
            } else if (target.isAlikeDead()) {
                continue;
            }

            if (target.isInvul()) {
                continue;
            }

            if (!Formulas.getInstance().calcSkillSuccess(activeChar, target, skill, ss, sps, bss)) {
                return;
            }

            int damage = (int) (((L2PcInstance) target).getCurrentCp() * (1 - skill.getPower()));

            // Manage attack or cast break of the target (calculating rate, sending message...)
            if (!target.isRaid() && Formulas.calcAtkBreak(target, damage)) {
                target.breakAttack();
                target.breakCast();
            }
            skill.getEffects(activeChar, target, ss, sps, bss);
            activeChar.sendDamageMessage(target, damage, false, false, false);
            ((L2PcInstance) target).setCurrentCp(((L2PcInstance) target).getCurrentCp() - damage);

        }
        if (skill.isMagic()) {
            if (bss) {
                activeChar.removeBss();
            } else if (sps) {
                activeChar.removeSps();
            }

        } else {

            activeChar.removeSs();

        }
    }

    @Override
    public SkillType[] getSkillIds() {
        return SKILL_IDS;
    }
}
