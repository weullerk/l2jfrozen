/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.model.actor.status;

import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.actor.instance.L2PlayableInstance;
import com.l2jfrozen.gameserver.skills.Formulas;

public class PlayableStatus extends CharStatus {
    // =========================================================
    // Data Field
    /**
     * The _current cp.
     */
    private double _currentCp = 0; //Current CP of the L2Character

    // =========================================================
    // Constructor
    public PlayableStatus(L2PlayableInstance activeChar) {
        super(activeChar);
    }

    // =========================================================
    // Method - Public
    @Override
    public void reduceHp(double value, L2Character attacker) {
        reduceHp(value, attacker, true);
    }

    @Override
    public void reduceHp(double value, L2Character attacker, boolean awake) {
        if (getActiveChar().isDead())
            return;

        super.reduceHp(value, attacker, awake);
    }

    /**
     * Gets the current cp.
     *
     * @return the current cp
     */
    @Override
    public final double getCurrentCp() {
        return _currentCp;
    }

    /**
     * Sets the current cp.
     *
     * @param newCp the new current cp
     */
    public final void setCurrentCp(double newCp) {
        setCurrentCp(newCp, true, false);
    }

    @Override
    public L2PlayableInstance getActiveChar() {
        return (L2PlayableInstance) super.getActiveChar();
    }

    /**
     * Sets the current cp direct.
     *
     * @param newCp the new current cp direct
     */
    public final void setCurrentCpDirect(double newCp) {
        setCurrentCp(newCp, true, true);
    }

    /**
     * Sets the current cp.
     *
     * @param newCp           the new cp
     * @param broadcastPacket the broadcast packet
     */
    public final void setCurrentCp(double newCp, boolean broadcastPacket) {
        setCurrentCp(newCp, broadcastPacket, false);
    }

    /**
     * Sets the current cp.
     *
     * @param newCp           the new cp
     * @param broadcastPacket the broadcast packet
     * @param direct          the direct
     */
    public final void setCurrentCp(double newCp, boolean broadcastPacket, boolean direct) {
        synchronized (this) {
            // Get the Max CP of the L2Character
            int maxCp = getActiveChar().getStat().getMaxCp();

            if (newCp < 0) {
                newCp = 0;
            }

            if (newCp >= maxCp && !direct) {
                // Set the RegenActive flag to false
                _currentCp = maxCp;
                _flagsRegenActive &= ~REGEN_FLAG_CP;

                // Stop the HP/MP/CP Regeneration task
                if (_flagsRegenActive == 0) {
                    stopHpMpRegeneration();
                }
            } else {
                // Set the RegenActive flag to true
                _currentCp = newCp;
                _flagsRegenActive |= REGEN_FLAG_CP;

                // Start the HP/MP/CP Regeneration task with Medium priority
                startHpMpRegeneration();
            }
        }

        // Send the Server->Client packet StatusUpdate with current HP and MP to all other L2PcInstance to inform
        if (broadcastPacket) {
            getActiveChar().broadcastStatusUpdate();
        }
    }

    /**
     * Reduce cp.
     *
     * @param value the value
     */
    public final void reduceCp(int value) {
        if (getCurrentCp() > value) {
            setCurrentCp(getCurrentCp() - value);
        } else {
            setCurrentCp(0);
        }
    }

    @Override
    protected void doRegenState() {
        // Modify the current CP of the L2Character and broadcast Server->Client packet StatusUpdate
        if (getCurrentCp() < getActiveChar().getStat().getMaxCp()) {
            setCurrentCp(getCurrentCp() + Formulas.calcCpRegen(getActiveChar()), false);
        }
    }

    @Override
    protected void doStopRegen() {
        // no broadcast necessary for characters that are in inactive regions.
        // stop regeneration for characters who are filled up and in an inactive region.
        if (getCurrentCp() == getActiveChar().getStat().getMaxCp() && getCurrentHp() == getActiveChar().getStat().getMaxHp() && getCurrentMp() == getActiveChar().getStat().getMaxMp()) {
            stopHpMpRegeneration();
        }
    }
}
