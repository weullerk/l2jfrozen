/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.model;

import com.l2jfrozen.database.exception.ManagerException;
import com.l2jfrozen.database.manager.CommonManager;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterShortcut;
import com.l2jfrozen.gameserver.model.actor.instance.L2ItemInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.serverpackets.ExAutoSoulShot;
import com.l2jfrozen.gameserver.network.serverpackets.ShortCutInit;
import com.l2jfrozen.gameserver.templates.L2EtcItemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * This class ...
 *
 * @version $Revision: 1.1.2.1.2.3 $ $Date: 2005/03/27 15:29:33 $
 */
public class ShortCuts {
    private static Logger LOGGER = LoggerFactory.getLogger(ShortCuts.class.getName());

    private L2PcInstance _owner;
    private Map<Integer, L2ShortCut> _shortCuts = new TreeMap<Integer, L2ShortCut>();

    public ShortCuts(L2PcInstance owner) {
        _owner = owner;
    }

    public synchronized void deleteShortCutByObjectId(int objectId) {
        L2ShortCut toRemove = null;

        for (L2ShortCut shortcut : _shortCuts.values()) {
            if (shortcut.getType() == L2ShortCut.TYPE_ITEM && shortcut.getId() == objectId) {
                toRemove = shortcut;
                break;
            }
        }

        if (toRemove != null) {
            deleteShortCut(toRemove.getSlot(), toRemove.getPage());
        }

        toRemove = null;
    }

    /**
     * @param slot
     * @param page
     */
    public synchronized void deleteShortCut(int slot, int page) {
        L2ShortCut old = _shortCuts.remove(slot + page * 12);

        if (old == null || _owner == null)
            return;

        deleteShortCutFromDb(old);

        if (old.getType() == L2ShortCut.TYPE_ITEM) {
            L2ItemInstance item = _owner.getInventory().getItemByObjectId(old.getId());

            if (item != null && item.getItemType() == L2EtcItemType.SHOT) {
                _owner.removeAutoSoulShot(item.getItemId());
                _owner.sendPacket(new ExAutoSoulShot(item.getItemId(), 0));
            }

            item = null;
        }

        _owner.sendPacket(new ShortCutInit(_owner));

        for (int shotId : _owner.getAutoSoulShot().values()) {
            _owner.sendPacket(new ExAutoSoulShot(shotId, 1));
        }

        old = null;
    }

    /**
     * @param shortcut
     */
    private void deleteShortCutFromDb(L2ShortCut shortcut) {
        try {
            Iterator<Map.Entry<Long, CharacterShortcut>> iterator = _owner.getCharacter().getShortcuts().entrySet().iterator();
            CharacterShortcut shortcut1 = null;
            while (iterator.hasNext()) {
                Map.Entry<Long, CharacterShortcut> item = iterator.next();
                if (item.getValue().getClassIndex() == _owner.getClassIndex() &&
                        item.getValue().getSlot() == shortcut.getSlot() &&
                        item.getValue().getPage() == shortcut.getPage()) {
                    shortcut1 = item.getValue();
                    iterator.remove();
                }
            }
            CommonManager.getInstance().delete(shortcut1);
        } catch (ManagerException e) {
            LOGGER.warn("Could not delete character shortcut: " + e);
        }
    }

    public L2ShortCut getShortCut(int slot, int page) {
        L2ShortCut sc = _shortCuts.get(slot + page * 12);

        // verify shortcut
        if (sc != null && sc.getType() == L2ShortCut.TYPE_ITEM) {
            if (_owner.getInventory().getItemByObjectId(sc.getId()) == null) {
                deleteShortCut(sc.getSlot(), sc.getPage());
                sc = null;
            }
        }

        return sc;
    }

    public synchronized void registerShortCut(L2ShortCut shortcut) {
        L2ShortCut oldShortCut = _shortCuts.put(shortcut.getSlot() + 12 * shortcut.getPage(), shortcut);
        registerShortCutInDb(shortcut, oldShortCut);
        oldShortCut = null;
    }

    private void registerShortCutInDb(L2ShortCut shortcut, L2ShortCut oldShortCut) {
        if (oldShortCut != null) {
            deleteShortCutFromDb(oldShortCut);
        }
        CharacterShortcut shortcut1 = null;
        try {
            shortcut1 = new CharacterShortcut();
            shortcut1.setCharacter(_owner.getCharacter());
            shortcut1.setSlot(shortcut.getSlot());
            shortcut1.setPage(shortcut.getPage());
            shortcut1.setType(shortcut.getType());
            shortcut1.setLevel(String.valueOf(shortcut.getLevel()));
            shortcut1.setClassIndex(_owner.getClassIndex());
            shortcut1.setShortcutId(shortcut.getId());
            CommonManager.getInstance().saveNew(shortcut1);
            _owner.getCharacter().getShortcuts().put(shortcut1.getId(), shortcut1);
        } catch (Exception e) {
            LOGGER.warn("Could not store character shortcut: " + e);
        }
    }

    public void restore() {
        _shortCuts.clear();
        try {
            CharacterEntity characterEntity = _owner.getCharacter();
            for (CharacterShortcut shortcut : characterEntity.getShortcuts().values()) {
                if (shortcut.getClassIndex() != _owner.getClassIndex()) {
                    return;
                }
                int slot = shortcut.getSlot();
                int page = shortcut.getPage();
                int type = shortcut.getType();
                int id = shortcut.getShortcutId();
                int level = Integer.decode(shortcut.getLevel());

                L2ShortCut sc = new L2ShortCut(slot, page, type, id, level, 1);
                _shortCuts.put(slot + page * 12, sc);
            }
        } catch (Exception e) {
            LOGGER.warn("Could not restore character shortcuts: " + e);
        }


        // verify shortcuts
        for (L2ShortCut sc : getAllShortCuts()) {
            if (sc.getType() == L2ShortCut.TYPE_ITEM) {
                if (!_owner.getCharacter().getItems().containsKey(((Number) sc.getId()).longValue())) {
                    deleteShortCut(sc.getSlot(), sc.getPage());
                }
            }
        }
    }

    public L2ShortCut[] getAllShortCuts() {
        return _shortCuts.values().toArray(new L2ShortCut[_shortCuts.values().size()]);
    }
}
