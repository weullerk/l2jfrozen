/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.model.actor.instance;

import com.l2jfrozen.gameserver.managers.CustomNpcInstanceManager;
import com.l2jfrozen.gameserver.model.base.ClassId;
import com.l2jfrozen.gameserver.templates.L2WeaponType;
import com.l2jfrozen.util.random.Rnd;

/**
 * This class manages Npc Polymorph into player instances, they look like regular players. This effect will show up on
 * all clients.
 *
 * @author Darki699
 */
public final class L2CustomNpcInstance {

    private boolean _allowRandomWeapons = true; //Default value
    private boolean _allowRandomClass = true; //Default value
    private boolean _allowRandomAppearance = true; //Default value
    private String _name;
    private String _title;
    private CustomNpcInstanceManager.CustomNpc customNpc;

    private L2NpcInstance polymorphicNbc; // Reference to Npc with this stats
    private ClassId _classId; // ClassId of this (N)Pc

    /**
     * A constructor
     *
     * @param myNpc - Receives the L2NpcInstance as a reference.
     */
    public L2CustomNpcInstance(L2NpcInstance myNpc) {
        polymorphicNbc = myNpc;
        if (polymorphicNbc == null) {
            return;
        } else if (polymorphicNbc.getSpawn() == null) {
            return;
        } else {
            initialize();
        }
    }

    /**
     * Initializes the semi PcInstance stats for this NpcInstance, making it appear as a PcInstance on all clients
     */
    private final void initialize() {

        // karma=1, clanId=2, allyId=3, clanCrest=4, allyCrest=5, race=6, classId=7
        // EnchantWeapon=8, PledgeClass=9, CursedWeaponLevel=10
        // RightHand=11, LeftHand=12, Gloves=13, Chest=14, Legs=15, Feet=16, Hair1=17, Hair2=18
        // HairStyle=19, HairColor=20, Face=21
        // NameColor=22, TitleColor=23
        //pvp=0 , noble=1, hero=2, isFemaleSex=3

        // load the Pc Morph Data
        CustomNpcInstanceManager.CustomNpc ci = CustomNpcInstanceManager.getInstance().getCustomData(polymorphicNbc.getSpawn().getId(), polymorphicNbc.getNpcId());

        if (ci == null) {
            polymorphicNbc.setCustomNpcInstance(null);
            polymorphicNbc = null;
            return;
        }

        polymorphicNbc.setCustomNpcInstance(this);

        setPcInstanceData(ci);

        if (_allowRandomClass) {
            chooseRandomClass();
        }
        if (_allowRandomAppearance) {
            chooseRandomAppearance();
        }
        if (_allowRandomWeapons) {
            chooseRandomWeapon();
        }

        ci = null;
    }

    /**
     * @return the custom npc's name, or the original npc name if no custom name is provided
     */
    public final String getName() {
        return _name == null ? polymorphicNbc.getName() : _name;
    }

    /**
     * @return the custom npc's title, or the original npc title if no custom title is provided
     */
    public final String getTitle() {
        return _title == null ? polymorphicNbc.getTitle() : polymorphicNbc.isChampion() ? "The Champion" + _title : _title;
    }

    /**
     * @return the npc's karma or aggro range if he has any...
     */
    public final int getKarma() {
        return customNpc.getKarma() > 0 ? customNpc.getKarma() : polymorphicNbc.getAggroRange();
    }

    /**
     * @return the clan Id
     */
    public final int getClanId() {
        return customNpc.getClanId();
    }

    /**
     * @return the ally Id
     */
    public final int getAllyId() {
        return customNpc.getAllyId();
    }

    /**
     * @return the clan crest Id
     */
    public final int getClanCrestId() {
        return customNpc.getClanCrest();
    }

    /**
     * @return the ally crest Id
     */
    public final int getAllyCrestId() {
        return customNpc.getAllyCrest();
    }

    /**
     * @return the Race ordinal
     */
    public final int getRace() {
        return customNpc.getRace();
    }

    /**
     * @return the class id, e.g.: fighter, warrior, mystic muse...
     */
    public final int getClassId() {
        return customNpc.getClassId();
    }

    /**
     * @return the enchant level of the equipped weapon, if one is equipped (max = 127)
     */
    public final int getEnchantWeapon() {
        return PAPERDOLL_RHAND() == 0 || getCursedWeaponLevel() != 0 ? 0 : customNpc.getWpnEnchant() > 127 ? 127 : customNpc.getWpnEnchant();
    }

    /**
     * @return the pledge class identifier, e.g. vagabond, baron, marquiz
     * @remark Champion mobs are always Marquiz
     */
    public final int getPledgeClass() {
        return polymorphicNbc.isChampion() ? 8 : customNpc.getPledge();
    }

    /**
     * @return the cursed weapon level, if one is equipped
     */
    public final int getCursedWeaponLevel() {
        return PAPERDOLL_RHAND() == 0 || customNpc.getWpnEnchant() > 0 ? 0 : customNpc.getCwLevel();
    }

    /**
     * @return the item id for the item in the right hand, if a custom item is not equipped the value returned is the
     * original npc right-hand weapon id
     */
    public final int PAPERDOLL_RHAND() {
        return customNpc.getRightHand() != 0 ? customNpc.getRightHand() : polymorphicNbc.getRightHandItem();
    }

    /**
     * @return the item id for the item in the left hand, if a custom item is not equipped the value returned is the
     * original npc left-hand weapon id. Setting this value _int[12] = -1 will not allow a npc to have anything
     * in the left hand
     */
    public final int PAPERDOLL_LHAND() {
        return customNpc.getLeftHand() > 0 ? customNpc.getLeftHand() : customNpc.getLeftHand() == 0 ? polymorphicNbc.getLeftHandItem() : 0;
    }

    /**
     * @return the item id for the gloves
     */
    public final int PAPERDOLL_GLOVES() {
        return customNpc.getGloves();
    }

    /**
     * @return the item id for the chest armor
     */
    public final int PAPERDOLL_CHEST() {
        return customNpc.getChest();
    }

    /**
     * @return the item id for the leg armor, or 0 if wearing a full armor
     */
    public final int PAPERDOLL_LEGS() {
        return customNpc.getLegs();
    }

    /**
     * @return the item id for feet armor
     */
    public final int PAPERDOLL_FEET() {
        return customNpc.getFeet();
    }

    /**
     * @return the item id for the 1st hair slot, or all hair
     */
    public final int PAPERDOLL_HAIR() {
        return customNpc.getHair();
    }

    /**
     * @return the item id for the 2nd hair slot
     */
    public final int PAPERDOLL_HAIR2() {
        return customNpc.getHair2();
    }

    /**
     * @return the npc's hair style appearance
     */
    public final int getHairStyle() {
        return customNpc.getHairStyle();
    }

    /**
     * @return the npc's hair color appearance
     */
    public final int getHairColor() {
        return customNpc.getHairColor();
    }

    /**
     * @return the npc's face appearance
     */
    public final int getFace() {
        return customNpc.getFace();
    }

    /**
     * @return the npc's name color (in hexadecimal), 0xFFFFFF is the default value
     */
    public final int nameColor() {
        return customNpc.getNameColor() == 0 ? 0xFFFFFF : customNpc.getNameColor();
    }

    /**
     * @return the npc's title color (in hexadecimal), 0xFFFF77 is the default value
     */
    public final int titleColor() {
        return customNpc.getTitleColor() == 0 ? 0xFFFF77 : customNpc.getTitleColor();
    }

    /**
     * @return is npc in pvp mode?
     */
    public final boolean getPvpFlag() {
        return customNpc.isPvp();
    }

    /**
     * @return is npc in pvp mode?
     */
    public final int getHeading() {
        return polymorphicNbc.getHeading();
    }

    /**
     * @return true if npc is a noble
     */
    public final boolean isNoble() {
        return customNpc.isNoble();
    }

    /**
     * @return true if hero glow should show up
     * @remark A Champion mob will always have hero glow
     */
    public final boolean isHero() {
        return polymorphicNbc.isChampion() || customNpc.isHero();
    }

    /**
     * @return true if female, false if male
     * @remark In the DB, if you set
     * @MALE value=0
     * @FEMALE value=1
     * @MAYBE value=2 % chance for the <b>Entire Template</b> to become male or female (it's a maybe value) If female,
     * all template will be female, if Male, all template will be male
     */
    public final boolean isFemaleSex() {
        return customNpc.isFemale();
    }

    /**
     * Choose a random weapon for this L2CustomNpcInstance
     */
    private final void chooseRandomWeapon() {
        L2WeaponType wpnType = null;

        wpnType = Rnd.get(100) > 40 ? L2WeaponType.BOW : L2WeaponType.BOW;

        while (true) {
            wpnType = L2WeaponType.values()[Rnd.get(L2WeaponType.values().length)];
            if (wpnType == null) {
                continue;
            }
            break;
        }
    }

    /**
     * Choose a random class & race for this L2CustomNpcInstance
     */
    private final void chooseRandomClass() {
        while (true) {
            _classId = ClassId.values()[Rnd.get(ClassId.values().length)];
            if (_classId == null) {
            } else if (_classId.getRace() != null && _classId.getParent() != null) {
                break;
            }
        }
        customNpc.setRace(_classId.getRace().ordinal());
        customNpc.setClassId(_classId.getId());
    }

    /**
     * Choose random appearance for this L2CustomNpcInstance
     */
    private final void chooseRandomAppearance() {
        // Karma=1, PledgeClass=9
        // HairStyle=19, HairColor=20, Face=21
        // NameColor=22, TitleColor=23
        // noble=1, hero=2, isFemaleSex=3
        customNpc.setKarma(Rnd.get(100));
        customNpc.setFemale(Rnd.get(100) < 50);
        customNpc.setHairColor(0);
        customNpc.setTitleColor(0);
        if (Rnd.get(100) < 5) {
            customNpc.setNameColor(0x0000FF);
        } else if (Rnd.get(100) < 5) {
            customNpc.setNameColor(0x00FF00);
        }
        if (Rnd.get(100) < 5) {
            customNpc.setNameColor(0x0000FF);
        } else if (Rnd.get(100) < 5) {
            customNpc.setNameColor(0x00FF00);
        }
        customNpc.setHairColor(Rnd.get(100) > 95 ? 0 : Rnd.get(100) > 10 ? 50 : 1000);
        customNpc.setHairStyle(Rnd.get(100) < 34 ? 0 : Rnd.get(100) < 34 ? 1 : 2);
        customNpc.setHairColor(Rnd.get(100) < 34 ? 0 : Rnd.get(100) < 34 ? 1 : 2);
        customNpc.setFace(Rnd.get(100) < 34 ? 0 : Rnd.get(100) < 34 ? 1 : 2);

        int pledgeLevel = Rnd.get(100);
        // 30% is left for either pledge=0 or default sql data
        // Only Marqiz are Champion mobs
        if (pledgeLevel > 30) {
            customNpc.setPledge(1);
        }
        if (pledgeLevel > 50) {
            customNpc.setPledge(2);
        }
        if (pledgeLevel > 60) {
            customNpc.setPledge(3);
        }
        if (pledgeLevel > 80) {
            customNpc.setPledge(4);
        }
        if (pledgeLevel > 90) {
            customNpc.setPledge(5);
        }
        if (pledgeLevel > 95) {
            customNpc.setPledge(6);
        }
        if (pledgeLevel > 98) {
            customNpc.setPledge(7);
        }
    }

    /**
     * Sets the data received from the CustomNpcInstanceManager
     *
     * @param npc the CustomInfo data
     */
    public void setPcInstanceData(CustomNpcInstanceManager.CustomNpc npc) {
        if (npc == null) {
            return;
        }

        // random variables to apply to this L2NpcInstance polymorph
        _allowRandomClass = npc.isRndClass();
        _allowRandomAppearance = npc.isRndAppearance();
        _allowRandomWeapons = npc.isRndWeapon();

        // name & title override
        _name = npc.getName();
        _title = npc.getTitle();
        if (_name != null && _name.equals("")) {
            _name = null;
        }
        if (_title != null && _title.equals("")) {
            _title = null;
        }

        // Not really necessary but maybe called upon on wrong random settings:
        // Initiate this PcInstance class id to the correct pcInstance class.
        ClassId ids[] = ClassId.values();
        if (ids != null) {
            for (ClassId id : ids) {
                if (id == null) {
                    continue;
                } else if (id.getId() == npc.getClassId()) {
                    _classId = id;
                    npc.setRace(id.getRace().ordinal());
                    break;
                }
            }
        }
        customNpc = npc;
    }
}
