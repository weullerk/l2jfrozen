/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.model.actor.instance;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.database.manager.CommonManager;
import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.ItemLocation;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterItemEntity;
import com.l2jfrozen.gameserver.ai.CtrlIntention;
import com.l2jfrozen.gameserver.datatables.sql.ItemTable;
import com.l2jfrozen.gameserver.datatables.sql.L2PetDataTable;
import com.l2jfrozen.gameserver.geo.GeoData;
import com.l2jfrozen.gameserver.idfactory.IdFactory;
import com.l2jfrozen.gameserver.logger.ItemAudit;
import com.l2jfrozen.gameserver.managers.ItemsOnGroundManager;
import com.l2jfrozen.gameserver.model.*;
import com.l2jfrozen.gameserver.model.actor.knownlist.NullKnownList;
import com.l2jfrozen.gameserver.model.extender.BaseExtender.EventType;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.InventoryUpdate;
import com.l2jfrozen.gameserver.network.serverpackets.StatusUpdate;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;
import com.l2jfrozen.gameserver.skills.funcs.Func;
import com.l2jfrozen.gameserver.templates.L2Armor;
import com.l2jfrozen.gameserver.templates.L2EtcItem;
import com.l2jfrozen.gameserver.templates.L2Item;
import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import com.l2jfrozen.util.CloseUtil;
import com.l2jfrozen.util.database.L2DatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.ScheduledFuture;

/**
 * This class manages items.
 *
 * @version $Revision: 1.4.2.1.2.11 $ $Date: 2005/03/31 16:07:50 $
 */
public final class L2ItemInstance extends L2Object {

    /**
     * The Constant CHARGED_NONE.
     */
    public static final int CHARGED_NONE = 0;
    /**
     * The Constant CHARGED_SOULSHOT.
     */
    public static final int CHARGED_SOULSHOT = 1;
    /**
     * The Constant CHARGED_SPIRITSHOT.
     */
    public static final int CHARGED_SPIRITSHOT = 1;
    /**
     * The Constant CHARGED_BLESSED_SOULSHOT.
     */
    public static final int CHARGED_BLESSED_SOULSHOT = 2; // It's a realy exists? ;-)
    /**
     * The Constant CHARGED_BLESSED_SPIRITSHOT.
     */
    public static final int CHARGED_BLESSED_SPIRITSHOT = 2;
    /**
     * The Constant UNCHANGED.
     */
    public static final int UNCHANGED = 0;
    /**
     * The Constant ADDED.
     */
    public static final int ADDED = 1;
    /**
     * The Constant REMOVED.
     */
    public static final int REMOVED = 3;
    /**
     * The Constant MODIFIED.
     */
    public static final int MODIFIED = 2;
    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(L2ItemInstance.class.getName());
    /**
     * The Constant LOGGERItems.
     */
    private static final Logger LOGGERItems = LoggerFactory.getLogger("item");
    /**
     * The Constant MANA_CONSUMPTION_RATE.
     */
    private static final int MANA_CONSUMPTION_RATE = 60000;
    /**
     * The _drop protection.
     */
    private final DropProtection _dropProtection = new DropProtection();
    /**
     * ID of the item.
     */
    private final int _itemId;
    /**
     * Object L2Item associated to the item.
     */
    private final L2Item _item;
    /**
     * ID of the owner.
     */
    private int _ownerId;
    /**
     * Quantity of the item.
     */
    private int _count;
    /**
     * Initial Quantity of the item.
     */
    private int _initCount;
    /**
     * Time after restore Item count (in Hours).
     */
    private int _time;
    /**
     * Quantity of the item can decrease.
     */
    private boolean _decrease = false;
    /**
     * Location of the item : Inventory, PaperDoll, WareHouse.
     */
    private ItemLocation _loc;
    /**
     * Slot where item is stored.
     */
    private int _locData;
    /**
     * Level of enchantment of the item.
     */
    private int _enchantLevel;
    /**
     * Price of the item for selling.
     */
    private int _priceSell;
    /**
     * Price of the item for buying.
     */
    private int _priceBuy;
    /**
     * Wear Item.
     */
    private boolean _wear;
    /**
     * Augmented Item.
     */
    private L2Augmentation _augmentation = null;
    /**
     * Shadow item.
     */
    private int _mana = -1;
    /**
     * The _consuming mana.
     */
    private boolean _consumingMana = false;
    /**
     * Custom item types (used loto, race tickets).
     */
    private int _type1;
    /**
     * The _type2.
     */
    private int _type2;
    /**
     * The _drop time.
     */
    private long _dropTime;
    /**
     * Item charged with SoulShot (type of SoulShot).
     */
    private int _chargedSoulshot = CHARGED_NONE;
    /**
     * Item charged with SpiritShot (type of SpiritShot).
     */
    private int _chargedSpiritshot = CHARGED_NONE;
    /**
     * The _charged fishtshot.
     */
    private boolean _chargedFishtshot = false;
    /**
     * The _protected.
     */
    private boolean _protected;
    /**
     * The _last change.
     */
    private int _lastChange = 2; //1 ??, 2 modified, 3 removed
    /**
     * The _exists in db.
     */
    private boolean _existsInDb; // if a record exists in DB.
    /**
     * The _stored in db.
     */
    private boolean _storedInDb; // if DB data is up-to-date.
    /**
     * The item loot shedule.
     */
    private ScheduledFuture<?> itemLootShedule = null;


    /**
     * Constructor of the L2ItemInstance from the objectId and the itemId.
     *
     * @param objectId : int designating the ID of the object in the world
     * @param itemId   : int designating the ID of the item
     * @throws IllegalArgumentException the illegal argument exception
     */
    private L2ItemInstance(int objectId, int itemId) throws IllegalArgumentException {
        this(objectId, ItemTable.getInstance().getTemplate(itemId));
    }

    /**
     * Constructor of the L2ItemInstance from the objetId and the description of the item given by the L2Item.
     *
     * @param objectId : int designating the ID of the object in the world
     * @param item     : L2Item containing informations of the item
     * @throws IllegalArgumentException the illegal argument exception
     */
    private L2ItemInstance(int objectId, L2Item item) throws IllegalArgumentException {
        super(objectId);

        if (item == null)
            throw new IllegalArgumentException();

        super.setKnownList(new NullKnownList(this));

        _itemId = item.getItemId();
        _item = item;
        _count = 1;
        _loc = ItemLocation.VOID;
        _mana = _item.getDuration();
    }

    /**
     * Returns a L2ItemInstance stored in database from its objectID.
     *
     * @param charItem : int designating the objectID of the item
     * @return L2ItemInstance
     */
    public static L2ItemInstance restoreFromDb(CharacterItemEntity charItem) {
        L2ItemInstance inst = null;
        Connection con = null;

        try {

            PreparedStatement statement = null;
            ResultSet rs = null;


            int owner_id = charItem.getCharacter().getId().intValue();
            int item_id = charItem.getItemId();
            int count = charItem.getCount();

            ItemLocation loc = charItem.getLoc();

            int loc_data = charItem.getLocData();
            int enchant_level = charItem.getEnchantLevel();
            int custom_type1 = charItem.getCustomType1();
            int custom_type2 = charItem.getCustomType2();
            int price_sell = charItem.getPriceSell();
            int price_buy = charItem.getPriceBuy();
            int manaLeft = charItem.getManaLeft();

            L2Item item = ItemTable.getInstance().getTemplate(item_id);

            if (item == null) {
                LOGGER.error("Item item_id=" + item_id + " not known, object_id=" + charItem.getId());
                return null;
            }

            inst = new L2ItemInstance(charItem.getId().intValue(), item);
            inst._existsInDb = true;
            inst._storedInDb = true;
            inst._ownerId = owner_id;
            inst._count = count;
            inst._enchantLevel = enchant_level;
            inst._type1 = custom_type1;
            inst._type2 = custom_type2;
            inst._loc = loc;
            inst._locData = loc_data;
            inst._priceSell = price_sell;
            inst._priceBuy = price_buy;

            // Setup life time for shadow weapons
            inst._mana = manaLeft;

            // consume 1 mana
            if (inst._mana > 0 && inst.getLocation() == ItemLocation.PAPERDOLL)
                inst.decreaseMana(false);

            // if mana left is 0 delete this item
            if (inst._mana == 0) {
                inst.removeFromDb();
                return null;
            } else if (inst._mana > 0 && inst.getLocation() == ItemLocation.PAPERDOLL) {
                inst.scheduleConsumeManaTask();
            }

            loc = null;
            item = null;

            //load augmentation
            con = L2DatabaseFactory.getInstance().getConnection(false);
            statement = con.prepareStatement("SELECT attributes,skill,level FROM augmentations WHERE item_id=?");
            statement.setInt(1, charItem.getId().intValue());
            rs = statement.executeQuery();

            if (rs.next())
                inst._augmentation = new L2Augmentation(inst, rs.getInt("attributes"), rs.getInt("skill"), rs.getInt("level"), false);

            rs.close();
            statement.close();
            rs = null;
            statement = null;
        } catch (Exception e) {
            if (GameServerConfig.ENABLE_ALL_EXCEPTIONS)
                e.printStackTrace();

            LOGGER.info("Could not restore item " + charItem.getId() + " from DB:", e);
        } finally {
            CloseUtil.close(con);

        }

        if (inst != null)
            inst.fireEvent(EventType.LOAD.name);

        return inst;
    }

    /**
     * Sets the ownerID of the item.
     *
     * @param process   : String Identifier of process triggering this action
     * @param owner_id  : int designating the ID of the owner
     * @param creator   : L2PcInstance Player requesting the item creation
     * @param reference : L2Object Object referencing current action like NPC selling item or previous item in
     *                  transformation
     */
    public void setOwnerId(String process, int owner_id, L2PcInstance creator, L2Object reference) {
        int oldOwner = _ownerId;
        setOwnerId(owner_id);

        fireEvent(EventType.SETOWNER.name, process, oldOwner);
    }

    /**
     * Returns the ownerID of the item.
     *
     * @return int : ownerID of the item
     */
    public int getOwnerId() {
        return _ownerId;
    }

    /**
     * Sets the ownerID of the item.
     *
     * @param owner_id : int designating the ID of the owner
     */
    public void setOwnerId(int owner_id) {
        if (owner_id == _ownerId)
            return;

        _ownerId = owner_id;
        _storedInDb = false;
    }

    /**
     * Sets the location of the item.<BR>
     * <BR>
     * <U><I>Remark :</I></U> If loc and loc_data different from database, say datas not up-to-date
     *
     * @param loc      : ItemLocation (enumeration)
     * @param loc_data : int designating the slot where the item is stored or the village for freights
     */
    public void setLocation(ItemLocation loc, int loc_data) {
        if (loc == _loc && loc_data == _locData)
            return;
        _loc = loc;
        _locData = loc_data;
        _storedInDb = false;
    }

    /**
     * Gets the location.
     *
     * @return the location
     */
    public ItemLocation getLocation() {
        return _loc;
    }

    /**
     * Sets the location of the item.
     *
     * @param loc : ItemLocation (enumeration)
     */
    public void setLocation(ItemLocation loc) {
        setLocation(loc, 0);
    }

    public boolean isPotion() {
        return _item.isPotion();
    }

    /**
     * Returns the quantity of item.
     *
     * @return int
     */
    public int getCount() {
        return _count;
    }

    // No logging (function designed for shots only)

    /**
     * Sets the quantity of the item.<BR>
     * <BR>
     * <U><I>Remark :</I></U> If loc and loc_data different from database, say datas not up-to-date
     *
     * @param count : int
     */
    public void setCount(int count) {
        if (_count == count)
            return;

        _count = count >= -1 ? count : 0;
        _storedInDb = false;
    }

    /**
     * Sets the quantity of the item.<BR>
     * <BR>
     * <U><I>Remark :</I></U> If loc and loc_data different from database, say datas not up-to-date
     *
     * @param process   : String Identifier of process triggering this action
     * @param count     : int
     * @param creator   : L2PcInstance Player requesting the item creation
     * @param reference : L2Object Object referencing current action like NPC selling item or previous item in
     *                  transformation
     */
    public void changeCount(String process, int count, L2PcInstance creator, L2Object reference) {
        if (count == 0)
            return;

        if (count > 0 && _count > Integer.MAX_VALUE - count)
            _count = Integer.MAX_VALUE;
        else
            _count += count;

        if (_count < 0)
            _count = 0;

        _storedInDb = false;

		/*if(Config.LOG_ITEMS)
        {
			LogRecord record = new LogRecord( "CHANGE:" + process);
			record.setLoggerName("item");
			record.setParameters(new Object[]
			{
					this, creator, reference
			});
			LOGGERItems.log(record);
			record = null;
		}*/
    }

    /**
     * Change count without trace.
     *
     * @param process   the process
     * @param count     the count
     * @param creator   the creator
     * @param reference the reference
     */
    public void changeCountWithoutTrace(String process, int count, L2PcInstance creator, L2Object reference) {
        if (count == 0)
            return;
        if (count > 0 && _count > Integer.MAX_VALUE - count)
            _count = Integer.MAX_VALUE;
        else
            _count += count;
        if (_count < 0)
            _count = 0;

        _storedInDb = false;
    }

    /**
     * Returns if item is equipable.
     *
     * @return boolean
     */
    public boolean isEquipable() {
        return !(_item.getBodyPart() == 0 || _item instanceof L2EtcItem);
    }

    /**
     * Returns if item is equipped.
     *
     * @return boolean
     */
    public boolean isEquipped() {
        return _loc == ItemLocation.PAPERDOLL || _loc == ItemLocation.PET_EQUIP;
    }

    /**
     * Returns the slot where the item is stored.
     *
     * @return int
     */
    public int getEquipSlot() {
        if (GameServerConfig.ASSERT)
            assert _loc == ItemLocation.PAPERDOLL || _loc == ItemLocation.PET_EQUIP || _loc == ItemLocation.FREIGHT;

        return _locData;
    }

    /**
     * Returns the characteristics of the item.
     *
     * @return L2Item
     */
    public L2Item getItem() {
        return _item;
    }

    /**
     * Gets the custom type1.
     *
     * @return the custom type1
     */
    public int getCustomType1() {
        return _type1;
    }

    /**
     * Sets the custom type1.
     *
     * @param newtype the new custom type1
     */
    public void setCustomType1(int newtype) {
        _type1 = newtype;
    }

    /**
     * Gets the custom type2.
     *
     * @return the custom type2
     */
    public int getCustomType2() {
        return _type2;
    }

    /**
     * Sets the custom type2.
     *
     * @param newtype the new custom type2
     */
    public void setCustomType2(int newtype) {
        _type2 = newtype;
    }

    /**
     * Gets the drop time.
     *
     * @return the drop time
     */
    public long getDropTime() {
        return _dropTime;
    }

    //Cupid's bow

    /**
     * Sets the drop time.
     *
     * @param time the new drop time
     */
    public void setDropTime(long time) {
        _dropTime = time;
    }

    /**
     * Checks if is cupid bow.
     *
     * @return true, if is cupid bow
     */
    public boolean isCupidBow() {
        if (getItemId() == 9140 || getItemId() == 9141)
            return true;
        return false;
    }

    /**
     * Checks if is wear.
     *
     * @return true, if is wear
     */
    public boolean isWear() {
        return _wear;
    }

    /**
     * Sets the wear.
     *
     * @param newwear the new wear
     */
    public void setWear(boolean newwear) {
        _wear = newwear;
    }

    /**
     * Returns the type of item.
     *
     * @return Enum
     */
    public Enum<?> getItemType() {
        return _item.getItemType();
    }

    /**
     * Returns the ID of the item.
     *
     * @return int
     */
    public int getItemId() {
        return _itemId;
    }

    /**
     * Returns the quantity of crystals for crystallization.
     *
     * @return int
     */
    public final int getCrystalCount() {
        return _item.getCrystalCount(_enchantLevel);
    }

    /**
     * Returns the reference price of the item.
     *
     * @return int
     */
    public int getReferencePrice() {
        return _item.getReferencePrice();
    }

    /**
     * Returns the name of the item.
     *
     * @return String
     */
    public String getItemName() {
        return _item.getName();
    }

    /**
     * Returns the price of the item for selling.
     *
     * @return int
     */
    public int getPriceToSell() {
        return isConsumable() ? (int) (_priceSell * GameServerConfig.RATE_CONSUMABLE_COST) : _priceSell;
    }

    /**
     * Sets the price of the item for selling <U><I>Remark :</I></U> If loc and loc_data different from database, say
     * datas not up-to-date.
     *
     * @param price : int designating the price
     */
    public void setPriceToSell(int price) {
        _priceSell = price;
        _storedInDb = false;
    }

    /**
     * Returns the price of the item for buying.
     *
     * @return int
     */
    public int getPriceToBuy() {
        return isConsumable() ? (int) (_priceBuy * GameServerConfig.RATE_CONSUMABLE_COST) : _priceBuy;
    }

    /**
     * Sets the price of the item for buying <U><I>Remark :</I></U> If loc and loc_data different from database, say
     * datas not up-to-date.
     *
     * @param price : int
     */
    public void setPriceToBuy(int price) {
        _priceBuy = price;
        _storedInDb = false;
    }

    /**
     * Returns the last change of the item.
     *
     * @return int
     */
    public int getLastChange() {
        return _lastChange;
    }

    /**
     * Sets the last change of the item.
     *
     * @param lastChange : int
     */
    public void setLastChange(int lastChange) {
        _lastChange = lastChange;
    }

    /**
     * Returns if item is stackable.
     *
     * @return boolean
     */
    public boolean isStackable() {
        return _item.isStackable();
    }

    /**
     * Returns if item is dropable.
     *
     * @return boolean
     */
    public boolean isDropable() {
        return !isAugmented() && _item.isDropable();
    }

    /**
     * Returns if item is destroyable.
     *
     * @return boolean
     */
    public boolean isDestroyable() {
        return _item.isDestroyable();
    }

    /**
     * Returns if item is tradeable.
     *
     * @return boolean
     */
    public boolean isTradeable() {
        return !isAugmented() && _item.isTradeable();
    }

    /**
     * Returns if item is consumable.
     *
     * @return boolean
     */
    public boolean isConsumable() {
        return _item.isConsumable();
    }

    /**
     * Returns if item is available for manipulation.
     *
     * @param player        the player
     * @param allowAdena    the allow adena
     * @param allowEquipped
     * @return boolean
     */
    public boolean isAvailable(L2PcInstance player, boolean allowAdena, boolean allowEquipped) {
        return (!isEquipped() || allowEquipped)
                && getItem().getType2() != L2Item.TYPE2_QUEST
                && (getItem().getType2() != L2Item.TYPE2_MONEY || getItem().getType1() != L2Item.TYPE1_SHIELD_ARMOR) // TODO: what does this mean?
                && (player.getPet() == null || getObjectId() != player.getPet().getControlItemId()) // Not Control item of currently summoned pet
                && player.getActiveEnchantItem() != this
                && (allowAdena || getItemId() != 57)
                && (player.getCurrentSkill() == null || player.getCurrentSkill().getSkill().getItemConsumeId() != getItemId())
                && isTradeable();
    }

    /* (non-Javadoc)
     * @see com.l2jfrozen.gameserver.model.L2Object#onAction(com.l2jfrozen.gameserver.model.L2PcInstance)
     * also check constraints: only soloing castle owners may pick up mercenary tickets of their castle
     */
    @Override
    public void onAction(L2PcInstance player) {
        // this causes the validate position handler to do the pickup if the location is reached.
        // mercenary tickets can only be picked up by the castle owner and GMs.
        if ((!player.isGM()) && (_itemId >= 3960 && _itemId <= 4021 && player.isInParty() || _itemId >= 3960 && _itemId <= 3969 && !player.isCastleLord(1) || _itemId >= 3973 && _itemId <= 3982 && !player.isCastleLord(2) || _itemId >= 3986 && _itemId <= 3995 && !player.isCastleLord(3) || _itemId >= 3999 && _itemId <= 4008 && !player.isCastleLord(4) || _itemId >= 4012 && _itemId <= 4021 && !player.isCastleLord(5) || _itemId >= 5205 && _itemId <= 5214 && !player.isCastleLord(6) || _itemId >= 6779 && _itemId <= 6788 && !player.isCastleLord(7) || _itemId >= 7973 && _itemId <= 7982 && !player.isCastleLord(8) || _itemId >= 7918 && _itemId <= 7927 && !player.isCastleLord(9))) {
            if (player.isInParty())
                player.sendMessage("You cannot pickup mercenaries while in a party.");
            else
                player.sendMessage("Only the castle lord can pickup mercenaries.");

            player.setTarget(this);
            player.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
            // Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
            player.sendPacket(ActionFailed.STATIC_PACKET);
        } else {
            player.getAI().setIntention(CtrlIntention.AI_INTENTION_PICK_UP, this);
        }
    }

    /**
     * Returns the level of enchantment of the item.
     *
     * @return int
     */
    public int getEnchantLevel() {
        return _enchantLevel;
    }

    /**
     * Sets the level of enchantment of the item.
     *
     * @param enchantLevel the new enchant level
     */
    public void setEnchantLevel(int enchantLevel) {
        if (_enchantLevel == enchantLevel)
            return;
        _enchantLevel = enchantLevel;
        _storedInDb = false;
    }

    /**
     * Returns the physical defense of the item.
     *
     * @return int
     */
    public int getPDef() {
        if (_item instanceof L2Armor)
            return ((L2Armor) _item).getPDef();
        return 0;
    }

    /**
     * Returns whether this item is augmented or not.
     *
     * @return true if augmented
     */
    public boolean isAugmented() {
        return _augmentation != null;
    }

    /**
     * Returns the augmentation object for this item.
     *
     * @return augmentation
     */
    public L2Augmentation getAugmentation() {
        return _augmentation;
    }

    /**
     * Sets a new augmentation.
     *
     * @param augmentation the augmentation
     * @return return true if sucessfull
     */
    public boolean setAugmentation(L2Augmentation augmentation) {
        // there shall be no previous augmentation..
        if (_augmentation != null)
            return false;
        _augmentation = augmentation;
        return true;
    }

    /**
     * Remove the augmentation.
     */
    public void removeAugmentation() {
        if (_augmentation == null)
            return;
        _augmentation.deleteAugmentationData();
        _augmentation = null;
    }

    /**
     * Returns true if this item is a shadow item Shadow items have a limited life-time.
     *
     * @return true, if is shadow item
     */
    public boolean isShadowItem() {
        return _mana >= 0;
    }

    /**
     * Returns the remaining mana of this shadow item.
     *
     * @return lifeTime
     */
    public int getMana() {
        return _mana;
    }

    /**
     * Sets the mana for this shadow item <b>NOTE</b>: does not send an inventory update packet.
     *
     * @param mana the new mana
     */
    public void setMana(int mana) {
        _mana = mana;
    }

    /**
     * Decreases the mana of this shadow item, sends a inventory update schedules a new consumption task if non is
     * running optionally one could force a new task.
     *
     * @param resetConsumingMana the reset consuming mana
     */
    public void decreaseMana(boolean resetConsumingMana) {
        if (!isShadowItem())
            return;

        if (_mana > 0)
            _mana--;

        if (_storedInDb)
            _storedInDb = false;
        if (resetConsumingMana)
            _consumingMana = false;

        L2PcInstance player = (L2PcInstance) L2World.getInstance().findObject(getOwnerId());
        if (player != null) {
            SystemMessage sm;
            switch (_mana) {
                case 10:
                    sm = new SystemMessage(SystemMessageId.S1S_REMAINING_MANA_IS_NOW_10);
                    sm.addString(getItemName());
                    player.sendPacket(sm);
                    break;
                case 5:
                    sm = new SystemMessage(SystemMessageId.S1S_REMAINING_MANA_IS_NOW_5);
                    sm.addString(getItemName());
                    player.sendPacket(sm);
                    break;
                case 1:
                    sm = new SystemMessage(SystemMessageId.S1S_REMAINING_MANA_IS_NOW_1);
                    sm.addString(getItemName());
                    player.sendPacket(sm);
                    break;
            }

            if (_mana == 0) // The life time has expired
            {
                sm = new SystemMessage(SystemMessageId.S1S_REMAINING_MANA_IS_NOW_0);
                sm.addString(getItemName());
                player.sendPacket(sm);

                // unequip
                if (isEquipped()) {
                    L2ItemInstance[] unequiped = player.getInventory().unEquipItemInSlotAndRecord(getEquipSlot());
                    InventoryUpdate iu = new InventoryUpdate();

                    for (L2ItemInstance element : unequiped) {
                        player.checkSSMatch(null, element);
                        iu.addModifiedItem(element);
                    }

                    player.sendPacket(iu);

                    unequiped = null;
                    iu = null;
                }

                if (getLocation() != ItemLocation.WAREHOUSE) {
                    // destroy
                    player.getInventory().destroyItem("L2ItemInstance", this, player, null);

                    // send update
                    InventoryUpdate iu = new InventoryUpdate();
                    iu.addRemovedItem(this);
                    player.sendPacket(iu);
                    iu = null;

                    StatusUpdate su = new StatusUpdate(player.getObjectId());
                    su.addAttribute(StatusUpdate.CUR_LOAD, player.getCurrentLoad());
                    player.sendPacket(su);
                    su = null;
                } else {
                    player.getWarehouse().destroyItem("L2ItemInstance", this, player, null);
                }

                // delete from world
                L2World.getInstance().removeObject(this);
            } else {
                // Reschedule if still equipped
                if (!_consumingMana && isEquipped())
                    scheduleConsumeManaTask();

                if (getLocation() != ItemLocation.WAREHOUSE) {
                    InventoryUpdate iu = new InventoryUpdate();
                    iu.addModifiedItem(this);
                    player.sendPacket(iu);
                    iu = null;
                }
            }

            sm = null;
        }

        player = null;
    }

    /**
     * Schedule consume mana task.
     */
    private void scheduleConsumeManaTask() {
        _consumingMana = true;
        ThreadPoolManager.getInstance().scheduleGeneral(new ScheduleConsumeManaTask(this), MANA_CONSUMPTION_RATE);
    }

    /**
     * Returns false cause item can't be attacked.
     *
     * @param attacker the attacker
     * @return boolean false
     */
    @Override
    public boolean isAutoAttackable(L2Character attacker) {
        return false;
    }

    /**
     * Returns the type of charge with SoulShot of the item.
     *
     * @return int (CHARGED_NONE, CHARGED_SOULSHOT)
     */
    public int getChargedSoulshot() {
        return _chargedSoulshot;
    }

    /**
     * Sets the type of charge with SoulShot of the item.
     *
     * @param type : int (CHARGED_NONE, CHARGED_SOULSHOT)
     */
    public void setChargedSoulshot(int type) {
        _chargedSoulshot = type;
    }

    /**
     * Returns the type of charge with SpiritShot of the item.
     *
     * @return int (CHARGED_NONE, CHARGED_SPIRITSHOT, CHARGED_BLESSED_SPIRITSHOT)
     */
    public int getChargedSpiritshot() {
        return _chargedSpiritshot;
    }

    /**
     * Sets the type of charge with SpiritShot of the item.
     *
     * @param type : int (CHARGED_NONE, CHARGED_SPIRITSHOT, CHARGED_BLESSED_SPIRITSHOT)
     */
    public void setChargedSpiritshot(int type) {
        _chargedSpiritshot = type;
    }

    /**
     * Gets the charged fishshot.
     *
     * @return the charged fishshot
     */
    public boolean getChargedFishshot() {
        return _chargedFishtshot;
    }

    /**
     * Sets the charged fishshot.
     *
     * @param type the new charged fishshot
     */
    public void setChargedFishshot(boolean type) {
        _chargedFishtshot = type;
    }

    /**
     * This function basically returns a set of functions from L2Item/L2Armor/L2Weapon, but may add additional
     * functions, if this particular item instance is enhanched for a particular player.
     *
     * @param player : L2Character designating the player
     * @return Func[]
     */
    public Func[] getStatFuncs(L2Character player) {
        return getItem().getStatFuncs(this, player);
    }

    /**
     * Updates database.<BR>
     * <BR>
     * <U><I>Concept : </I></U><BR>
     * <B>IF</B> the item exists in database :
     * <UL>
     * <LI><B>IF</B> the item has no owner, or has no location, or has a null quantity : remove item from database</LI>
     * <LI><B>ELSE</B> : update item in database</LI>
     * </UL>
     * <B> Otherwise</B> :
     * <UL>
     * <LI><B>IF</B> the item hasn't a null quantity, and has a correct location, and has a correct owner : insert item
     * in database</LI>
     * </UL>
     */
    public void updateDatabase() {

        if (isWear())
            return;

        if (_existsInDb) {
            if (_ownerId == 0 || _loc == ItemLocation.VOID || _count == 0 && _loc != ItemLocation.LEASE)
                removeFromDb();
            else
                updateInDb();
        } else {
            if (_count == 0 && _loc != ItemLocation.LEASE)
                return;

            if (_loc == ItemLocation.VOID || _ownerId == 0)
                return;

            insertIntoDb();
        }
    }

    /**
     * Init a dropped L2ItemInstance and add it in the world as a visible object.<BR>
     * <BR>
     * <B><U> Actions</U> :</B><BR>
     * <BR>
     * <li>Set the x,y,z position of the L2ItemInstance dropped and update its _worldregion</li> <li>Add the
     * L2ItemInstance dropped to _visibleObjects of its L2WorldRegion</li> <li>Add the L2ItemInstance dropped in the
     * world as a <B>visible</B> object</li><BR>
     * <BR>
     * <FONT COLOR=#FF0000><B> <U>Caution</U> : This method DOESN'T ADD the object to _allObjects of L2World </B></FONT><BR>
     * <BR>
     * <B><U> Assert </U> :</B><BR>
     * <BR>
     * <li>_worldRegion == null <I>(L2Object is invisible at the beginning)</I></li><BR>
     * <BR>
     * <B><U> Example of use </U> :</B><BR>
     * <BR>
     * <li>Drop item</li> <li>Call Pet</li><BR>
     *
     * @param dropper the dropper
     * @param x       the x
     * @param y       the y
     * @param z       the z
     */
    public final void dropMe(L2Character dropper, int x, int y, int z) {
        if (GameServerConfig.ASSERT)
            assert getPosition().getWorldRegion() == null;

        if (GameServerConfig.GEODATA > 0 && dropper != null) {
            Location dropDest = GeoData.getInstance().moveCheck(dropper.getX(), dropper.getY(), dropper.getZ(), x, y, z);

            if (dropDest != null && dropDest.getX() != 0 && dropDest.getY() != 0) {

                x = dropDest.getX();
                y = dropDest.getY();
                z = dropDest.getZ();

            }

            dropDest = null;
        }

        synchronized (this) {
            // Set the x,y,z position of the L2ItemInstance dropped and update its _worldregion
            setIsVisible(true);
            getPosition().setWorldPosition(x, y, z);
            getPosition().setWorldRegion(L2World.getInstance().getRegion(getPosition().getWorldPosition()));

            // Add the L2ItemInstance dropped to _visibleObjects of its L2WorldRegion
            getPosition().getWorldRegion().addVisibleObject(this);
        }

        setDropTime(System.currentTimeMillis());

        // this can synchronize on others instancies, so it's out of
        // synchronized, to avoid deadlocks
        // Add the L2ItemInstance dropped in the world as a visible object
        L2World.getInstance().addVisibleObject(this, getPosition().getWorldRegion(), dropper);

        if (GameServerConfig.SAVE_DROPPED_ITEM)
            ItemsOnGroundManager.getInstance().save(this);
    }

    /**
     * Update the database with values of the item.
     */
    private void updateInDb() {
        if (GameServerConfig.ASSERT)
            assert _existsInDb;

        if (_wear)
            return;

        if (_storedInDb)
            return;
        try {
            CharacterEntity characterEntity;
            L2PcInstance player = L2World.getInstance().getPlayer(getOwnerId());
            if (player != null) {
                characterEntity = player.getCharacter();
            } else {
                characterEntity = CharacterManager.getInstance().get(_ownerId);
            }

            CharacterItemEntity item = characterEntity.getItems().get(((Number) getObjectId()).longValue());
            if (!item.getCharacter().getId().equals(characterEntity.getId())) {
                item.setCharacter(characterEntity);
                LOGGER.info("update owner");
            }
            item.setItemId(getItemId());
            item.setCount(getCount());
            item.setLoc(getLocation());
            item.setLocData(_locData);
            item.setEnchantLevel(getEnchantLevel());
            item.setPriceSell(getPriceToSell());
            item.setPriceBuy(getPriceToBuy());
            item.setCustomType1(getCustomType1());
            item.setCustomType2(getCustomType2());
            item.setManaLeft(getMana());
            characterEntity.getItems().put(item.getId(), item);
            CommonManager.getInstance().update(item);
            _existsInDb = true;
            _storedInDb = true;

        } catch (Exception e) {
            LOGGER.info("Could not update item " + getObjectId() + " in DB: Reason: ", e);
        }

        if (_existsInDb)
            fireEvent(EventType.STORE.name, (Object[]) null);
    }

    /**
     * Insert the item in database.
     */
    private void insertIntoDb() {
        if (_wear)
            return;

        if (GameServerConfig.ASSERT)
            assert !_existsInDb && getObjectId() != 0;
        try

        {
            CharacterEntity characterEntity;
            L2PcInstance player = L2World.getInstance().getPlayer(_ownerId);
            if (player != null) {
                characterEntity = player.getCharacter();
            } else {
                characterEntity = CharacterManager.getInstance().get(_ownerId);
            }
            if (characterEntity == null) {
                LOGGER.warn("Character nit found");
                return;
            }
            CharacterItemEntity item = new CharacterItemEntity();
            item.setCharacter(characterEntity);
            item.setItemId(_itemId);
            item.setCount(getCount());
            item.setLoc(_loc);
            item.setLocData(_locData);
            item.setEnchantLevel(getEnchantLevel());
            item.setPriceSell(_priceSell);
            item.setPriceBuy(_priceBuy);
            item.setCustomType1(_type1);
            item.setCustomType2(_type2);
            item.setManaLeft(getMana());
            CharacterManager.getInstance().create(item);
            characterEntity.getItems().put(item.getId(), item);
            setObjectId(item.getId().intValue());
            _existsInDb = true;
            _storedInDb = true;

        } catch (Exception e) {
            LOGGER.error("ATTENTION: Update Item instead of Insert one, check player with id " + this.getOwnerId() + " actions on item " + this.getObjectId(), e);
            updateInDb();

        }
    }

    /**
     * Delete item from database.
     */
    private void removeFromDb() {
        if (_wear)
            return;

        if (GameServerConfig.ASSERT)
            assert _existsInDb;

        // delete augmentation data
        if (isAugmented())
            _augmentation.deleteAugmentationData();

        try {
            CharacterItemEntity entity = CharacterManager.getInstance().getItem(getObjectId());
            if (entity == null) {
                return;
            }
            L2PcInstance player = L2World.getInstance().getPlayer(entity.getCharacter().getId().intValue());
            if (player == null) {
                return;
            }
            player.getCharacter().getItems().remove(entity.getId());
            CharacterManager.getInstance().deleteItem(entity);

            _existsInDb = false;
            _storedInDb = true;

        } catch (Exception e) {
            LOGGER.warn("Could not delete item " + getObjectId() + " in DB:", e);
        }

        if (!_existsInDb)
            fireEvent(EventType.DELETE.name, (Object[]) null);
    }

    /**
     * Returns the item in String format.
     *
     * @return String
     */
    @Override
    public String toString() {
        return "" + _item;
    }

    /**
     * Reset owner timer.
     */
    public void resetOwnerTimer() {
        if (itemLootShedule != null) {
            itemLootShedule.cancel(true);
        }
        itemLootShedule = null;
    }

    /**
     * Gets the item loot shedule.
     *
     * @return the item loot shedule
     */
    public ScheduledFuture<?> getItemLootShedule() {
        return itemLootShedule;
    }

    /**
     * Sets the item loot shedule.
     *
     * @param sf the new item loot shedule
     */
    public void setItemLootShedule(ScheduledFuture<?> sf) {
        itemLootShedule = sf;
    }

    /**
     * Checks if is protected.
     *
     * @return true, if is protected
     */
    public boolean isProtected() {
        return _protected;
    }

    /**
     * Sets the protected.
     *
     * @param is_protected the new protected
     */
    public void setProtected(boolean is_protected) {
        _protected = is_protected;
    }

    /**
     * Checks if is night lure.
     *
     * @return true, if is night lure
     */
    public boolean isNightLure() {
        return _itemId >= 8505 && _itemId <= 8513 || _itemId == 8485;
    }

    /**
     * Gets the count decrease.
     *
     * @return the count decrease
     */
    public boolean getCountDecrease() {
        return _decrease;
    }

    /**
     * Sets the count decrease.
     *
     * @param decrease the new count decrease
     */
    public void setCountDecrease(boolean decrease) {
        _decrease = decrease;
    }

    /**
     * Gets the inits the count.
     *
     * @return the inits the count
     */
    public int getInitCount() {
        return _initCount;
    }

    /**
     * Sets the inits the count.
     *
     * @param InitCount the new inits the count
     */
    public void setInitCount(int InitCount) {
        _initCount = InitCount;
    }

    /**
     * Restore init count.
     */
    public void restoreInitCount() {
        if (_decrease)
            _count = _initCount;
    }

    /**
     * Gets the time.
     *
     * @return the time
     */
    public int getTime() {
        return _time;
    }

    /**
     * Sets the time.
     *
     * @param time the new time
     */
    public void setTime(int time) {
        if (time > 0)
            _time = time;
        else
            _time = 0;
    }

    /**
     * Returns the slot where the item is stored.
     *
     * @return int
     */
    public int getLocationSlot() {
        if (GameServerConfig.ASSERT)
            assert _loc == ItemLocation.PAPERDOLL || _loc == ItemLocation.PET_EQUIP || _loc == ItemLocation.FREIGHT || _loc == ItemLocation.INVENTORY;

        return _locData;
    }

    /**
     * Gets the drop protection.
     *
     * @return the drop protection
     */
    public final DropProtection getDropProtection() {
        return _dropProtection;
    }

    /**
     * Checks if is varka ketra ally quest item.
     *
     * @return true, if is varka ketra ally quest item
     */
    public boolean isVarkaKetraAllyQuestItem() {

        return (this.getItemId() >= 7211 && this.getItemId() <= 7215)
                || (this.getItemId() >= 7221 && this.getItemId() <= 7225);

    }

    public boolean isOlyRestrictedItem() {
        return (GameServerConfig.LIST_OLY_RESTRICTED_ITEMS.contains(_itemId));
    }

    public boolean isHeroItem() {
        return ((_itemId >= 6611 && _itemId <= 6621) || (_itemId >= 9388 && _itemId <= 9390) || _itemId == 6842);
    }

    public boolean checkOlympCondition() {
        return !(isHeroItem() || isOlyRestrictedItem() || isWear() || (!GameServerConfig.ALT_OLY_AUGMENT_ALLOW && isAugmented()));
    }

    /**
     * Used to decrease mana (mana means life time for shadow items).
     */
    public class ScheduleConsumeManaTask implements Runnable {

        /**
         * The _shadow item.
         */
        private final L2ItemInstance _shadowItem;

        /**
         * Instantiates a new schedule consume mana task.
         *
         * @param item the item
         */
        public ScheduleConsumeManaTask(L2ItemInstance item) {
            _shadowItem = item;
        }

        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            try {
                // decrease mana
                if (_shadowItem != null) {
                    _shadowItem.decreaseMana(true);
                }
            } catch (Throwable t) {
                if (GameServerConfig.ENABLE_ALL_EXCEPTIONS)
                    t.printStackTrace();
            }
        }
    }


    public static L2ItemInstance createItem(int itemId) {

        L2ItemInstance item = null;

        try {
            item = new L2ItemInstance(IdFactory.getInstance().getNextId(), itemId);
        } catch (IllegalArgumentException e) {
            LOGGER.error("", e);
        }

        return item;

    }

    public static L2ItemInstance createItem(int objid, int itemId) {

        L2ItemInstance item = null;

        try {
            item = new L2ItemInstance(objid, itemId);
        } catch (IllegalArgumentException e) {
            LOGGER.error("", e);
        }

        return item;

    }

    /**
     * Create the L2ItemInstance corresponding to the Item Identifier and quantitiy add logs the activity.<BR>
     * <BR>
     * <B><U> Actions</U> :</B><BR>
     * <BR>
     * <li>Create and Init the L2ItemInstance corresponding to the Item Identifier and quantity</li> <li>Add the
     * L2ItemInstance object to _allObjects of L2world</li> <li>Logs Item creation according to log settings</li><BR>
     * <BR>
     *
     * @param process   : String Identifier of process triggering this action
     * @param itemId    : int Item Identifier of the item to be created
     * @param count     : int Quantity of items to be created for stackable items
     * @param actor     : L2PcInstance Player requesting the item creation
     * @param reference : L2Object Object referencing current action like NPC selling item or previous item in
     *                  transformation
     * @return L2ItemInstance corresponding to the new item
     */
    public static L2ItemInstance createItem(String process, int itemId, int count, L2PcInstance actor, L2Object reference) {
        // Create and Init the L2ItemInstance corresponding to the Item Identifier
        L2ItemInstance item = createItem(itemId);

        // Set Item parameters
        if (item.isStackable() && count > 1) {
            item.setCount(count);
        }

        //create loot schedule also if autoloot is enabled
        if (process.equalsIgnoreCase("loot")) {
            int ownerId;
            ScheduledFuture<?> itemLootShedule;
            long delay = 0;
            // if in CommandChannel and was killing a World/RaidBoss
            if (reference instanceof L2GrandBossInstance || reference instanceof L2RaidBossInstance) {
                if (((L2Attackable) reference).getFirstCommandChannelAttacked() != null && ((L2Attackable) reference).getFirstCommandChannelAttacked().meetRaidWarCondition(reference)) {
                    ownerId = ((L2Attackable) reference).getFirstCommandChannelAttacked().getChannelLeader().getObjectId();
                    //item.setOwnerId(((L2Attackable) reference).getFirstCommandChannelAttacked().getChannelLeader().getObjectId());
                    delay = 300000;
                } else {
                    ownerId = actor.getObjectId();
                    delay = 15000;
                    //item.setOwnerId(actor.getObjectId());
                }
            } else {
                ownerId = actor.getObjectId();
                //item.setOwnerId(actor.getObjectId());
                delay = 15000;
            }

            //it's loot action, so set the owner but not store it on db until it has been taken
            item.setOwnerId(ownerId);

            itemLootShedule = ThreadPoolManager.getInstance().scheduleGeneral(new ResetOwner(item), delay);
            item.setItemLootShedule(itemLootShedule);
        }
        //if item is not loot one, set the owner id to provided actor ID
        else if (actor != null) {
            item.setOwnerId(actor.getObjectId());
        }

        LOGGER.debug("ItemTable: Item created  oid: {} itemid: {}" + " " + item.getObjectId() + " " + itemId);

        // Add the L2ItemInstance object to _allObjects of L2world
        L2World.getInstance().storeObject(item);

        if (GameServerConfig.LOG_ITEMS) {
            ItemAudit.getInstance().chat("CREATE: Item:" + item + " ID:" + item.getItemId() + " [" + actor.getName() + "] to " + reference);
        }

        return item;
    }

    public static L2ItemInstance createItem(String process, int itemId, int count, L2PcInstance actor) {
        return createItem(process, itemId, count, actor, null);
    }

    /**
     * Returns a dummy (fr = factice) item.<BR>
     * <BR>
     * <U><I>Concept :</I></U><BR>
     * Dummy item is created by setting the ID of the object in the world at null value
     *
     * @param itemId : int designating the item
     * @return L2ItemInstance designating the dummy item created
     */
    public static L2ItemInstance createDummyItem(int itemId) {
        L2Item item = ItemTable.getInstance().getTemplate(itemId);

        if (item == null)
            return null;

        L2ItemInstance temp = new L2ItemInstance(0, item);

        try {
            temp = new L2ItemInstance(0, itemId);
        } catch (ArrayIndexOutOfBoundsException e) {
            LOGGER.error("", e);

            // this can happen if the item templates were not initialized
        }

        if (temp.getItem() == null) {
            LOGGER.warn("ItemTable: Item Template missing for Id: {}" + " " + itemId);
        }

        return temp;
    }

    /**
     * Destroys the L2ItemInstance.<BR>
     * <BR>
     * <B><U> Actions</U> :</B><BR>
     * <BR>
     * <li>Sets L2ItemInstance parameters to be unusable</li> <li>Removes the L2ItemInstance object to _allObjects of
     * L2world</li> <li>Logs Item delettion according to log settings</li><BR>
     * <BR>
     *
     * @param process   : String Identifier of process triggering this action
     * @param item
     * @param actor     : L2PcInstance Player requesting the item destroy
     * @param reference : L2Object Object referencing current action like NPC selling item or previous item in
     *                  transformation
     */
    public static void destroyItem(String process, L2ItemInstance item, L2PcInstance actor, L2Object reference) {
        synchronized (item) {
            item.setOwnerId(0);

            //remove it from database if present
            item.updateDatabase();

            item.setCount(0);
            item.setLocation(ItemLocation.VOID);
            item.setLastChange(L2ItemInstance.REMOVED);

            L2World.getInstance().removeObject(item);

            //if loot item, release id from factory
            IdFactory.getInstance().releaseId(item.getObjectId());

            if (GameServerConfig.LOG_ITEMS) {
                if (actor != null)
                    ItemAudit.getInstance().chat("DELETE: Item:" + item + " ID:" + item.getItemId() + " [" + actor.getName() + "] to " + reference);
                else
                    ItemAudit.getInstance().chat("DELETE: Item:" + item + " ID:" + item.getItemId() + " to " + reference);

            }

            // if it's a pet control item, delete the pet as well
            if (L2PetDataTable.isPetItem(item.getItemId())) {
                Connection con = null;
                try {
                    // Delete the pet in db
                    con = L2DatabaseFactory.getInstance().getConnection(false);
                    final PreparedStatement statement = con.prepareStatement("DELETE FROM pets WHERE item_obj_id=?");
                    statement.setInt(1, item.getObjectId());
                    statement.execute();
                    statement.close();
                } catch (Exception e) {
                    LOGGER.error("could not delete pet objectid" + " " + e);
                } finally {
                    CloseUtil.close(con);
                }
            }
        }
    }

    protected static class ResetOwner implements Runnable {
        L2ItemInstance _item;

        public ResetOwner(L2ItemInstance item) {
            _item = item;
        }

        @Override
        public void run() {
            _item.setObjectId(IdFactory.getInstance().getNextId());
            _item.setOwnerId(0);
            _item.setItemLootShedule(null);
        }
    }
}
