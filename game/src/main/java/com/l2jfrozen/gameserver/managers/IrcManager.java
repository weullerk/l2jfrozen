/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.managers;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.network.L2IrcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Beetle
 */
public class IrcManager {
    static final Logger LOGGER = LoggerFactory.getLogger(IrcManager.class.getName());

    private static L2IrcClient _ircConnection;

    public static final IrcManager getInstance() {
        return SingletonHolder._instance;
    }

    public IrcManager() {
        LOGGER.info("Initializing IRCManager");
        load();
    }

    // =========================================================
    // Method - Public
    public void reload() {
        _ircConnection.disconnect();
        try {
            _ircConnection.connect();
        } catch (Exception e) {
            LOGGER.warn(e.toString());
        }
    }

    public L2IrcClient getConnection() {
        return _ircConnection;
    }

    // =========================================================
    // Method - Private
    private final void load() {
        _ircConnection = new L2IrcClient(GameServerConfig.IRC_SERVER, GameServerConfig.IRC_PORT, GameServerConfig.IRC_PASS, GameServerConfig.IRC_NICK, GameServerConfig.IRC_USER, GameServerConfig.IRC_NAME, GameServerConfig.IRC_SSL, GameServerConfig.IRC_CHANNEL);
        try {
            _ircConnection.connect();
        } catch (Exception e) {
            LOGGER.warn(e.toString());
        }
    }

    private static class SingletonHolder {
        protected static final IrcManager _instance = new IrcManager();
    }
}
