package com.l2jfrozen.gameserver;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterFriendEntity;
import com.l2jfrozen.gameserver.datatables.sql.CharTemplateTable;
import com.l2jfrozen.gameserver.datatables.sql.ClanTable;
import com.l2jfrozen.gameserver.managers.CursedWeaponsManager;
import com.l2jfrozen.gameserver.model.L2Clan;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.appearance.PcAppearance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.base.SubClass;
import com.l2jfrozen.gameserver.model.extender.BaseExtender;
import com.l2jfrozen.gameserver.network.serverpackets.FriendList;
import com.l2jfrozen.gameserver.templates.L2PcTemplate;
import com.l2jfrozen.util.CloseUtil;
import com.l2jfrozen.util.database.L2DatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.List;

import static com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance.PunishLevel.NONE;

/**
 * author vadim.didenko
 * 1/13/14.
 */
public class PlayerManager {
    private static Logger LOGGER = LoggerFactory.getLogger(PlayerManager.class.getName());
    private static PlayerManager manager;

    private PlayerManager() {
    }

    public static PlayerManager getInstance() {
        if (manager == null) {
            manager = new PlayerManager();
        }
        return manager;
    }

    /**
     * Create a new L2PcInstance and add it in the characters table of the database.<BR>
     * <BR>
     * <B><U> Actions</U> :</B><BR>
     * <BR>
     * <li>Create a new L2PcInstance with an account name</li> <li>Set the name, the Hair Style, the Hair Color and the
     * Face type of the L2PcInstance</li> <li>Add the player in the characters table of the database</li><BR>
     * <BR>
     *
     * @param objectId    Identifier of the object to initialized
     * @param template    The L2PcTemplate to apply to the L2PcInstance
     * @param accountName The name of the L2PcInstance
     * @param name        The name of the L2PcInstance
     * @param hairStyle   The hair style Identifier of the L2PcInstance
     * @param hairColor   The hair color Identifier of the L2PcInstance
     * @param face        The face type Identifier of the L2PcInstance
     * @param sex         the sex
     * @return The L2PcInstance added to the database or null
     */
    public L2PcInstance create(int objectId, L2PcTemplate template, String accountName, String name, byte hairStyle, byte hairColor, byte face, boolean sex) {
        // Create a new L2PcInstance with an account name
        PcAppearance app = new PcAppearance(face, hairColor, hairStyle, sex);
        L2PcInstance player = new L2PcInstance(objectId, template, accountName, app);
        app = null;

        // Set the name of the L2PcInstance
        player.setName(name);

        // Set the base class ID to that of the actual class ID.
        player.setBaseClass(player.getClassId());

        if (GameServerConfig.ALT_GAME_NEW_CHAR_ALWAYS_IS_NEWBIE) {
            player.setNewbie(true);
        }

        // Add the player in the characters table of the database
        boolean ok = createDb(player);

        if (!ok) {
            return null;
        }

        return player;
    }

    /**
     * Create a new player in the characters table of the database.<BR>
     * <BR>
     *
     * @return true, if successful
     */
    private boolean createDb(L2PcInstance player) {
        boolean output = false;
        CharacterEntity character = new CharacterEntity();

        character.setAccountName(player.getAccountName());
        character.setLevel(player.getLevel());
        character.setCharName(player.getName());
        character.setMaxHp(player.getMaxHp());
        character.setCurHp((int) player.getCurrentHp());
        character.setMaxCp(player.getMaxCp());
        character.setCurCp((int) player.getCurrentCp());
        character.setMaxMp(player.getMaxMp());
        character.setCurMp((int) player.getCurrentMp());
        character.setAccuracy(player.getAccuracy());
        character.setCritical(player.getCriticalHit(null, null));
        character.setEvasion(player.getEvasionRate(null));
        character.setMagicAtk(player.getMAtk(null, null));
        character.setmDef(player.getMDef(null, null));
        character.setmSpd(player.getMAtkSpd());
        character.setpAtk(player.getPAtk(null));
        character.setpDef(player.getPDef(null));
        character.setpSpd(player.getPAtkSpd());
        character.setRunSpd(player.getRunSpeed());
        character.setWalkSpd(player.getWalkSpeed());
        character.setStatStr(player.getSTR());
        character.setStatCon(player.getCON());
        character.setStatDex(player.getDEX());
        character.setStatInt(player.getINT());
        character.setStatMen(player.getMEN());
        character.setStatWit(player.getWIT());
        character.setFace(player.getAppearance().getFace());
        character.setHairStyle(player.getAppearance().getHairStyle());
        character.setHairColor(player.getAppearance().getHairColor());
        character.setSex(player.getAppearance().getSex() ? 1 : 0);
        character.setMovementMultiplier(1);
        character.setAttackSpeedMultiplier(1);
        character.setCollisionRadius(player.getTemplate().collisionRadius);
        character.setCollisionHeight(player.getTemplate().collisionHeight);
        character.setExp((int) player.getExp());
        character.setSp(player.getSp());
        character.setKarma(player.getKarma());
        character.setPvpKillCount(player.getPvpKills());
        character.setPkKillCount(player.getPkKills());
        character.setClanId(player.getClanId());
        character.setMaxLoad(player.getMaxLoad());
        character.setRace(player.getRace().ordinal());
        character.setClassId(player.getClassId().getId());
        character.setDeleteTime((int) player.getDeleteTimer());
        character.setCanCraft(player.hasDwarvenCraft() ? 1 : 0);
        character.setTitle(player.getTitle());
        character.setAccessLevel(player.getAccessLevel().getAccessType());
        character.setOnline(player.isOnline());
        character.setSevenSingsDungeon(player.isIn7sDungeon() ? 1 : 0);
        character.setClanPrivs(player.getClanPrivileges());
        character.setWantSpeace(player.getWantsPeace());
        character.setBaseClass(player.getBaseClass());
        character.setNewbie(player.isNewbie() ? 1 : 0);
        character.setNobless(player.isNoble() ? 1 : 0);
        character.setPowerGrade(0);
        character.setLastRecomDate((int) System.currentTimeMillis());

        character.setNameColor(StringToHex(Integer.toHexString(player.getAppearance().getNameColor()).toUpperCase()));
        character.setTitleColor(StringToHex(Integer.toHexString(player.getAppearance().getTitleColor()).toUpperCase()));
        character.setAio(player.isAio() ? 1 : 0);
        character.setAioEnd(0);
        character = CharacterManager.getInstance().create(character);

        if (character != null) {
            output = true;
            player.setObjectId(character.getId().intValue());
            player.setCharacter(character);
        }

        if (output) {
            String text = "Created new character : " + player.getName() + " for account: " + player.getAccountName();
            LOGGER.info(text, "New_chars");
        }

        return output;
    }

    /**
     * Store char base.
     */
    public synchronized void storeCharBase(L2PcInstance player) {

        player.setClassIndex(player.getClassIndex());

        CharacterEntity character = player.getCharacter();
        if (character == null) {
            return;
        }
        character.setLevel(player.getStat().getLevel());
        character.setMaxHp(player.getMaxHp());
        character.setCurHp((int) player.getCurrentHp());
        character.setMaxCp(player.getMaxCp());
        character.setCurCp((int) player.getCurrentCp());
        character.setMaxMp(player.getMaxMp());
        character.setCurMp((int) player.getCurrentMp());
        character.setAccuracy(player.getAccuracy());
        character.setCritical(player.getCriticalHit(null, null));
        character.setEvasion(player.getEvasionRate(null));
        character.setMagicAtk(player.getMAtk(null, null));
        character.setmDef(player.getMDef(null, null));
        character.setmSpd(player.getMAtkSpd());
        character.setpAtk(player.getPAtk(null));
        character.setpDef(player.getPDef(null));
        character.setpSpd(player.getPAtkSpd());
        character.setRunSpd(player.getRunSpeed());
        character.setWalkSpd(player.getWalkSpeed());
        character.setStatStr(player.getSTR());
        character.setStatCon(player.getCON());
        character.setStatDex(player.getDEX());
        character.setStatInt(player.getINT());
        character.setStatMen(player.getMEN());
        character.setStatWit(player.getWIT());
        character.setFace(player.getAppearance().getFace());
        character.setHairStyle(player.getAppearance().getHairStyle());
        character.setHairColor(player.getAppearance().getHairColor());
        character.setSex(player.getAppearance().getSex() ? 1 : 0);
        character.setMovementMultiplier(1);
        character.setAttackSpeedMultiplier(1);
        character.setCollisionRadius(player.getTemplate().collisionRadius);
        character.setCollisionHeight(player.getTemplate().collisionHeight);
        character.setExp((int) player.getStat().getExp());
        character.setSp(player.getStat().getSp());
        character.setExpBeforeDeath((int) player.getExpBeforeDeath());
        character.setKarma(player.getKarma());
        character.setPvpKillCount(player.getPvpKills());
        character.setPkKillCount(player.getPkKills());
        character.setClanId(player.getClanId());
        character.setMaxLoad(player.getMaxLoad());
        character.setRace(player.getRace().ordinal());
        character.setClassId(player.getClassId().getId());
        character.setDeleteTime((int) player.getDeleteTimer());
        character.setCanCraft(player.hasDwarvenCraft() ? 1 : 0);
        character.setTitle(player.getTitle());
        character.setAccessLevel(player.getAccessLevel().getAccessType());
        character.setOnline(player.isOnline());
        if (player.isOffline() || player.isOnline() == 1) { // in offline mode or online
            character.setOnline(1);
        } else {
            character.setOnline(player.isOnline());
        }
        character.setSevenSingsDungeon(player.isIn7sDungeon() ? 1 : 0);
        character.setClanPrivs(player.getClanPrivileges());
        character.setWantSpeace(player.getWantsPeace());
        character.setBaseClass(player.getBaseClass());
        character.setNewbie(player.isNewbie() ? 1 : 0);
        character.setNobless(player.isNoble() ? 1 : 0);
        character.setPowerGrade(0);
        character.setLastRecomDate((int) System.currentTimeMillis());

        character.setNameColor(StringToHex(Integer.toHexString(player._originalNameColorOffline).toUpperCase()));
        character.setTitleColor(StringToHex(Integer.toHexString(player.getAppearance().getTitleColor()).toUpperCase()));
        character.setAio(player.isAio() ? 1 : 0);
        character.setAioEnd((int) player.getAioEndTime());
        character.setX(player.inObserverMode() ? player.getObsX() : player.getX());
        character.setY(player.inObserverMode() ? player.getObsY() : player.getY());
        character.setZ(player.inObserverMode() ? player.getObsZ() : player.getZ());
        character.setHeading(player.getHeading());
        character.setRecHave(player.getRecomHave());
        character.setRecLeft(player.getRecomLeft());
        long totalOnlineTime = player.getOnlineTime();

        if (player.getOnlineBeginTime() > 0) {
            totalOnlineTime += (System.currentTimeMillis() - player.getOnlineBeginTime()) / 1000;
        }

        character.setOnlineTime(totalOnlineTime);
        character.setPunishLevel(player.getPunishLevel().value());
        character.setPunishTimer(player.getPunishTimer());
        character.setSubPledge(player.getPledgeType());
        character.setLastRecomDate(player.getLastRecomUpdate());
        character.setLvlJoinedAcademy(player.getLvlJoinedAcademy());
        character.setApprentice(player.getApprentice());
        character.setSponsor(((Number) player.getSponsor()).longValue());
        character.setVarkaKetraAlly(player.getAllianceWithVarkaKetra());
        character.setClanJoinExpiryTime(player.getClanJoinExpiryTime());
        character.setClanCreateExpiryTime(player.getClanCreateExpiryTime());
        character.setDeathPenaltyLevel(player.getDeathPenaltyBuffLevel());
        character.setPcPoint(player.getPcBangScore());
        CharacterManager.getInstance().update(character);
    }

    /**
     * String to hex.
     *
     * @param color the color
     * @return the string
     */
    private String StringToHex(String color) {
        switch (color.length()) {
            case 1:
                color = new StringBuilder().append("00000").append(color).toString();
                break;

            case 2:
                color = new StringBuilder().append("0000").append(color).toString();
                break;

            case 3:
                color = new StringBuilder().append("000").append(color).toString();
                break;

            case 4:
                color = new StringBuilder().append("00").append(color).toString();
                break;

            case 5:
                color = new StringBuilder().append('0').append(color).toString();
                break;
        }
        return color;
    }

    public boolean addFriend(L2PcInstance invitor, L2PcInstance requestor) {
        CharacterEntity characterInvitor = invitor.getCharacter();
        CharacterEntity characterRequestor = requestor.getCharacter();

        CharacterFriendEntity inviteFriend = new CharacterFriendEntity();
        inviteFriend.setFriendName(requestor.getCharacter().getCharName());
        inviteFriend.setFriend(requestor.getCharacter());
        inviteFriend.setNotBlocked(1);


        CharacterFriendEntity requestFriend = new CharacterFriendEntity();
        requestFriend.setFriendName(invitor.getCharacter().getCharName());
        requestFriend.setFriend(invitor.getCharacter());
        requestFriend.setNotBlocked(1);


        characterInvitor.getFriends().put(inviteFriend.getFriend().getId(), CharacterManager.getInstance().addFriend(characterInvitor, inviteFriend));
        characterRequestor.getFriends().put(requestFriend.getFriend().getId(), CharacterManager.getInstance().addFriend(characterRequestor, requestFriend));
        return characterInvitor != null && characterRequestor != null;
    }

    public void deleteCharacter(int objectId) {
        Connection con = null;

        try {
            con = L2DatabaseFactory.getInstance().getConnection(false);
            PreparedStatement statement;

            L2PcInstance player = L2World.getInstance().getPlayer(objectId);
            CharacterEntity character;

            if (player != null) {//player is online
                character = player.getCharacter();
                player.getClient().onForcedDisconnection(false);
            } else {//Player offline
                character = CharacterManager.getInstance().get(((Number) objectId).longValue());
            }
            if (character == null) {
                return;
            }
            //Get all character friends, delete character of friend. Delete character friends data of database
            for (CharacterFriendEntity friend : character.getFriends().values()) {
                String friendName = friend.getFriendName();
                L2PcInstance friendPlayer = L2World.getInstance().getPlayer(friendName);

                if (friendPlayer == null) {
                    return;
                }
                friendPlayer.getFriendList().remove(character.getCharName());
                CharacterFriendEntity characterFriend = null;
                for (CharacterFriendEntity friend1 : friendPlayer.getCharacter().getFriends().values()) {
                    if (friend1.getFriendName().equals(character.getCharName())) {
                        characterFriend = friend1;
                    }
                }
                if (characterFriend != null) {
                    friendPlayer.getCharacter().getFriends().remove(characterFriend);
                    CharacterManager.getInstance().deleteFriend(characterFriend.getCharacter(), characterFriend.getFriend());
                }
                friendPlayer.sendPacket(new FriendList(friendPlayer));
            }

         /*   statement = con.prepareStatement("DELETE FROM character_hennas WHERE char_obj_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;*/

           /* statement = con.prepareStatement("DELETE FROM character_macroses WHERE char_obj_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;*/

            /*statement = con.prepareStatement("DELETE FROM character_quests WHERE char_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;*/

            /*statement = con.prepareStatement("DELETE FROM character_recipebook WHERE char_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;*/

           /* statement = con.prepareStatement("DELETE FROM character_shortcuts WHERE char_obj_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;*/

            /*statement = con.prepareStatement("DELETE FROM character_skills WHERE char_obj_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;*/

            /*statement = con.prepareStatement("DELETE FROM character_skills_save WHERE char_obj_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;*/

            /*statement = con.prepareStatement("DELETE FROM character_subclasses WHERE char_obj_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;*/

            statement = con.prepareStatement("DELETE FROM heroes WHERE charId=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;

            statement = con.prepareStatement("DELETE FROM olympiad_nobles WHERE charId=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;

            statement = con.prepareStatement("DELETE FROM seven_signs WHERE char_obj_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;

            statement = con.prepareStatement("DELETE FROM pets WHERE item_obj_id IN (SELECT object_id FROM items WHERE items.owner_id=?)");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;

            statement = con.prepareStatement("DELETE FROM augmentations WHERE item_id IN (SELECT object_id FROM items WHERE items.owner_id=?)");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;


            statement = con.prepareStatement("DELETE FROM merchant_lease WHERE player_id=?");
            statement.setInt(1, objectId);
            statement.execute();
            statement.close();
            statement = null;

            CharacterManager.getInstance().deleteItemsById(character, Arrays.asList(0));
        } catch (Exception e) {
            LOGGER.error("", e);

            LOGGER.warn("Data error on deleting char: " + e);
        } finally {
            CloseUtil.close(con);
            con = null;
        }
    }

    /**
     * Retrieve a L2PcInstance from the characters table of the database and add it in _allObjects of the L2world (call
     * restore method).<BR>
     * <BR>
     * <B><U> Actions</U> :</B><BR>
     * <BR>
     * <li>Retrieve the L2PcInstance from the characters table of the database</li> <li>Add the L2PcInstance object in
     * _allObjects</li> <li>Set the x,y,z position of the L2PcInstance and make it invisible</li> <li>Update the
     * overloaded status of the L2PcInstance</li><BR>
     * <BR>
     *
     * @param objectId Identifier of the object to initialized
     * @return The L2PcInstance loaded from the database
     */
    public L2PcInstance load(int objectId) {
        return restore(objectId);
    }

    /**
     * Retrieve a L2PcInstance from the characters table of the database and add it in _allObjects of the L2world.<BR>
     * <BR>
     * <B><U> Actions</U> :</B><BR>
     * <BR>
     * <li>Retrieve the L2PcInstance from the characters table of the database</li> <li>Add the L2PcInstance object in
     * _allObjects</li> <li>Set the x,y,z position of the L2PcInstance and make it invisible</li> <li>Update the
     * overloaded status of the L2PcInstance</li><BR>
     * <BR>
     *
     * @param objectId Identifier of the object to initialized
     * @return The L2PcInstance loaded from the database
     */
    public L2PcInstance restore(int objectId) {
        L2PcInstance player = null;
        double curHp = 0;
        double curCp = 0;
        double curMp = 0;
        try {
            CharacterEntity character = CharacterManager.getInstance().get(((Number) objectId).longValue());
            if (character == null) {
                return null;
            }

            final int activeClassId = character.getClassId();
            final boolean female = character.getSex() != 0;
            final L2PcTemplate template = CharTemplateTable.getInstance().getTemplate(activeClassId);
            PcAppearance app = new PcAppearance((byte) character.getFace(), (byte) character.getHairColor(), (byte) character.getHairStyle(), female);

            player = new L2PcInstance(objectId, template, character.getAccountName(), app);
            if (player.getCharacter() == null) {
                player.setCharacter(character);
            }
            player.setName(player.getCharacter().getCharName());
            player.setLastAccess(player.getCharacter().getLastAccess());

            player.getStat().setExp(player.getCharacter().getExp());
            player.setExpBeforeDeath(player.getCharacter().getExpBeforeDeath());
            player.getStat().setLevel(player.getCharacter().getLevel());
            player.getStat().setSp(player.getCharacter().getSp());

            player.setWantsPeace(player.getCharacter().getWantSpeace());

            player.setHeading(player.getCharacter().getHeading());

            player.setKarma(player.getCharacter().getKarma());
            player.setPvpKills(player.getCharacter().getPvpKillCount());
            player.setPkKills(player.getCharacter().getPkKillCount());
            player.setOnlineTime(player.getCharacter().getOnlineTime());
            player.setNewbie(player.getCharacter().getNewbie() == 1);
            player.setNoble(player.getCharacter().getNobless() == 1);
            player.setClanJoinExpiryTime(player.getCharacter().getClanJoinExpiryTime());
            player.setFirstLog(player.getCharacter().getFirstLog());
            player.setPcBangPoint(player.getCharacter().getPcPoint());
            app = null;

            if (player.getClanJoinExpiryTime() < System.currentTimeMillis()) {
                player.setClanJoinExpiryTime(0);
            }
            player.setClanCreateExpiryTime(player.getCharacter().getClanCreateExpiryTime());
            if (player.getClanCreateExpiryTime() < System.currentTimeMillis()) {
                player.setClanCreateExpiryTime(0);
            }

            int clanId = player.getCharacter().getClanId();
            player.setPowerGrade(player.getCharacter().getPowerGrade());
            player.setPledgeType(player.getCharacter().getSubPledge());
            player.setLastRecomUpdate(player.getCharacter().getLastRecomDate());

            if (clanId > 0) {
                player.setClan(ClanTable.getInstance().getClan(clanId));
            }

            if (player.getClan() != null) {
                if (player.getClan().getLeaderId() != player.getObjectId()) {
                    if (player.getPowerGrade() == 0) {
                        player.setPowerGrade(5);
                    }
                    player.setClanPrivileges(player.getClan().getRankPrivs(player.getPowerGrade()));
                } else {
                    player.setClanPrivileges(L2Clan.CP_ALL);
                    player.setPowerGrade(1);
                }
            } else {
                player.setClanPrivileges(L2Clan.CP_NOTHING);
            }

            player.setDeleteTimer(player.getCharacter().getDeleteTime());

            player.setTitle(player.getCharacter().getTitle());
            player.setAccessLevel(player.getCharacter().getAccessLevel().ordinal());
            player.setFistsWeaponItem(player.findFistsWeaponItem(activeClassId));
            player.setUptime(System.currentTimeMillis());

            curHp = player.getCharacter().getCurHp();
            curCp = player.getCharacter().getCurCp();
            curMp = player.getCharacter().getCurMp();


            //Check recs
            player.checkRecom(player.getCharacter().getRecHave(), player.getCharacter().getRecLeft());

            player.setClassIndex(0);
            try {
                player.setBaseClass(player.getCharacter().getBaseClass());
            } catch (Exception e) {
                LOGGER.error("", e);

                player.setBaseClass(activeClassId);
            }

            // Restore Subclass Data (cannot be done earlier in function)
            if (player.restoreSubClassData(player)) {
                if (activeClassId != player.getBaseClass()) {
                    for (SubClass subClass : player.getSubClasses().values()) {
                        if (subClass.getClassId() == activeClassId) {
                            player.setClassIndex(subClass.getClassIndex());
                        }
                    }
                }
            }
            if (player.getClassIndex() == 0 && activeClassId != player.getBaseClass()) {
                // Subclass in use but doesn't exist in DB -
                // a possible restart-while-modifysubclass cheat has been attempted.
                // Switching to use base class
                player.setClassId(player.getBaseClass());
                LOGGER.warn("Player " + player.getName() + " reverted to base class. Possibly has tried a relogin exploit while subclassing.");
            } else {
                player.setActiveClass(activeClassId);
            }

            player.setApprentice(player.getCharacter().getApprentice());
            player.setSponsor(player.getCharacter().getSponsor().intValue());
            player.setLvlJoinedAcademy(player.getCharacter().getLvlJoinedAcademy());
            player.setIsIn7sDungeon(player.getCharacter().getSevenSingsDungeon() == 1);

            player.setPunishLevel(player.getCharacter().getPunishLevel());
            if (player.getPunishLevel() != NONE) {
                player.setPunishTimer(player.getCharacter().getPunishTimer());
            } else {
                player.setPunishTimer(0);
            }


            try {
                player.getAppearance().setNameColor(Integer.decode(new StringBuilder().append("0x").append(player.getCharacter().getNameColor()).toString()));
                player.getAppearance().setTitleColor(Integer.decode(new StringBuilder().append("0x").append(player.getCharacter().getTitleColor()).toString()));
            } catch (Exception e) {
                LOGGER.error("", e);

                //leave them as default
            }

            CursedWeaponsManager.getInstance().checkPlayer(player);

            player.setAllianceWithVarkaKetra(player.getCharacter().getVarkaKetraAlly());

            player.setDeathPenaltyBuffLevel(player.getCharacter().getDeathPenaltyLevel());
            player.setAio(character.getAio() == 1);
            player.setAioEndTime(player.getCharacter().getAioEnd());

            // Set the x,y,z position of the L2PcInstance and make it invisible
            player.setXYZInvisible(player.getCharacter().getX(), player.getCharacter().getY(), player.getCharacter().getZ());

            List<CharacterEntity> characters = CharacterManager.getInstance().get(player.getAccountName());
            for (CharacterEntity character1 : characters) {
                Long charId = character1.getId();
                String charName = character1.getCharName();
                player.getAllChars().put(charId.intValue(), charName);
            }
            // Retrieve from the database all secondary data of this L2PcInstance
            // and reward expertise/lucky skills if necessary.
            // Note that Clan, Noblesse and Hero skills are given separately and not here.
            player.restoreCharData();
            player.rewardSkills();
            // Retrieve from the database all skills of this L2PcInstance and add them to _skills
            // Retrieve from the database all items of this L2PcInstance and add them to _inventory
            player.getInventory().restore();
            if (!GameServerConfig.WAREHOUSE_CACHE) {
                player.getWarehouse();
            }
            player.getFreight().restore();
            // Restore pet if exists in the world
            player.setPet(L2World.getInstance().getPet(player.getObjectId()));
            if (player.getPet() != null) {
                player.getPet().setOwner(player);
            }

            // Update the overloaded status of the L2PcInstance
            player.refreshOverloaded();

            player.restoreFriendList();
        } catch (Exception e) {
            LOGGER.error("Could not restore char data: ", e);
        }

        if (player != null) {
            player.fireEvent(BaseExtender.EventType.LOAD.name, (Object[]) null);

            //once restored all the skill status, update current CP, MP and HP
            player.setCurrentHpDirect(curHp);
            player.setCurrentCpDirect(curCp);
            player.setCurrentMpDirect(curMp);
        }
        return player;
    }
}
