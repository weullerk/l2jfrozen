package com.l2jfrozen.gameserver.powerpak;

/**
 * L2JFrozen
 */

import com.l2jfrozen.configuration.GameServerConfig;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Map;

public final class StringTable {
    private static final Logger LOGGER = LoggerFactory.getLogger(StringTable.class.getName());
    private Map<String, String> _messagetable = new FastMap<String, String>();

    public StringTable(String name) {
        if (!name.startsWith(".") && !name.startsWith("/"))
            name = GameServerConfig.DATAPACK_ROOT.getPath() + "/data/messages/" + name;

        File f = new File(name);
        load(f);

    }

    public void load(File f) {
        if (!f.exists())
            return;

        FileInputStream fis = null;
        InputStreamReader isr = null;
        LineNumberReader lnr = null;

        try {

            fis = new FileInputStream(f);
            isr = new InputStreamReader(fis, "UTF-8");
            lnr = new LineNumberReader(isr);

            String line;
            while ((line = lnr.readLine()) != null) {
                if (line.length() > 1 && line.getBytes()[0] == (byte) 0x3F) {
                    line = line.substring(1);
                }
                int iPos = line.indexOf("#");
                if (iPos != -1) line = line.substring(0, iPos);
                iPos = line.indexOf("=");
                if (iPos != -1) {
                    _messagetable.put(line.substring(0, iPos).trim(), line.substring(iPos + 1));
                }
            }
        } catch (IOException e) {
            LOGGER.error("", e);

        } finally {

            if (lnr != null)
                try {
                    lnr.close();
                } catch (Exception e1) {
                    LOGGER.error("unhandled exception", e1);
                }

            if (isr != null)
                try {
                    isr.close();
                } catch (Exception e1) {
                    LOGGER.error("unhandled exception", e1);
                }

            if (fis != null)
                try {
                    fis.close();
                } catch (Exception e1) {
                    LOGGER.error("unhandled exception", e1);
                }

        }

    }

    public String Message(String MsgId) {
        if (_messagetable.containsKey(MsgId))
            return _messagetable.get(MsgId);
        return MsgId;
    }

    public String format(String MsgId, Object... params) {
        String msg = Message(MsgId);
        return String.format(msg, params);
    }

}
