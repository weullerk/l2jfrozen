package com.l2jfrozen.gameserver.ai.accessor;

/**
 * User: vadimDidenko
 * Date: 17.12.13
 * Time: 23:08
 */
public abstract class ControllableAIAcessor extends AIAccessor {
    @Override
    public void detachAI() {
    }
}
