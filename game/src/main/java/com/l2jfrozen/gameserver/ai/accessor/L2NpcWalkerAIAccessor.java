package com.l2jfrozen.gameserver.ai.accessor;

import com.l2jfrozen.gameserver.ai.L2AttackableAI;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.actor.instance.L2NpcWalkerInstance;
import com.l2jfrozen.gameserver.model.actor.stat.NpcStat;

/**
 * User: vadimDidenko
 * Date: 17.12.13
 * Time: 23:07
 */
public abstract class L2NpcWalkerAIAccessor<S extends NpcStat, A extends L2AttackableAI, T extends L2Character<S, A>> extends AIAccessor<S, A, L2NpcWalkerInstance<S, A>> {
    /**
     * AI can't be deattached.
     */
    @Override
    public void detachAI() {
    }
}
