/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.util.database;

import com.l2jfrozen.configuration.DatabaseConfig;
import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class L2DatabaseFactory {
    private ComboPooledDataSource _source;
    private static final Logger LOGGER = LoggerFactory.getLogger(L2DatabaseFactory.class);
    // =========================================================
    // Data Field
    protected static L2DatabaseFactory _instance;
    protected DatabaseConfig databaseConfig;

    // =========================================================
    // Property - Public
    public static L2DatabaseFactory getInstance() {
        if (_instance == null) {

            _instance = new L2DatabaseFactory();
        }
        return _instance;
    }

    public void initialize() throws SQLException {
        try {

            if (databaseConfig.getMaxConnections() < 2) {
                databaseConfig.setMaxConnections(2);
                LOGGER.warn("A minimum of " + databaseConfig.getMaxConnections() + " db connections are required.");
            }

            _source = new ComboPooledDataSource();
            _source.setAutoCommitOnClose(true);

            _source.setInitialPoolSize(10);
            _source.setMinPoolSize(10);
            _source.setMaxPoolSize(Math.max(10, databaseConfig.getMaxConnections()));

            _source.setAcquireRetryAttempts(0); // try to obtain connections indefinitely (0 = never quit)
            _source.setAcquireRetryDelay(500); // 500 milliseconds wait before try to acquire connection again
            _source.setCheckoutTimeout(0); // 0 = wait indefinitely for new connection
            // if pool is exhausted
            _source.setAcquireIncrement(5); // if pool is exhausted, get 5 more connections at a time
            // cause there is a "long" delay on acquire connection
            // so taking more than one connection at once will make connection pooling
            // more effective.

            // this "connection_test_table" is automatically created if not already there
            _source.setAutomaticTestTable("connection_test_table");
            _source.setTestConnectionOnCheckin(false);

            // testing OnCheckin used with IdleConnectionTestPeriod is faster than  testing on checkout

            _source.setIdleConnectionTestPeriod(3600); // test idle connection every 60 sec
            _source.setMaxIdleTime(databaseConfig.getMaxIdleTime()); // 0 = idle connections never expire
            // *THANKS* to connection testing configured above
            // but I prefer to disconnect all connections not used
            // for more than 1 hour

            // enables statement caching,  there is a "semi-bug" in c3p0 0.9.0 but in 0.9.0.2 and later it's fixed
            _source.setMaxStatementsPerConnection(100);

            _source.setBreakAfterAcquireFailure(false); // never fail if any way possible
            // setting this to true will make
            // c3p0 "crash" and refuse to work
            // till restart thus making acquire
            // errors "FATAL" ... we don't want that
            // it should be possible to recover
            _source.setDriverClass(databaseConfig.getDriver());
            _source.setJdbcUrl(databaseConfig.getUrl());
            _source.setUser(databaseConfig.getLogin());
            _source.setPassword(databaseConfig.getPassword());

			/* Test the connection */
            _source.getConnection().close();

            if (databaseConfig.isDebugMode())
                LOGGER.debug("Database Connection Working");
        } catch (SQLException x) {
            if (databaseConfig.isDebugMode())
                LOGGER.error("Database Connection FAILED");
            // re-throw the exception
            throw x;
        } catch (Exception e) {
            if (databaseConfig.isDebugMode())
                LOGGER.error("Database Connection FAILED");
            throw new SQLException("Could not init DB connection:" + e.getMessage());
        }
    }

    protected L2DatabaseFactory() {

    }

    public static void close(Connection con) {
        if (con == null)
            return;

        try {
            con.close();
        } catch (SQLException e) {
            LOGGER.error("Failed to close database connection! " + e);
        }
    }

    public final String prepQuerySelect(String[] fields, String tableName, String whereClause, boolean returnOnlyTopRecord) {

        String query = "SELECT " + safetyString(fields) + " FROM " + tableName + " WHERE " + whereClause + " Limit 1 ";
        return query;
    }

    public final String safetyString(String... whatToCheck) {
        // NOTE: Use brace as a safty precaution just incase name is a reserved word
        final char braceLeft;
        final char braceRight;

        braceLeft = '`';
        braceRight = '`';

        int length = 0;

        for (String word : whatToCheck) {
            length += word.length() + 4;
        }

        final StringBuilder sbResult = new StringBuilder(length);

        for (String word : whatToCheck) {
            if (sbResult.length() > 0) {
                sbResult.append(", ");
            }

            sbResult.append(braceLeft);
            sbResult.append(word);
            sbResult.append(braceRight);
        }

        return sbResult.toString();
    }

    public Connection getConnection() throws SQLException {
        return getConnection(true);
    }

    public void shutdown() {
        try {
            //sleep 10 seconds before the final source shutdown
            Thread.sleep(10000);
        } catch (Exception e) {
            //nothing
        }

        try {
            _source.close();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("", e);
        }
        try {
            _source = null;
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("", e);
        }
    }

    public Connection getConnection(boolean checkclose) throws SQLException {
        Connection con = null;

        while (con == null && _source != null) {
            try {
                con = _source.getConnection();
                if (checkclose)
                    ThreadPoolManager.getInstance().scheduleGeneral(new ConnectionCloser(con, new RuntimeException()), databaseConfig.getConnectionTimeout());
            } catch (SQLException e) {
                LOGGER.error("getConnection() failed, trying again" + e);
            }
        }

        return con;
    }

    public Connection getConnection(long max_connection_time) throws SQLException {
        Connection con = null;

        while (con == null && _source != null) {
            try {
                con = _source.getConnection();
                ThreadPoolManager.getInstance().scheduleGeneral(new ConnectionCloser(con, new RuntimeException()), max_connection_time);

            } catch (SQLException e) {
                LOGGER.error("getConnection() failed, trying again \n" + e);
            }
        }

        return con;
    }

    public int getBusyConnectionCount() throws SQLException {
        return _source.getNumBusyConnectionsDefaultUser();
    }

    public void setDatabaseConfig(DatabaseConfig databaseConfig) {
        this.databaseConfig = databaseConfig;
    }

    public DatabaseConfig getDatabaseConfig() {
        return databaseConfig;
    }
}
