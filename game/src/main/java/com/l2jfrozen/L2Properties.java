/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen;

import com.l2jfrozen.configuration.GameServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

public final class L2Properties extends Properties {
    protected static final Logger LOGGER = LoggerFactory.getLogger(GameServerConfig.class);
    private static final long serialVersionUID = -4599023842346938325L;
    private boolean _warn = false;

    public L2Properties() {
    }

    public L2Properties(String name) throws IOException {
        load(new FileInputStream(name));
    }

    public L2Properties(File file) throws IOException {
        load(new FileInputStream(file));
    }

    public L2Properties(InputStream inStream) {
        load(inStream);
    }

    public L2Properties(Reader reader) {
        load(reader);
    }

    public L2Properties setLog(boolean warn) {
        _warn = warn;

        return this;
    }

    public void load(String name) throws IOException {
        load(new FileInputStream(name));
    }

    public void load(File file) throws IOException {
        load(new FileInputStream(file));
    }

    @Override
    public synchronized void load(InputStream inStream) {
        try {
            super.load(inStream);
        } catch (IOException e) {
            LOGGER.error("", e);
        } finally {
            if (inStream != null)
                try {
                    inStream.close();
                } catch (IOException e) {
                    LOGGER.error("", e);
                }
        }
    }

    @Override
    public synchronized void load(Reader reader) {
        try {
            super.load(reader);
        } catch (IOException e) {
            LOGGER.error("", e);
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.error("", e);
                }
        }
    }

    @Override
    public String getProperty(String key) {
        String property = super.getProperty(key);

        if (property == null) {
            if (_warn) {
                LOGGER.warn("L2Properties: Missing property for key - " + key);
            }
            return null;
        }
        return property.trim();
    }

    @Override
    public String getProperty(String key, String defaultValue) {
        String property = super.getProperty(key, defaultValue);

        if (property == null) {
            if (_warn) {
                LOGGER.warn("L2Properties: Missing defaultValue for key - " + key);
            }
            return null;
        }
        return property.trim();
    }
}