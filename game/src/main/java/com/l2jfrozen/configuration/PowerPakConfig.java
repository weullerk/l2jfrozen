/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.configuration;

import com.l2jfrozen.gameserver.powerpak.L2Utils;
import com.l2jfrozen.gameserver.templates.L2Item;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author Nick
 */
@Component
public class PowerPakConfig {

    //private static PowerPakConfig instance;
    private static final Logger LOGGER = LoggerFactory.getLogger(PowerPakConfig.class);

    public static boolean ENGRAVER_ENABLED;
    public static int ENGRAVE_PRICE = 0;
    public static int ENGRAVE_PRICE_ITEM = 57;
    public static int ENGRAVER_X = 82270;
    public static int ENGRAVER_Y = 149660;
    public static int ENGRAVER_Z = -3495;
    public static int MAX_ENGRAVED_ITEMS_PER_CHAR;
    public static boolean SPAWN_ENGRAVER = true;
    public static boolean ENGRAVE_ALLOW_DESTROY;
    public static ArrayList<Integer> ENGRAVE_EXCLUDED_ITEMS = new ArrayList<Integer>();
    public static ArrayList<Integer> ENGRAVE_ALLOW_GRADE = new ArrayList<Integer>();
    public static int BUFFER_NPC;
    public static boolean BUFFER_ENABLED;
    public static List<String> BUFFER_EXCLUDE_ON = new FastList<String>();
    public static String BUFFER_COMMAND;
    public static int BUFFER_PRICE;
    public static boolean BUFFER_USEBBS;
    public static boolean BUFFER_USECOMMAND;
    public static FastMap<Integer, Integer> FIGHTER_SKILL_LIST;
    public static FastMap<Integer, Integer> MAGE_SKILL_LIST;
    public static int NPCBUFFER_MAX_SCHEMES;
    public static int NPCBUFFER_MAX_SKILLS;
    public static boolean NPCBUFFER_STORE_SCHEMES;
    public static int NPCBUFFER_STATIC_BUFF_COST;
    public static List<String> GLOBALGK_EXCLUDE_ON;
    public static boolean GLOBALGK_ENABDLED;
    public static boolean GLOBALGK_USEBBS;
    public static int GLOBALGK_NPC;
    public static int GLOBALGK_PRICE;
    public static int GLOBALGK_TIMEOUT;
    public static String GLOBALGK_COMMAND;
    public static boolean GLOBALGK_USECOMMAND;
    public static int GMSHOP_NPC;
    public static boolean GMSHOP_ENABLED;
    public static boolean GMSHOP_USEBBS;
    public static String GMSHOP_COMMAND;
    public static List<String> GMSHOP_EXCLUDE_ON;
    public static boolean GMSHOP_USECOMMAND;
    public static boolean L2TOPDEMON_ENABLED;
    public static int L2TOPDEMON_POLLINTERVAL;
    public static boolean L2TOPDEMON_IGNOREFIRST;
    public static int L2TOPDEMON_MIN;
    public static int L2TOPDEMON_MAX;
    public static int L2TOPDEMON_ITEM;
    public static String L2TOPDEMON_MESSAGE;
    public static String L2TOPDEMON_URL;
    public static String L2TOPDEMON_PREFIX;
    //Vote Reward System Configs
    public static int VOTES_FOR_REWARD;
    public static String VOTES_REWARDS;
    public static String VOTES_SITE_TOPZONE_URL;
    public static String VOTES_SITE_HOPZONE_URL;
    public static FastMap<Integer, Integer> VOTES_REWARDS_LIST;
    public static int VOTES_SYSYEM_INITIAL_DELAY;
    public static int VOTES_SYSYEM_STEP_DELAY;
    public static String SERVER_WEB_SITE;
    public static boolean AUTOVOTEREWARD_ENABLED;
    public static boolean ENABLE_SAY_SOCIAL_ACTIONS;
    public static boolean CHAR_REPAIR;
    public static boolean VOTE_COMMAND_ENABLED;

    @Autowired
    private ConfigManager configManager;

    public PowerPakConfig() {
        LOGGER.info("Initialize PowerPakConfig config");
    }

    @PostConstruct
    public void load() {
        try {
            ENGRAVER_ENABLED = configManager.getBoolean("EngraveEnabled");
            ENGRAVE_PRICE = configManager.getInteger("EngravePrice");
            ENGRAVE_PRICE_ITEM = configManager.getInteger("EngravePriceItem");
            SPAWN_ENGRAVER = configManager.getBoolean("EngraveSpawnNpc");
            ENGRAVE_ALLOW_DESTROY = configManager.getBoolean("EngraveAllowDestroy");
            MAX_ENGRAVED_ITEMS_PER_CHAR = configManager.getInteger("EngraveMaxItemsPerChar");
            String str = configManager.getString("EngraveNpcLocation").trim();
            if (str.length() > 0) {
                StringTokenizer st = new StringTokenizer(str, " ");
                if (st.hasMoreTokens()) {
                    ENGRAVER_X = Integer.parseInt(st.nextToken());
                }
                if (st.hasMoreTokens()) {
                    ENGRAVER_Y = Integer.parseInt(st.nextToken());
                }
                if (st.hasMoreTokens()) {
                    ENGRAVER_Z = Integer.parseInt(st.nextToken());
                }
            }
            str = configManager.getString("EngraveExcludeItems").trim();
            if (str.length() > 0) {
                StringTokenizer st = new StringTokenizer(str, ",");
                while (st.hasMoreTokens()) {
                    ENGRAVE_EXCLUDED_ITEMS.add(Integer.parseInt(st.nextToken().trim()));
                }
            }
            str = configManager.getString("EngraveAllowGrades").toLowerCase();
            if (str.indexOf("none") != -1 || str.indexOf("all") != -1) {
                ENGRAVE_ALLOW_GRADE.add(L2Item.CRYSTAL_NONE);
            }

            if (str.indexOf("a") != -1 || str.indexOf("all") != -1) {
                ENGRAVE_ALLOW_GRADE.add(L2Item.CRYSTAL_A);
            }

            if (str.indexOf("b") != -1 || str.indexOf("all") != -1) {
                ENGRAVE_ALLOW_GRADE.add(L2Item.CRYSTAL_B);
            }

            if (str.indexOf("c") != -1 || str.indexOf("all") != -1) {
                ENGRAVE_ALLOW_GRADE.add(L2Item.CRYSTAL_C);
            }

            if (str.indexOf("d") != -1 || str.indexOf("all") != -1) {
                ENGRAVE_ALLOW_GRADE.add(L2Item.CRYSTAL_D);
            }

            if (str.indexOf("s") != -1 || str.indexOf("all") != -1) {
                ENGRAVE_ALLOW_GRADE.add(L2Item.CRYSTAL_S);
            }

            BUFFER_ENABLED = configManager.getBoolean("BufferEnabled");
            StringTokenizer st = new StringTokenizer(configManager.getString("BufferExcludeOn"), " ");
            while (st.hasMoreTokens()) {
                BUFFER_EXCLUDE_ON.add(st.nextToken());
            }
            BUFFER_COMMAND = configManager.getString("BufferCommand");
            BUFFER_NPC = configManager.getInteger("BufferNpcId");
            BUFFER_PRICE = configManager.getInteger("BufferPrice");
            BUFFER_USEBBS = configManager.getBoolean("BufferUseBBS");
            BUFFER_USECOMMAND = configManager.getBoolean("BufferUseCommand");

            FIGHTER_SKILL_LIST = new FastMap<Integer, Integer>();
            MAGE_SKILL_LIST = new FastMap<Integer, Integer>();

            String[] fPropertySplit;
            fPropertySplit = configManager.getString("FighterSkillList").split(";");

            String[] mPropertySplit;
            mPropertySplit = configManager.getString("MageSkillList").split(";");

            for (String skill : fPropertySplit) {
                String[] skillSplit = skill.split(",");
                if (skillSplit.length != 2) {
                    LOGGER.warn("SYS_LOG: " + "[FighterSkillList]: invalid config property -> FighterSkillList \"" + skill + "\"");
                } else {
                    try {
                        FIGHTER_SKILL_LIST.put(Integer.parseInt(skillSplit[0]), Integer.parseInt(skillSplit[1]));
                    } catch (NumberFormatException nfe) {
                        LOGGER.error("", nfe);

                        if (!skill.equals("")) {
                            LOGGER.warn("SYS_LOG: " + "[FighterSkillList]: invalid config property -> FighterSkillList \"" + skillSplit[0] + "\"" + skillSplit[1]);
                        }
                    }
                }
            }

            for (String skill : mPropertySplit) {
                String[] skillSplit = skill.split(",");
                if (skillSplit.length != 2) {
                    LOGGER.warn("SYS_LOG: " + "[MageSkillList]: invalid config property -> MageSkillList \"" + skill + "\"");
                } else {
                    try {
                        MAGE_SKILL_LIST.put(Integer.parseInt(skillSplit[0]), Integer.parseInt(skillSplit[1]));
                    } catch (NumberFormatException nfe) {
                        LOGGER.error("", nfe);

                        if (!skill.equals("")) {
                            LOGGER.warn("SYS_LOG: " + "[MageSkillList]: invalid config property -> MageSkillList \"" + skillSplit[0] + "\"" + skillSplit[1]);
                        }
                    }
                }
            }

            NPCBUFFER_MAX_SCHEMES = configManager.getInteger("NPCBufferMaxSchemesPerChar");
            NPCBUFFER_MAX_SKILLS = configManager.getInteger("NPCBufferMaxSkllsperScheme");
            NPCBUFFER_STORE_SCHEMES = configManager.getBoolean("NPCBufferStoreSchemes");
            NPCBUFFER_STATIC_BUFF_COST = configManager.getInteger("NPCBufferStaticCostPerBuff");

            GLOBALGK_NPC = configManager.getInteger("GKNpcId");
            GLOBALGK_ENABDLED = configManager.getBoolean("GKEnabled");
            GLOBALGK_COMMAND = configManager.getString("GKCommand");
            GLOBALGK_TIMEOUT = configManager.getInteger("GKTimeout");
            if (GLOBALGK_TIMEOUT < 1) {
                GLOBALGK_TIMEOUT = 1;
            }
            GLOBALGK_PRICE = configManager.getInteger("GKPrice");
            GLOBALGK_USECOMMAND = configManager.getBoolean("GKUseCommand");
            GLOBALGK_USEBBS = configManager.getBoolean("GKUseBBS");
            GLOBALGK_EXCLUDE_ON = new FastList<String>();
            st = new StringTokenizer(configManager.getString("GKExcludeOn"), " ");
            while (st.hasMoreTokens()) {
                GLOBALGK_EXCLUDE_ON.add(st.nextToken().toUpperCase());
            }

            GMSHOP_NPC = configManager.getInteger("GMShopNpcId");
            GMSHOP_ENABLED = configManager.getBoolean("GMShopEnabled");
            GMSHOP_COMMAND = configManager.getString("GMShopCommand");
            GMSHOP_USEBBS = configManager.getBoolean("GMShopUseBBS");
            GMSHOP_USECOMMAND = configManager.getBoolean("GMShopUseCommand");
            GMSHOP_EXCLUDE_ON = new FastList<String>();
            st = new StringTokenizer(configManager.getString("GMShopExcludeOn"), " ");
            while (st.hasMoreTokens()) {
                GMSHOP_EXCLUDE_ON.add(st.nextToken().toUpperCase());
            }

            L2TOPDEMON_ENABLED = configManager.getBoolean("L2TopDeamonEnabled");
            L2TOPDEMON_URL = configManager.getString("L2TopDeamonURL");
            L2TOPDEMON_POLLINTERVAL = configManager.getInteger("L2TopDeamonPollInterval");
            L2TOPDEMON_PREFIX = configManager.getString("L2TopDeamonPrefix");
            L2TOPDEMON_ITEM = configManager.getInteger("L2TopDeamonRewardItem");
            L2TOPDEMON_MESSAGE = L2Utils.loadMessage(configManager.getString("L2TopDeamonMessage"));
            L2TOPDEMON_MIN = configManager.getInteger("L2TopDeamonMin");
            L2TOPDEMON_MAX = configManager.getInteger("L2TopDeamonMax");
            L2TOPDEMON_IGNOREFIRST = configManager.getBoolean("L2TopDeamonDoNotRewardAtFirstTime");
            //if (ItemTable.getInstance().getTemplate(L2TOPDEMON_ITEM) == null) {
            //    L2TOPDEMON_ENABLED = false;
            //    System.err.println("Powerpak: Unknown item (" + L2TOPDEMON_ITEM + ") as vote reward. Vote disabled");
            //}

            AUTOVOTEREWARD_ENABLED = configManager.getBoolean("VoteRewardSystem");
            VOTES_FOR_REWARD = configManager.getInteger("VotesRequiredForReward");
            VOTES_SYSYEM_INITIAL_DELAY = configManager.getInteger("VotesSystemInitialDelay");
            VOTES_SYSYEM_STEP_DELAY = configManager.getInteger("VotesSystemStepDelay");
            VOTES_SITE_HOPZONE_URL = configManager.getString("VotesSiteHopZoneUrl");
            VOTES_SITE_TOPZONE_URL = configManager.getString("VotesSiteTopZoneUrl");
            SERVER_WEB_SITE = configManager.getString("ServerWebSite");
            VOTES_REWARDS = configManager.getString("VotesRewards");
            VOTES_REWARDS_LIST = new FastMap<Integer, Integer>();

            String[] splitted_VOTES_REWARDS = VOTES_REWARDS.split(";");

            for (String reward : splitted_VOTES_REWARDS) {

                String[] item_count = reward.split(",");

                if (item_count.length != 2) {
                    LOGGER.warn("SYS_LOG: " + "[VotesRewards]: invalid config property -> VotesRewards \"" + VOTES_REWARDS + "\"");
                } else
                    VOTES_REWARDS_LIST.put(Integer.parseInt(item_count[0]), Integer.parseInt(item_count[1]));

            }

            ENABLE_SAY_SOCIAL_ACTIONS = configManager.getBoolean("EnableSocialSayActions");
            CHAR_REPAIR = configManager.getBoolean("CharacterRepair");
            VOTE_COMMAND_ENABLED = configManager.getBoolean("VoteCommandEnabled");

        } catch (Exception e) {
            LOGGER.error("", e);
        }
    }

}
