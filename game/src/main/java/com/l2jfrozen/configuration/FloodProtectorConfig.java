package com.l2jfrozen.configuration;

import com.l2jfrozen.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * User: vdidenko
 * Date: 11/21/13
 * Time: 10:56 PM
 */
@Component
public class FloodProtectorConfig {
    public static final String FLOOD_PROTECTOR_NAME = "FloodProtector";
    //--------------------------------------------------
    // FloodProtector Settings
    //--------------------------------------------------
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_USE_ITEM;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_ROLL_DICE;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_FIREWORK;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_ITEM_PET_SUMMON;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_HERO_VOICE;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_GLOBAL_CHAT;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_SUBCLASS;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_DROP_ITEM;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_SERVER_BYPASS;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_MULTISELL;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_TRANSACTION;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_MANUFACTURE;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_MANOR;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_CHARACTER_SELECT;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_UNKNOWN_PACKETS;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_PARTY_INVITATION;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_SAY_ACTION;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_MOVE_ACTION;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_GENERIC_ACTION;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_MACRO;
    public static FloodProtectorConfigItem FLOOD_PROTECTOR_POTION;
    @Autowired
    private ConfigManager configManager;

    private static final Logger LOGGER = LoggerFactory.getLogger(FloodProtectorConfig.class);

    public FloodProtectorConfig() {
        LOGGER.info("Initialize flood protector config");
    }

    @PostConstruct
    public void load() {
        FLOOD_PROTECTOR_USE_ITEM =
                new FloodProtectorConfigItem("UseItemFloodProtector");
        FLOOD_PROTECTOR_ROLL_DICE =
                new FloodProtectorConfigItem("RollDiceFloodProtector");
        FLOOD_PROTECTOR_FIREWORK =
                new FloodProtectorConfigItem("FireworkFloodProtector");
        FLOOD_PROTECTOR_ITEM_PET_SUMMON =
                new FloodProtectorConfigItem("ItemPetSummonFloodProtector");
        FLOOD_PROTECTOR_HERO_VOICE =
                new FloodProtectorConfigItem("HeroVoiceFloodProtector");
        FLOOD_PROTECTOR_GLOBAL_CHAT =
                new FloodProtectorConfigItem("GlobalChatFloodProtector");
        FLOOD_PROTECTOR_SUBCLASS =
                new FloodProtectorConfigItem("SubclassFloodProtector");
        FLOOD_PROTECTOR_DROP_ITEM =
                new FloodProtectorConfigItem("DropItemFloodProtector");
        FLOOD_PROTECTOR_SERVER_BYPASS =
                new FloodProtectorConfigItem("ServerBypassFloodProtector");
        FLOOD_PROTECTOR_MULTISELL =
                new FloodProtectorConfigItem("MultiSellFloodProtector");
        FLOOD_PROTECTOR_TRANSACTION =
                new FloodProtectorConfigItem("TransactionFloodProtector");
        FLOOD_PROTECTOR_MANUFACTURE =
                new FloodProtectorConfigItem("ManufactureFloodProtector");
        FLOOD_PROTECTOR_MANOR =
                new FloodProtectorConfigItem("ManorFloodProtector");
        FLOOD_PROTECTOR_CHARACTER_SELECT =
                new FloodProtectorConfigItem("CharacterSelectFloodProtector");

        FLOOD_PROTECTOR_UNKNOWN_PACKETS =
                new FloodProtectorConfigItem("UnknownPacketsFloodProtector");
        FLOOD_PROTECTOR_PARTY_INVITATION =
                new FloodProtectorConfigItem("PartyInvitationFloodProtector");
        FLOOD_PROTECTOR_SAY_ACTION =
                new FloodProtectorConfigItem("SayActionFloodProtector");
        FLOOD_PROTECTOR_MOVE_ACTION =
                new FloodProtectorConfigItem("MoveActionFloodProtector");
        FLOOD_PROTECTOR_GENERIC_ACTION =
                new FloodProtectorConfigItem("GenericActionFloodProtector", true);
        FLOOD_PROTECTOR_MACRO =
                new FloodProtectorConfigItem("MacroFloodProtector", true);
        FLOOD_PROTECTOR_POTION =
                new FloodProtectorConfigItem("PotionFloodProtector", true);

        loadFloodProtectorConfig(FLOOD_PROTECTOR_USE_ITEM, "UseItem");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_ROLL_DICE, "RollDice");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_FIREWORK, "Firework");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_ITEM_PET_SUMMON, "ItemPetSummon");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_HERO_VOICE, "HeroVoice");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_GLOBAL_CHAT, "GlobalChat");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_SUBCLASS, "Subclass");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_DROP_ITEM, "DropItem");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_SERVER_BYPASS, "ServerBypass");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_MULTISELL, "MultiSell");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_TRANSACTION, "Transaction");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_MANUFACTURE, "Manufacture");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_MANOR, "Manor");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_CHARACTER_SELECT, "CharacterSelect");

        loadFloodProtectorConfig(FLOOD_PROTECTOR_UNKNOWN_PACKETS, "UnknownPackets");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_PARTY_INVITATION, "PartyInvitation");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_SAY_ACTION, "SayAction");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_MOVE_ACTION, "MoveAction");

        loadFloodProtectorConfig(FLOOD_PROTECTOR_GENERIC_ACTION, "GenericAction");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_MACRO, "Macro");
        loadFloodProtectorConfig(FLOOD_PROTECTOR_POTION, "Potion");


    }

    private void loadFloodProtectorConfig(FloodProtectorConfigItem configItem, String configName) {
        configItem.FLOOD_PROTECTION_INTERVAL = configManager.getFloat(StringUtil.concat(FLOOD_PROTECTOR_NAME, configName, "Interval"));
        configItem.LOG_FLOODING = configManager.getBoolean(StringUtil.concat(FLOOD_PROTECTOR_NAME, configName, "LogFlooding"));
        configItem.PUNISHMENT_LIMIT = configManager.getInteger(StringUtil.concat(FLOOD_PROTECTOR_NAME, configName, "PunishmentLimit"));
        configItem.PUNISHMENT_TYPE = configManager.getString(StringUtil.concat(FLOOD_PROTECTOR_NAME, configName, "PunishmentType"));
        configItem.PUNISHMENT_TIME = configManager.getInteger(StringUtil.concat(FLOOD_PROTECTOR_NAME, configName, "PunishmentTime"));
    }
}
