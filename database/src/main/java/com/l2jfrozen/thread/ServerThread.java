package com.l2jfrozen.thread;

/**
 * vdidenko 04.12.13
 */
public interface ServerThread
{
	int getPlayerCount();

	boolean hasAccountOnGameServer(String account);

	public void kickPlayer(String accountName);
}
