package com.l2jfrozen.database.dal.login.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.l2jfrozen.database.dal.impl.ManagerImpl;
import com.l2jfrozen.database.dal.login.GameServerManagerDAO;
import com.l2jfrozen.database.model.login.GameServer;

/**
 * User: vdidenko Date: 26.11.13 Time: 23:02
 */

@Repository("GameServerManagerDAO")
public class GameServerManagerImplDAO extends ManagerImpl implements GameServerManagerDAO
{
	@Override
	@Transactional
	public GameServer add(GameServer gameServer)
	{
		try
		{
			getCurrentSession().persist(gameServer);
			return gameServer;
		}
		catch (Exception ex)
		{
			LOGGER.error("Got hibernate exception. Most likely this is just a constraint violation.", ex);
		}
		return null;
	}

	@Override
	@Transactional
	public void clear()
	{
		try
		{
			getCurrentSession().createQuery("DELETE GameServer");
			getCurrentSession().flush();
			getCurrentSession().clear();
		}
		catch (HibernateException e)
		{
			LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
		}
	}

	@Override
	@Transactional
	public void delete(GameServer gameServer)
	{
		try
		{
			getCurrentSession().delete(gameServer);
			getCurrentSession().flush();
			getCurrentSession().clear();
		}
		catch (HibernateException e)
		{
			LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
		}
	}

	@Override
	@Transactional(readOnly = true)
	public GameServer get(Integer id)
	{
		for (GameServer gameServer : getAll())
		{
			if (gameServer.getId().equals(id))
			{
				return gameServer;
			}
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<GameServer> getAll()
	{
		List<GameServer> servers = getCurrentSession().createQuery("SELECT gameServer FROM GameServer gameServer").list();
		return servers;
	}
}
