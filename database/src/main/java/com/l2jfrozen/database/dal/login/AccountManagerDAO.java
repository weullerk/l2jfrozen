package com.l2jfrozen.database.dal.login;

import java.sql.SQLException;

import com.l2jfrozen.database.model.login.Account;

/**
 * author vadim.didenko 1/11/14.
 */
public interface AccountManagerDAO
{
	Account add(Account account);

	Account get(String login) throws SQLException;

	Account get(String login, String password) throws SQLException;

	Account update(Account account);
}
