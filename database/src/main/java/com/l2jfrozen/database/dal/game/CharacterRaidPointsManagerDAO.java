package com.l2jfrozen.database.dal.game;

import com.l2jfrozen.database.model.game.ItemLocation;
import com.l2jfrozen.database.model.game.character.CharacterItemEntity;
import com.l2jfrozen.database.model.game.character.CharacterRaidPoints;

import java.util.List;

/**
 * author vadim.didenko 1/11/14.
 */
public interface CharacterRaidPointsManagerDAO
{
	CharacterRaidPoints create(CharacterRaidPoints raidPoints);

	void delete(CharacterRaidPoints raidPoints);

	void delete(long id);

	void delete(Long ownerId, List<Long> bossIds);

    void cleanUpPoints();

    CharacterRaidPoints get(long _Id);

    CharacterRaidPoints get(int charId, int bossId);

    List<CharacterRaidPoints> getCharRaidPoints(int ownerId);

    List<CharacterRaidPoints> getAllCharsRaidPoints();

    CharacterRaidPoints update(CharacterRaidPoints raidPoints);

}
