package com.l2jfrozen.database.dal.game;

import java.util.List;

import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterFriendEntity;

/**
 * author vadim.didenko 1/10/14.
 */

public interface CharacterManagerDAO
{
	CharacterEntity create(CharacterEntity character);

	void delete(CharacterEntity character);

	List<CharacterEntity> get(String accountName);

	CharacterEntity get(Long id);

	List<CharacterEntity> getByClanId(int clanId);

	CharacterEntity getByName(String name);

	List<CharacterEntity> getSponsor(long id);

	CharacterEntity update(CharacterEntity character);

	CharacterFriendEntity createFriend(CharacterFriendEntity friend);

	boolean deleteFriend(CharacterEntity character, CharacterEntity friendCharacter);

	void deleteAllShortcuts(CharacterEntity character);

	void deleteShortcutByClass(CharacterEntity character, int classIndex);

	void clearCache();
}
