package com.l2jfrozen.database.dal.login.impl;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.l2jfrozen.database.dal.impl.ManagerImpl;
import com.l2jfrozen.database.dal.login.AccountManagerDAO;
import com.l2jfrozen.database.model.login.Account;

/**
 * vdidenko 05.12.13
 */
@Repository("AccountManagerDAO")
public class AccountManagerDAOImpl extends ManagerImpl implements AccountManagerDAO
{
	@Override
	@Transactional
	public Account add(Account account)
	{
		try
		{
			getCurrentSession().persist(account);
			return account;
		}
		catch (Exception ex)
		{
			LOGGER.error("Got hibernate exception.", ex);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Account get(String login) throws SQLException
	{
		Account account = null;
		try
		{
			account = (Account) getCurrentSession().createCriteria(Account.class).add(Restrictions.eq("login", login)).uniqueResult();
		}
		catch (Exception e)
		{
			LOGGER.error("", e);
		}
		return account;
	}

	@Override
	@Transactional(readOnly = true)
	public Account get(String login, String password) throws SQLException
	{
		Account account = null;
		try
		{
			account = (Account) getCurrentSession().createCriteria(Account.class).add(Restrictions.and(Restrictions.eq("login", login), Restrictions.eq("password", password))).uniqueResult();
		}
		catch (Exception e)
		{
			LOGGER.error("", e);
		}
		return account;
	}

	@Override
	@Transactional
	public Account update(Account account)
	{
		try
		{
			getCurrentSession().update(account);
			getCurrentSession().flush();
			getCurrentSession().clear();
			return account;
		}
		catch (HibernateException e)
		{
			LOGGER.error("Got hibernate exception.", e);
		}
		return null;
	}
}
