package com.l2jfrozen.database.dal.game;

import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterSkill;

/**
 * Created by vadim.didenko on 22.01.14.
 */
public interface CharacterSkillManagerDAO
{

	CharacterSkill addSkill(CharacterSkill skill);

	CharacterSkill updateSkill(CharacterSkill skill);

	void deleteSkills(Long charId, int classIndex);

	CharacterEntity getSavedSkills(CharacterEntity entity, int classIndex);
}
