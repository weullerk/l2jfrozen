package com.l2jfrozen.database.dal.game.impl;

import java.util.HashSet;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.l2jfrozen.database.dal.game.CharacterSkillManagerDAO;
import com.l2jfrozen.database.dal.impl.ManagerImpl;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterSkill;
import com.l2jfrozen.database.model.game.character.CharacterSkillsSave;

/**
 * author vadim.didenko 22.01.14
 */
@Repository
public class CharacterSkillManagerDAOImpl extends ManagerImpl implements CharacterSkillManagerDAO
{

	@Override
	@Transactional
	public CharacterSkill addSkill(CharacterSkill skill)
	{
		getCurrentSession().persist(skill);
		return skill;
	}

	@Override
	@Transactional
	public CharacterSkill updateSkill(CharacterSkill skill)
	{
		getCurrentSession().merge(skill);
		return skill;
	}

	@Override
	@Transactional
	public void deleteSkills(Long charId, int classIndex)
	{
		getCurrentSession().createQuery("DELETE FROM character_skill skill WHERE skill.character.id=:characterId AND skill.classIndex=:classIndex").setLong("characterId", charId).setInteger("classIndex", classIndex);

	}

	@Override
	@Transactional
	public CharacterEntity getSavedSkills(CharacterEntity entity, int classIndex)
	{
		List<CharacterSkillsSave> skillsSaves;
		entity.setSkillsSave(new HashSet<CharacterSkillsSave>());
		skillsSaves = getCurrentSession().createCriteria(CharacterSkillsSave.class).add(Restrictions.and(Restrictions.eq("classIndex", classIndex), Restrictions.eq("character", entity))).list();
		for (CharacterSkillsSave skillsSave : skillsSaves)
		{
			entity.getSkillsSave().add(skillsSave);
		}
		return entity;
	}

}
