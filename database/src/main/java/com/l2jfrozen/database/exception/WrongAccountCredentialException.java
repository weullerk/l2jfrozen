package com.l2jfrozen.database.exception;

/**
 * vdidenko 25.12.13
 */
public class WrongAccountCredentialException extends Exception
{

	public WrongAccountCredentialException(String message)
	{
		super(message);
	}
}
