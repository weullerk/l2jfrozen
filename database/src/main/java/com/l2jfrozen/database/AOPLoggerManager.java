package com.l2jfrozen.database;

import java.util.concurrent.TimeUnit;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * author vadim.didenko 1/11/14.
 */
@Aspect
public class AOPLoggerManager
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AOPLoggerManager.class);

	public LogLevel level = LogLevel.INFO;

	@Pointcut("execution(* com.l2jfrozen.database.dal..*(..))")
	public void methodAnnotatedWithMyService()
	{
		// Méthode vide servant de Pointcut
	}

	@Around("methodAnnotatedWithMyService()")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable
	{
		long time = System.nanoTime();
		Object result = joinPoint.proceed();
		String message = "Call method: '{}', delay: '{}' milliseconds";
		Object[] args =
		{ joinPoint.getSignature().toShortString(), TimeUnit.MILLISECONDS.convert(System.nanoTime() - time, TimeUnit.NANOSECONDS) };
		switch (level)
		{
			case DEBUG:
				LOGGER.debug(message, args);
				break;
			case INFO:
				LOGGER.info(message, args);
				break;
			case ERROR:
				LOGGER.error(message, args);
				break;
		}

		return result;
	}

	public LogLevel getLevel()
	{
		return level;
	}

	public void setLevel(LogLevel level)
	{
		this.level = level;
	}

	public static enum LogLevel
	{
		INFO, DEBUG, ERROR
	}
}
