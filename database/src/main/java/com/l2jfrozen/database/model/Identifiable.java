package com.l2jfrozen.database.model;

import java.io.Serializable;

/**
 *
 */
public interface Identifiable extends Serializable
{
	/**
	 * Returns a Long id which identifies this object. It will be mostly used to load this object from DB.
	 * 
	 * @return ID of the object.
	 */
	Long getId();

}
