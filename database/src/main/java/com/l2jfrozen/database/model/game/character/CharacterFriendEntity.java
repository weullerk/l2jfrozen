package com.l2jfrozen.database.model.game.character;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_friend")
public class CharacterFriendEntity extends AbstractIdentifiable
{

	private CharacterEntity character;
	private CharacterEntity friend;
	private String friendName;
	private int notBlocked;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "char_id")
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity charId)
	{
		this.character = charId;
	}

	@Id
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "friend_id")
	public CharacterEntity getFriend()
	{
		return friend;
	}

	public void setFriend(CharacterEntity friendId)
	{
		this.friend = friendId;
	}

	@Column(name = "friend_name")
	public String getFriendName()
	{
		return friendName;
	}

	public void setFriendName(String friendName)
	{
		this.friendName = friendName;
	}

	@Column(name = "not_blocked")
	public int getNotBlocked()
	{
		return notBlocked;
	}

	public void setNotBlocked(int notBlocked)
	{
		this.notBlocked = notBlocked;
	}
}
