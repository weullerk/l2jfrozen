package com.l2jfrozen.database.model.game.character;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_raid_points")
public class CharacterRaidPoints extends AbstractIdentifiable
{
	private int charId;

	private int bossId;

	private int points;

	@Column(name = "boss_id")
	public int getBossId()
	{
		return bossId;
	}

	public void setBossId(int bossId)
	{
		this.bossId = bossId;
	}

	@Column(name = "charId")
	public int getCharId()
	{
		return charId;
	}

	public void setCharId(int charId)
	{
		this.charId = charId;
	}

	@Column(name = "points")
	public int getPoints()
	{
		return points;
	}

	public void setPoints(int points)
	{
		this.points = points;
	}
}
