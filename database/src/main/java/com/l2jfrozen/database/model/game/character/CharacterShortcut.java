package com.l2jfrozen.database.model.game.character;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_shortcut")
public class CharacterShortcut extends AbstractIdentifiable
{
	private CharacterEntity character;

	private int slot;

	private int page;

	private Integer type;

	private Integer shortcutId;

	private String level;

	private int classIndex;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "char_obj_id")
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity character)
	{
		this.character = character;
	}

	@Column(name = "class_index")
	public int getClassIndex()
	{
		return classIndex;
	}

	public void setClassIndex(int classIndex)
	{
		this.classIndex = classIndex;
	}

	@Basic
	@Column(name = "level")
	public String getLevel()
	{
		return level;
	}

	public void setLevel(String level)
	{
		this.level = level;
	}

	@Column(name = "page")
	public int getPage()
	{
		return page;
	}

	public void setPage(int page)
	{
		this.page = page;
	}

	@Basic
	@Column(name = "shortcut_id")
	public Integer getShortcutId()
	{
		return shortcutId;
	}

	public void setShortcutId(Integer shortcutId)
	{
		this.shortcutId = shortcutId;
	}

	@Column(name = "slot")
	public int getSlot()
	{
		return slot;
	}

	public void setSlot(int slot)
	{
		this.slot = slot;
	}

	@Basic
	@Column(name = "type")
	public Integer getType()
	{
		return type;
	}

	public void setType(Integer type)
	{
		this.type = type;
	}
}
