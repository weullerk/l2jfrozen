package com.l2jfrozen.database.model.game.character;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_recipebook")
public class CharacterRecipebook extends AbstractIdentifiable
{
	private CharacterEntity character;

	private RecipeType type;
	private int recipeId;

	@Column(name = "recipe_id")
	public int getRecipeId()
	{
		return recipeId;
	}

	public void setRecipeId(int recipeId)
	{
		this.recipeId = recipeId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "char_id")
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity character)
	{
		this.character = character;
	}

	@Column(name = "type")
	@Enumerated(EnumType.ORDINAL)
	public RecipeType getType()
	{
		return type;
	}

	public void setType(RecipeType type)
	{
		this.type = type;
	}

	public static enum RecipeType
	{
		COMMON, DWARVEN
	}
}
