package com.l2jfrozen.database.model.game.character;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_henna")
public class CharacterHenna extends AbstractIdentifiable
{

	private CharacterEntity character;

	private Integer symbolId;

	private int slot;

	private int classIndex;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "char_obj_id")
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity character)
	{
		this.character = character;
	}

	@Id
	@Column(name = "class_index")
	public int getClassIndex()
	{
		return classIndex;
	}

	public void setClassIndex(int classIndex)
	{
		this.classIndex = classIndex;
	}

	@Id
	@Column(name = "slot")
	public int getSlot()
	{
		return slot;
	}

	public void setSlot(int slot)
	{
		this.slot = slot;
	}

	@Column(name = "symbol_id")
	public Integer getSymbolId()
	{
		return symbolId;
	}

	public void setSymbolId(Integer symbolId)
	{
		this.symbolId = symbolId;
	}

}
