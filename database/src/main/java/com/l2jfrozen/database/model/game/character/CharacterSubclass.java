package com.l2jfrozen.database.model.game.character;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_subclasses")
public class CharacterSubclass extends AbstractIdentifiable
{
	private CharacterEntity character;

	private int classId;

	private long exp;

	private int sp;

	private int level;

	private int classIndex;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "char_obj_id")
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity character)
	{
		this.character = character;
	}

	@Id
	@Column(name = "class_id")
	public int getClassId()
	{
		return classId;
	}

	public void setClassId(int classId)
	{
		this.classId = classId;
	}

	@Basic
	@Column(name = "class_index")
	public int getClassIndex()
	{
		return classIndex;
	}

	public void setClassIndex(int classIndex)
	{
		this.classIndex = classIndex;
	}

	@Basic
	@Column(name = "exp")
	public long getExp()
	{
		return exp;
	}

	public void setExp(long exp)
	{
		this.exp = exp;
	}

	@Basic
	@Column(name = "level")
	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	@Basic
	@Column(name = "sp")
	public int getSp()
	{
		return sp;
	}

	public void setSp(int sp)
	{
		this.sp = sp;
	}
}
