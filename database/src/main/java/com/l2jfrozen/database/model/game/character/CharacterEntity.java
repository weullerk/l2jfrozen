package com.l2jfrozen.database.model.game.character;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;
import com.l2jfrozen.database.model.game.AccessLevelType;

/**
 * author vadim.didenko 1/10/14.
 */
@Entity
@Table(name = "characters")
public class CharacterEntity extends AbstractIdentifiable
{
	private String accountName;
	private String charName;
	private int level;
	private int maxHp;
	private int curHp;
	private int maxCp;
	private int curCp;
	private int maxMp;
	private int curMp;
	private int accuracy;
	private int critical;
	private int evasion;
	private int magicAtk;
	private int mDef;
	private int mSpd;
	private int pAtk;
	private int pDef;
	private int pSpd;
	private int runSpd;
	private int walkSpd;
	private int statStr;
	private int statCon;
	private int statDex;
	private int statInt;
	private int statMen;
	private int statWit;
	private int face;
	private int hairStyle;
	private int hairColor;
	private int sex;
	private int heading;
	private int x;
	private int y;
	private int z;
	private int movementMultiplier;
	private int attackSpeedMultiplier;
	private int collisionRadius;
	private int collisionHeight;
	private int exp;
	private int expBeforeDeath;
	private int sp;
	private int karma;
	private int pvpKillCount;
	private int pkKillCount;
	private int clanId;
	private int maxLoad;
	private int race;
	private int classId;
	private int baseClass;
	private long deleteTime;
	private int canCraft;
	private String title;
	private int recHave;
	private int recLeft;
	private AccessLevelType accessLevel;
	private int online;
	private long onlineTime;
	private int charSlot;
	private int newbie;
	private long lastAccess;
	private int clanPrivs;
	private int wantSpeace;
	private int sevenSingsDungeon;
	private int punishLevel;
	private long punishTimer;
	private int powerGrade;
	private int nobless;
	private int subPledge;
	private long lastRecomDate;
	private int lvlJoinedAcademy;
	private int apprentice;
	private Long sponsor;
	private int varkaKetraAlly;
	private long clanJoinExpiryTime;
	private long clanCreateExpiryTime;
	private int deathPenaltyLevel;
	private int pcPoint;
	private String nameColor;
	private String titleColor;
	private int firstLog;
	private int aio;
	private int aioEnd;
	private CharacterCustomData customData;

	private Map<Long, CharacterFriendEntity> friends = new HashMap<>();
	private Map<Long, CharacterSkill> skills = new HashMap<>();
	private Map<Long, CharacterHenna> hennas = new HashMap<>();
	private Map<Long, CharacterItemEntity> items = new HashMap<>();
	private Map<Long, CharacterMacros> macrosList = new HashMap<>();
	private Map<Long, CharacterQuest> quests = new HashMap<>();
	private Map<Long, CharacterShortcut> shortcuts = new HashMap<>();
	private Map<Integer, CharacterRecipebook> recipeBooks = new HashMap<>();
	private Set<CharacterEntity> recommends = new HashSet<>();
	private Set<CharacterEntity> recommended = new HashSet<>();
	private Map<Integer, CharacterSubclass> subclasses = new HashMap<>();
	private Set<CharacterSkillsSave> skillsSave = new HashSet<>();

	@OneToOne(mappedBy = "character", fetch = FetchType.EAGER)
	public CharacterCustomData getCustomData()
	{
		return customData;
	}

	public void setCustomData(CharacterCustomData customData)
	{
		this.customData = customData;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL)
	public Set<CharacterSkillsSave> getSkillsSave()
	{
		return skillsSave;
	}

	public void setSkillsSave(Set<CharacterSkillsSave> skillsSave)
	{
		this.skillsSave = skillsSave;
	}

	@MapKeyColumn(name = "class_index")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL)
	public Map<Integer, CharacterSubclass> getSubclasses()
	{
		return subclasses;
	}

	public void setSubclasses(Map<Integer, CharacterSubclass> subclasses)
	{
		this.subclasses = subclasses;
	}

	@Column(name = "accesslevel")
	@Enumerated(EnumType.ORDINAL)
	public AccessLevelType getAccessLevel()
	{
		return accessLevel;
	}

	public void setAccessLevel(AccessLevelType accesslevel)
	{
		this.accessLevel = accesslevel;
	}

	@Column(name = "account_name")
	public String getAccountName()
	{
		return accountName;
	}

	public void setAccountName(String accountName)
	{
		this.accountName = accountName;
	}

	@Column(name = "acc")
	public int getAccuracy()
	{
		return accuracy;
	}

	public void setAccuracy(int acc)
	{
		this.accuracy = acc;
	}

	@Column(name = "aio")
	public int getAio()
	{
		return aio;
	}

	public void setAio(int aio)
	{
		this.aio = aio;
	}

	@Column(name = "aio_end")
	public int getAioEnd()
	{
		return aioEnd;
	}

	public void setAioEnd(int aioEnd)
	{
		this.aioEnd = aioEnd;
	}

	@Column(name = "apprentice")
	public int getApprentice()
	{
		return apprentice;
	}

	public void setApprentice(int apprentice)
	{
		this.apprentice = apprentice;
	}

	@Column(name = "attack_speed_multiplier")
	public int getAttackSpeedMultiplier()
	{
		return attackSpeedMultiplier;
	}

	public void setAttackSpeedMultiplier(int attackSpeedMultiplier)
	{
		this.attackSpeedMultiplier = attackSpeedMultiplier;
	}

	@Column(name = "base_class")
	public int getBaseClass()
	{
		return baseClass;
	}

	public void setBaseClass(int baseClass)
	{
		this.baseClass = baseClass;
	}

	@Column(name = "cancraft")
	public int getCanCraft()
	{
		return canCraft;
	}

	public void setCanCraft(int cancraft)
	{
		this.canCraft = cancraft;
	}

	@Column(name = "char_name")
	public String getCharName()
	{
		return charName;
	}

	public void setCharName(String charName)
	{
		this.charName = charName;
	}

	@Column(name = "char_slot")
	public int getCharSlot()
	{
		return charSlot;
	}

	public void setCharSlot(int charSlot)
	{
		this.charSlot = charSlot;
	}

	@Column(name = "clan_create_expiry_time", length = 25)
	public long getClanCreateExpiryTime()
	{
		return clanCreateExpiryTime;
	}

	public void setClanCreateExpiryTime(long clanCreateExpiryTime)
	{
		this.clanCreateExpiryTime = clanCreateExpiryTime;
	}

	@Column(name = "clanid")
	public int getClanId()
	{
		return clanId;
	}

	public void setClanId(int clanid)
	{
		this.clanId = clanid;
	}

	@Column(name = "clan_join_expiry_time", length = 25)
	public long getClanJoinExpiryTime()
	{
		return clanJoinExpiryTime;
	}

	public void setClanJoinExpiryTime(long clanJoinExpiryTime)
	{
		this.clanJoinExpiryTime = clanJoinExpiryTime;
	}

	@Column(name = "clan_privs")
	public int getClanPrivs()
	{
		return clanPrivs;
	}

	public void setClanPrivs(int clanPrivs)
	{
		this.clanPrivs = clanPrivs;
	}

	@Column(name = "classid")
	public int getClassId()
	{
		return classId;
	}

	public void setClassId(int classid)
	{
		this.classId = classid;
	}

	@Column(name = "colHeight")
	public int getCollisionHeight()
	{
		return collisionHeight;
	}

	public void setCollisionHeight(int colHeight)
	{
		this.collisionHeight = colHeight;
	}

	@Column(name = "colRad")
	public int getCollisionRadius()
	{
		return collisionRadius;
	}

	public void setCollisionRadius(int colRad)
	{
		this.collisionRadius = colRad;
	}

	@Column(name = "crit")
	public int getCritical()
	{
		return critical;
	}

	public void setCritical(int crit)
	{
		this.critical = crit;
	}

	@Column(name = "curCp")
	public int getCurCp()
	{
		return curCp;
	}

	public void setCurCp(int curCp)
	{
		this.curCp = curCp;
	}

	@Column(name = "curHp")
	public int getCurHp()
	{
		return curHp;
	}

	public void setCurHp(int curHp)
	{
		this.curHp = curHp;
	}

	@Column(name = "curMp")
	public int getCurMp()
	{
		return curMp;
	}

	public void setCurMp(int curMp)
	{
		this.curMp = curMp;
	}

	@Column(name = "death_penalty_level")
	public int getDeathPenaltyLevel()
	{
		return deathPenaltyLevel;
	}

	public void setDeathPenaltyLevel(int deathPenaltyLevel)
	{
		this.deathPenaltyLevel = deathPenaltyLevel;
	}

	@Column(name = "deletetime")
	public long getDeleteTime()
	{
		return deleteTime;
	}

	public void setDeleteTime(long deletetime)
	{
		this.deleteTime = deletetime;
	}

	@Column(name = "evasion")
	public int getEvasion()
	{
		return evasion;
	}

	public void setEvasion(int evasion)
	{
		this.evasion = evasion;
	}

	@Column(name = "exp")
	public int getExp()
	{
		return exp;
	}

	public void setExp(int exp)
	{
		this.exp = exp;
	}

	@Column(name = "expBeforeDeath")
	public int getExpBeforeDeath()
	{
		return expBeforeDeath;
	}

	public void setExpBeforeDeath(int expBeforeDeath)
	{
		this.expBeforeDeath = expBeforeDeath;
	}

	@Column(name = "face")
	public int getFace()
	{
		return face;
	}

	public void setFace(int face)
	{
		this.face = face;
	}

	@Column(name = "first_log")
	public int getFirstLog()
	{
		return firstLog;
	}

	public void setFirstLog(int firstLog)
	{
		this.firstLog = firstLog;
	}

	/**
	 * As key use friend id
	 * 
	 * @return map of friend entity
	 */
	@MapKeyColumn(name = "friend_id")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL)
	public Map<Long, CharacterFriendEntity> getFriends()
	{
		return friends;
	}

	public void setFriends(Map<Long, CharacterFriendEntity> friends)
	{
		this.friends = friends;
	}

	@Column(name = "hairColor")
	public int getHairColor()
	{
		return hairColor;
	}

	public void setHairColor(int hairColor)
	{
		this.hairColor = hairColor;
	}

	@Column(name = "hairStyle")
	public int getHairStyle()
	{
		return hairStyle;
	}

	public void setHairStyle(int hairStyle)
	{
		this.hairStyle = hairStyle;
	}

	@Column(name = "heading")
	public int getHeading()
	{
		return heading;
	}

	public void setHeading(int heading)
	{
		this.heading = heading;
	}

	/**
	 * As key use _id field
	 * 
	 * @return map of henna entity
	 */
	@MapKeyColumn(name = "_id")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL, orphanRemoval = true)
	public Map<Long, CharacterHenna> getHennas()
	{
		return hennas;
	}

	public void setHennas(Map<Long, CharacterHenna> hennas)
	{
		this.hennas = hennas;
	}

	/**
	 * As key use _id field
	 * 
	 * @return map of item entity
	 */
	@MapKeyColumn(name = "_id")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL)
	public Map<Long, CharacterItemEntity> getItems()
	{
		return items;
	}

	public void setItems(Map<Long, CharacterItemEntity> items)
	{
		this.items = items;
	}

	@Column(name = "karma")
	public int getKarma()
	{
		return karma;
	}

	public void setKarma(int karma)
	{
		this.karma = karma;
	}

	@Column(name = "lastAccess")
	public long getLastAccess()
	{
		return lastAccess;
	}

	public void setLastAccess(long lastAccess)
	{
		this.lastAccess = lastAccess;
	}

	@Column(name = "last_recom_date", length = 25)
	public long getLastRecomDate()
	{
		return lastRecomDate;
	}

	public void setLastRecomDate(long lastRecomDate)
	{
		this.lastRecomDate = lastRecomDate;
	}

	@Column(name = "level")
	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	@Column(name = "lvl_joined_academy")
	public int getLvlJoinedAcademy()
	{
		return lvlJoinedAcademy;
	}

	public void setLvlJoinedAcademy(int lvlJoinedAcademy)
	{
		this.lvlJoinedAcademy = lvlJoinedAcademy;
	}

	@MapKeyColumn(name = "_id")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL)
	public Map<Long, CharacterMacros> getMacrosList()
	{
		return macrosList;
	}

	public void setMacrosList(Map<Long, CharacterMacros> macrosList)
	{
		this.macrosList = macrosList;
	}

	@Column(name = "mAtk")
	public int getMagicAtk()
	{
		return magicAtk;
	}

	public void setMagicAtk(int mAtk)
	{
		this.magicAtk = mAtk;
	}

	@Column(name = "maxCp")
	public int getMaxCp()
	{
		return maxCp;
	}

	public void setMaxCp(int maxCp)
	{
		this.maxCp = maxCp;
	}

	@Column(name = "maxHp")
	public int getMaxHp()
	{
		return maxHp;
	}

	public void setMaxHp(int maxHp)
	{
		this.maxHp = maxHp;
	}

	@Column(name = "maxload")
	public int getMaxLoad()
	{
		return maxLoad;
	}

	public void setMaxLoad(int maxload)
	{
		this.maxLoad = maxload;
	}

	@Column(name = "maxMp")
	public int getMaxMp()
	{
		return maxMp;
	}

	public void setMaxMp(int maxMp)
	{
		this.maxMp = maxMp;
	}

	@Column(name = "movement_multiplier")
	public int getMovementMultiplier()
	{
		return movementMultiplier;
	}

	public void setMovementMultiplier(int movementMultiplier)
	{
		this.movementMultiplier = movementMultiplier;
	}

	@Column(name = "name_color")
	public String getNameColor()
	{
		return nameColor;
	}

	public void setNameColor(String nameColor)
	{
		this.nameColor = nameColor;
	}

	@Column(name = "newbie")
	public int getNewbie()
	{
		return newbie;
	}

	public void setNewbie(int newbie)
	{
		this.newbie = newbie;
	}

	@Column(name = "nobless")
	public int getNobless()
	{
		return nobless;
	}

	public void setNobless(int nobless)
	{
		this.nobless = nobless;
	}

	@Column(name = "online")
	public int getOnline()
	{
		return online;
	}

	public void setOnline(int online)
	{
		this.online = online;
	}

	@Column(name = "onlinetime")
	public long getOnlineTime()
	{
		return onlineTime;
	}

	public void setOnlineTime(long onlinetime)
	{
		this.onlineTime = onlinetime;
	}

	@Column(name = "pc_point")
	public int getPcPoint()
	{
		return pcPoint;
	}

	public void setPcPoint(int pcPoint)
	{
		this.pcPoint = pcPoint;
	}

	@Column(name = "pkkills")
	public int getPkKillCount()
	{
		return pkKillCount;
	}

	public void setPkKillCount(int pkkills)
	{
		this.pkKillCount = pkkills;
	}

	@Column(name = "power_grade")
	public int getPowerGrade()
	{
		return powerGrade;
	}

	public void setPowerGrade(int powerGrade)
	{
		this.powerGrade = powerGrade;
	}

	@Column(name = "punish_level")
	public int getPunishLevel()
	{
		return punishLevel;
	}

	public void setPunishLevel(int punishLevel)
	{
		this.punishLevel = punishLevel;
	}

	@Column(name = "punish_timer", length = 25)
	public long getPunishTimer()
	{
		return punishTimer;
	}

	public void setPunishTimer(long punishTimer)
	{
		this.punishTimer = punishTimer;
	}

	@Column(name = "pvpkills")
	public int getPvpKillCount()
	{
		return pvpKillCount;
	}

	public void setPvpKillCount(int pvpkills)
	{
		this.pvpKillCount = pvpkills;
	}

	@MapKeyColumn(name = "_id")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL)
	public Map<Long, CharacterQuest> getQuests()
	{
		return quests;
	}

	public void setQuests(Map<Long, CharacterQuest> quests)
	{
		this.quests = quests;
	}

	@Column(name = "race")
	public int getRace()
	{
		return race;
	}

	public void setRace(int race)
	{
		this.race = race;
	}

	@Column(name = "rec_have")
	public int getRecHave()
	{
		return recHave;
	}

	public void setRecHave(int recHave)
	{
		this.recHave = recHave;
	}

	@Column(name = "rec_left")
	public int getRecLeft()
	{
		return recLeft;
	}

	public void setRecLeft(int recLeft)
	{
		this.recLeft = recLeft;
	}

	@MapKeyColumn(name = "recipe_id")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL)
	public Map<Integer, CharacterRecipebook> getRecipeBooks()
	{
		return recipeBooks;
	}

	public void setRecipeBooks(Map<Integer, CharacterRecipebook> recipeBooks)
	{
		this.recipeBooks = recipeBooks;
	}

	@ManyToMany(mappedBy = "recommends")
	public Set<CharacterEntity> getRecommended()
	{
		return recommended;
	}

	public void setRecommended(Set<CharacterEntity> recommended)
	{
		this.recommended = recommended;
	}

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "character_recommend", joinColumns =
	{ @JoinColumn(name = "char_id", nullable = false, updatable = false) }, inverseJoinColumns =
	{ @JoinColumn(name = "target_id", nullable = false, updatable = false) })
	public Set<CharacterEntity> getRecommends()
	{
		return recommends;
	}

	public void setRecommends(Set<CharacterEntity> recommends)
	{
		this.recommends = recommends;
	}

	@Column(name = "runSpd")
	public int getRunSpd()
	{
		return runSpd;
	}

	public void setRunSpd(int runSpd)
	{
		this.runSpd = runSpd;
	}

	@Column(name = "isin7sdungeon")
	public int getSevenSingsDungeon()
	{
		return sevenSingsDungeon;
	}

	public void setSevenSingsDungeon(int isin7Sdungeon)
	{
		this.sevenSingsDungeon = isin7Sdungeon;
	}

	@Column(name = "sex")
	public int getSex()
	{
		return sex;
	}

	public void setSex(int sex)
	{
		this.sex = sex;
	}

	@MapKeyColumn(name = "_id")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL)
	public Map<Long, CharacterShortcut> getShortcuts()
	{
		return shortcuts;
	}

	public void setShortcuts(Map<Long, CharacterShortcut> shortcut)
	{
		this.shortcuts = shortcut;
	}

	/**
	 * As key use _id field
	 * 
	 * @return map of skill entity
	 */
	@MapKeyColumn(name = "_id")
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "character", cascade = CascadeType.ALL)
	public Map<Long, CharacterSkill> getSkills()
	{
		return skills;
	}

	public void setSkills(Map<Long, CharacterSkill> skills)
	{
		this.skills = skills;
	}

	@Column(name = "sp")
	public int getSp()
	{
		return sp;
	}

	public void setSp(int sp)
	{
		this.sp = sp;
	}

	@Column(name = "sponsor", nullable = true)
	public Long getSponsor()
	{
		return sponsor;
	}

	public void setSponsor(Long sponsor)
	{
		this.sponsor = sponsor;
	}

	@Column(name = "con")
	public int getStatCon()
	{
		return statCon;
	}

	public void setStatCon(int statCon)
	{
		this.statCon = statCon;
	}

	@Column(name = "dex")
	public int getStatDex()
	{
		return statDex;
	}

	public void setStatDex(int statDex)
	{
		this.statDex = statDex;
	}

	@Column(name = "_int")
	public int getStatInt()
	{
		return statInt;
	}

	public void setStatInt(int statInt)
	{
		this.statInt = statInt;
	}

	@Column(name = "men")
	public int getStatMen()
	{
		return statMen;
	}

	public void setStatMen(int statMen)
	{
		this.statMen = statMen;
	}

	@Column(name = "str")
	public int getStatStr()
	{
		return statStr;
	}

	public void setStatStr(int statStr)
	{
		this.statStr = statStr;
	}

	@Column(name = "wit")
	public int getStatWit()
	{
		return statWit;
	}

	public void setStatWit(int statWit)
	{
		this.statWit = statWit;
	}

	@Column(name = "subpledge")
	public int getSubPledge()
	{
		return subPledge;
	}

	public void setSubPledge(int subpledge)
	{
		this.subPledge = subpledge;
	}

	@Column(name = "title")
	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	@Column(name = "title_color")
	public String getTitleColor()
	{
		return titleColor;
	}

	public void setTitleColor(String titleColor)
	{
		this.titleColor = titleColor;
	}

	@Column(name = "varka_ketra_ally")
	public int getVarkaKetraAlly()
	{
		return varkaKetraAlly;
	}

	public void setVarkaKetraAlly(int varkaKetraAlly)
	{
		this.varkaKetraAlly = varkaKetraAlly;
	}

	@Column(name = "walkSpd")
	public int getWalkSpd()
	{
		return walkSpd;
	}

	public void setWalkSpd(int walkSpd)
	{
		this.walkSpd = walkSpd;
	}

	@Column(name = "wantspeace")
	public int getWantSpeace()
	{
		return wantSpeace;
	}

	public void setWantSpeace(int wantspeace)
	{
		this.wantSpeace = wantspeace;
	}

	@Column(name = "x")
	public int getX()
	{
		return x;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	@Column(name = "y")
	public int getY()
	{
		return y;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	@Column(name = "z")
	public int getZ()
	{
		return z;
	}

	public void setZ(int z)
	{
		this.z = z;
	}

	@Column(name = "mDef")
	public int getmDef()
	{
		return mDef;
	}

	@Column(name = "mSpd")
	public int getmSpd()
	{
		return mSpd;
	}

	@Column(name = "pAtk")
	public int getpAtk()
	{
		return pAtk;
	}

	@Column(name = "pDef")
	public int getpDef()
	{
		return pDef;
	}

	@Column(name = "pSpd")
	public int getpSpd()
	{
		return pSpd;
	}

	public void setmDef(int mDef)
	{
		this.mDef = mDef;
	}

	public void setmSpd(int mSpd)
	{
		this.mSpd = mSpd;
	}

	public void setpAtk(int pAtk)
	{
		this.pAtk = pAtk;
	}

	public void setpDef(int pDef)
	{
		this.pDef = pDef;
	}

	public void setpSpd(int pSpd)
	{
		this.pSpd = pSpd;
	}
}
