package com.l2jfrozen.database.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 *
 */
@MappedSuperclass
public abstract class AbstractIdentifiable implements Identifiable
{
	private Long id;

	@Override
	@Id
	@GeneratedValue(generator = "IdGenerator")
	@GenericGenerator(name = "IdGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters =
	{ @Parameter(name = "sequence_name", value = "hibernate_sequence"), @Parameter(name = "optimizer", value = "hilo"), @Parameter(name = "initial_value", value = "268435456"), @Parameter(name = "increment_size", value = "1") })
	@Column(name = "_id", unique = true, nullable = false, precision = 20, length = 20, scale = 0)
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		AbstractIdentifiable that = (AbstractIdentifiable) o;

		return !(id != null ? !id.equals(that.id) : !super.equals(that));
	}

	@Override
	public int hashCode()
	{
		return id != null ? id.hashCode() : super.hashCode();
	}
}
