package com.l2jfrozen.database.model.game.character;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * vadim.didenko 3/1/14.
 */
@Entity
@Table(name = "character_custom_data")
public class CharacterCustomData extends AbstractIdentifiable
{
	private CharacterEntity character;
	private String charName;
	private int hero;
	private int noble;
	private int donator;
	private long heroEndDate;

	@OneToOne(fetch = FetchType.LAZY)
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity objId)
	{
		this.character = objId;
	}

	@Basic
	@Column(name = "char_name")
	public String getCharName()
	{
		return charName;
	}

	public void setCharName(String charName)
	{
		this.charName = charName;
	}

	@Basic
	@Column(name = "hero")
	public int getHero()
	{
		return hero;
	}

	public void setHero(int hero)
	{
		this.hero = hero;
	}

	@Basic
	@Column(name = "noble")
	public int getNoble()
	{
		return noble;
	}

	public void setNoble(int noble)
	{
		this.noble = noble;
	}

	@Basic
	@Column(name = "donator")
	public int getDonator()
	{
		return donator;
	}

	public void setDonator(int donator)
	{
		this.donator = donator;
	}

	@Basic
	@Column(name = "hero_end_date")
	public long getHeroEndDate()
	{
		return heroEndDate;
	}

	public void setHeroEndDate(long heroEndDate)
	{
		this.heroEndDate = heroEndDate;
	}
}
