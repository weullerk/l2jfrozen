package com.l2jfrozen.database.model.login;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/10/14.
 */
@Entity
@Table(name = "gameservers")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class GameServer extends AbstractIdentifiable
{
	private int serverId;
	private String hexid;
	private String host;

	@Column(name = "server_id")
	public int getServerId()
	{
		return serverId;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	@Column(name = "hexid")
	public String getHexid()
	{
		return hexid;
	}

	public void setHexid(String hexid)
	{
		this.hexid = hexid;
	}

	@Column(name = "host")
	public String getHost()
	{
		return host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

}
