package com.l2jfrozen.database.model.game.character;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.l2jfrozen.database.model.AbstractIdentifiable;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_quest")
public class CharacterQuest extends AbstractIdentifiable
{
	private CharacterEntity character;

	private String name;

	private String var;

	private String value;

	private int classIndex;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "char_id")
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity charId)
	{
		this.character = charId;
	}

	@javax.persistence.Column(name = "class_index")
	public int getClassIndex()
	{
		return classIndex;
	}

	public void setClassIndex(int classIndex)
	{
		this.classIndex = classIndex;
	}

	@javax.persistence.Column(name = "name")
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Basic
	@javax.persistence.Column(name = "value")
	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	@javax.persistence.Column(name = "var")
	public String getVar()
	{
		return var;
	}

	public void setVar(String var)
	{
		this.var = var;
	}
}
