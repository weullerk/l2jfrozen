package com.l2jfrozen.database.model.game.character;

import com.l2jfrozen.database.model.AbstractIdentifiable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * author vadim.didenko 1/12/14.
 */
@Entity
@Table(name = "character_skill")
public class CharacterSkill extends AbstractIdentifiable
{

	private CharacterEntity character;

	private int skillId;

	private Integer skillLevel;

	private String skillName;

	private int classIndex;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "char_obj_id")
	public CharacterEntity getCharacter()
	{
		return character;
	}

	public void setCharacter(CharacterEntity character)
	{
		this.character = character;
	}

	@Column(name = "class_index")
	public int getClassIndex()
	{
		return classIndex;
	}

	public void setClassIndex(int classIndex)
	{
		this.classIndex = classIndex;
	}

	@Column(name = "skill_id")
	public int getSkillId()
	{
		return skillId;
	}

	public void setSkillId(int skillId)
	{
		this.skillId = skillId;
	}

	@Basic
	@Column(name = "skill_level")
	public Integer getSkillLevel()
	{
		return skillLevel;
	}

	public void setSkillLevel(Integer skillLevel)
	{
		this.skillLevel = skillLevel;
	}

	@Basic
	@Column(name = "skill_name")
	public String getSkillName()
	{
		return skillName;
	}

	public void setSkillName(String skillName)
	{
		this.skillName = skillName;
	}

}
