package com.l2jfrozen.database.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javolution.io.UTF8StreamReader;
import javolution.util.FastMap;
import javolution.xml.stream.XMLStreamConstants;
import javolution.xml.stream.XMLStreamException;
import javolution.xml.stream.XMLStreamReaderImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.context.SpringApplicationContext;
import com.l2jfrozen.database.dal.login.GameServerManagerDAO;
import com.l2jfrozen.database.model.login.GameServer;

/**
 * User: vdidenko Date: 26.11.13 Time: 23:49
 */
public class GameServerManager
{
	protected static final Logger LOGGER = LoggerFactory.getLogger(GameServerManager.class);
	private static GameServerManager gameServerManager;
	private static GameServerManagerDAO gameServerManagerDao;

	// Server Names Config
	private Map<Integer, String> serverNames = new FastMap<>();

	public static GameServerManager getInstance()
	{
		if (gameServerManager == null)
		{
			gameServerManager = new GameServerManager();
		}
		return gameServerManager;
	}

	private GameServerManager()
	{
		gameServerManagerDao = (GameServerManagerDAO) SpringApplicationContext.getBean(GameServerManagerDAO.class);
		loadServerNames();
	}

	void loadServerNames()
	{
		XMLStreamReaderImpl xpp = new XMLStreamReaderImpl();
		UTF8StreamReader reader = new UTF8StreamReader();

		InputStream in = null;
		try
		{
			// init with classloader resources for debug
			in = this.getClass().getClassLoader().getResourceAsStream("config/servername.xml");

			// if available, use local folder
			File conf_file = new File("config/servername.xml");
			if (conf_file.exists())
				in = new FileInputStream(conf_file);

			xpp.setInput(reader.setInput(in));

			for (int e = xpp.getEventType(); e != XMLStreamConstants.END_DOCUMENT; e = xpp.next())
			{
				if (e == XMLStreamConstants.START_ELEMENT)
				{
					if (xpp.getLocalName().toString().equals("server"))
					{
						Integer id = new Integer(xpp.getAttributeValue(null, "id").toString());
						String name = xpp.getAttributeValue(null, "name").toString();
						serverNames.put(id, name);
					}
				}
			}
			LOGGER.info("Loaded " + serverNames.size() + " server names");
		}
		catch (FileNotFoundException e)
		{
			LOGGER.warn("servername.xml could not be loaded: file not found");
		}
		catch (XMLStreamException xppe)
		{
			LOGGER.error("", xppe);
		}
		finally
		{
			try
			{
				xpp.close();
			}
			catch (XMLStreamException e)
			{
				LOGGER.error("", e);
			}

			try
			{
				reader.close();
			}
			catch (IOException e)
			{
				LOGGER.error("", e);
			}

			if (in != null)
			{
				try
				{
					in.close();
				}
				catch (IOException e)
				{
					LOGGER.error("", e);
				}
			}
		}
	}

	public Map<Integer, String> getServerNames()
	{
		return serverNames;
	}

	public GameServer add(int id, String host, String hex)
	{
		GameServer gameServer = new GameServer();
		gameServer.setServerId(id);
		gameServer.setHexid(hex);
		gameServer.setHost(host);
		gameServer = gameServerManagerDao.add(gameServer);
		if (gameServer == null)
		{
			return null;
		}
		return gameServer;
	}

	public void clear()
	{
		gameServerManagerDao.clear();
	}

	public GameServer getServer(int serverId)
	{
		return gameServerManagerDao.get(serverId);
	}

	public String getServerName(int serverId)
	{
		return serverNames.get(serverId);
	}

	public boolean isRegisterServer(int id)
	{
		for (GameServer gameServer : getAll())
		{
			if (gameServer.getId() == id)
			{
				return true;
			}
		}
		return false;
	}

	public List<GameServer> getAll()
	{
		return gameServerManagerDao.getAll();
	}
}
