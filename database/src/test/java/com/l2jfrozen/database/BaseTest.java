package com.l2jfrozen.database;

import org.junit.BeforeClass;

import com.l2jfrozen.configuration.DatabaseConfig;

/**
 * User: vdidenko
 * Date: 28.11.13
 * Time: 7:37
 */
public class BaseTest {
    private static DatabaseConfig databaseConfig;

    @BeforeClass
    public static void setUpClass() throws Exception {

    }

    protected static DatabaseConfig getDatabaseConfig() {
        return databaseConfig;
    }
}
